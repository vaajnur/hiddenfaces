<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный Кабинет");
CModule::IncludeModule("subscribe");
?>
<? if (!$USER->IsAuthorized())
    header("Location: /");
?>
<?	
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
?>
<div class="container inner__container">
	<div class="row guests relative-container">
		<h1 class="col-12 no-bot-wrap">Профиль</h1>
		<div class="exit">
			<button >
			<i class="fa fa-sign-out" aria-hidden="true"></i> 
			Выход</button>
			<a href="#" class="editprofile">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				<span>Редактировать профиль</span>
			</a>
		</div>
		<div class="bread col-12">
			<a href="/">Главная</a>
			<span> > </span>
			<a>Профиль</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="exit mobver">
				<button>
				<i class="fa fa-sign-out" aria-hidden="true"></i> 
				Выход</button>
				<a href="#" class="editprofile">
					<i class="fa fa-pencil" aria-hidden="true"></i>
					<span>Редактировать профиль</span>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 radiobtn">
			<a href="#" class="activeradio profilelink">Профиль</a>
			<a href="#" class="sublink">Подписки</a>
		</div>
		<div class="col-md-9 flex-content profilecontent">
			<div class="inputcontent">
				<p>Имя</p>
				<input type="text" placeholder="Ваше имя" id="personal-name"  value="<?=$arUser['NAME'];?>" disabled="disabled">
			</div>
			<div class="inputcontent">
				<p>Ваш E-Mail</p>
				<input type="email" placeholder="sample@mail.ru" id="personal-email" value="<?=$arUser['EMAIL'];?>" disabled="disabled">
			</div>
			<div class="inputcontent">
				<p>Пароль</p>
				<input type="password" placeholder="Пароль" value="" disabled="disabled" class="passwordinput active_password" onkeyup="checkpass()">
			</div>
			<div class="inputcontent">
				<p>Новый пароль</p>
				<input type="password" placeholder="Новый пароль" value="" disabled="disabled" class="passwordinput new_pass1">
			</div>
			<div class="inputcontent">
				<p>Подтверждение нового пароля</p>
				<input type="password" placeholder="Подтверждение нового пароля" value="" disabled="disabled" class="passwordinput new_pass2">
			</div>
			<div class="container">
			<div class="row profilecontent">	
				<div class="col-md-12 saveall">
					<button>Сохранить изменения</button>
				</div>
			</div>
			</div>
		</div>
		<div class="col-md-9 subscribes">
			<div class="cardcontent">
				<p>#1 Подписка на новые выпуски</p>
				<div class="inputthere">
					<label>
						<label for="checkinput">Получать уведомления</label>
						<?php
						$subscription = CSubscription::GetByEmail($arUser['EMAIL']);
						$subscription->ExtractFields("str_");
						$checked="";
						if($str_ID){ 
							$aSubscrRub = CSubscription::GetRubricArray($str_ID);
							if(in_array(1,$aSubscrRub)){
								$checked="checked";
							}
							if(in_array(2,$aSubscrRub)){
								$checked="checked";
							}
						}
						?>
						<input type="checkbox" id="checkinput" <?=$checked;?>>
						<span class="truecheckbox">
							<span class="checked"></span>
						</span>
					</label>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>