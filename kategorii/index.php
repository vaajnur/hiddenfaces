<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hidden Faces - Передача");
?>
<?
$arSelect = array("ID","IBLOCK_ID","NAME","DETAIL_PICTURE","DETAIL_TEXT","DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>5, 'CODE' => $_REQUEST['ELEMENT_CODE']);
// $res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
//$res = CIBlockSection::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);

$SectList = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>5,"ACTIVE"=>"Y","CODE"=>$_REQUEST['ELEMENT_CODE']) ,false, array("ID","IBLOCK_ID","NAME","SECTION_PAGE_URL","IBLOCK_SECTION_ID"));


// if($ob = $res->GetNextElement()){
	// $arItem=$ob->GetFields();
/*if($arItem = $res->GetNext()){
	//сео-теги из админки
	$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(
        $arItem["IBLOCK_ID"], // ID инфоблока
        $arItem["ID"] // ID элемента
    );
	$arElMetaProp = $ipropValues->getValues();
	if(strlen($arElMetaProp['ELEMENT_META_TITLE'])>0)
 		$APPLICATION->SetPageProperty("title", $arElMetaProp['ELEMENT_META_TITLE']);
	if(strlen($arElMetaProp['ELEMENT_META_KEYWORDS'])>0)
 		$APPLICATION->SetPageProperty("keywords", $arElMetaProp['ELEMENT_META_KEYWORDS']);
	if(strlen($arElMetaProp['ELEMENT_META_DESCRIPTION'])>0)
		 $APPLICATION->SetPageProperty("description", $arElMetaProp['ELEMENT_META_DESCRIPTION']);
	$db_old_groups = CIBlockElement::GetElementGroups($arItem['ID'], false);
	while($ar_group = $db_old_groups->Fetch()) {
		$ress = CIBlockSection::GetByID($ar_group['ID']);
		$ar_ress = $ress->GetNext();
		if (!empty($ar_ress['IBLOCK_SECTION_ID'])) {
      $r2 = CIBlockSection::GetByID($ar_ress['IBLOCK_SECTION_ID']);
      $ar2 = $r2->GetNext();
      $APPLICATION->AddChainItem($ar2['NAME'], $ar2['SECTION_PAGE_URL']);
    }
		$APPLICATION->AddChainItem($ar_group['NAME'], $ar_ress['SECTION_PAGE_URL']);
	}	 
	$APPLICATION->AddChainItem($arItem['NAME'], "");
	$APPLICATION->SetTitle($arItem["NAME"]);
}
else */
if ($SectListGet = $SectList->GetNext()){
	$sectionIsIt = "Y";
	$sectionId = $SectListGet['ID'];
		if (!empty($SectListGet['IBLOCK_SECTION_ID'])) {
      $r2 = CIBlockSection::GetByID($SectListGet['IBLOCK_SECTION_ID']);
      $ar2 = $r2->GetNext();
      $APPLICATION->AddChainItem($ar2['NAME'], $ar2['SECTION_PAGE_URL']);
    }
	$APPLICATION->SetPageProperty("title", $SectListGet['NAME']);
	$APPLICATION->AddChainItem($SectListGet['NAME'], "");
	$APPLICATION->SetTitle($SectListGet['NAME']);
} 
else{
	define(ERROR_404, "Y");
}
// if (!empty($_GET['traced'])) var_dump('<pre>',$SectListGet,'</pre>');
?>
<? if($sectionIsIt != "Y"){ ?>
<?$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arItem["NAME"].'" />',true)?>
<?$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arItem['DETAIL_TEXT'].'" />',true)?>
<?$APPLICATION->AddHeadString('<meta property="og:type" content="website" />',true)?>
<?$APPLICATION->AddHeadString('<meta property="og:url" content="'.SITE_URL_WITH_HTTP.$arItem['DETAIL_PAGE_URL'].'" />',true)?>
<?$APPLICATION->AddHeadString('<meta property="og:image" content="'.SITE_URL_WITH_HTTP.CFile::GetPath($arItem["PREVIEW_PICTURE"]).'" />',true)?>
<?}?>
<div class="container">
	<div class="row">
		<?
		$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				"WITHOUT_ROW" => "Y",
				"section__margin"=>"section__margin",
				"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
			  )
		);
		?>
		<!--
		<div class="col-12 col-lg-6">
			<img src="<?=CFile::GetPath($arItem['DETAIL_PICTURE']);?>" alt="изображение категории" class="img-responsive" alt="">
		</div>
		<div class="col-12 col-lg-6 pod-page-player column-space-between">
			<p class="pod-info"><?=$arItem['DETAIL_TEXT'];?></p>
			<div class="panel__footer">
				<div class="panel__social social">
					<span>Поделиться:</span>
					<ul class="social__list">
						<li class="social__item">
							<a  onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?=SITE_URL_WITH_HTTP.$arItem['DETAIL_PAGE_URL'];?>','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
						</li>
						<li class="social__item">
							<a onclick="window.open('http://vk.com/share.php?url=<?=SITE_URL_WITH_HTTP.$arItem['DETAIL_PAGE_URL'];?>&image=<? echo SITE_URL_WITH_HTTP.$arItem["PREVIEW_PICTURE"]['SRC']?>&noparse=true','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
								<i class="fa fa-vk" aria-hidden="true"></i>
							</a>
						</li>
						<li class="social__item">
							<a onclick="window.open('http://twitter.com/share?url=<?=SITE_URL_WITH_HTTP.$arItem['DETAIL_PAGE_URL'];?>&image-src=<? echo SITE_URL_WITH_HTTP.$arItem["PREVIEW_PICTURE"]['SRC']?>','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
								<i class="fa fa-twitter" aria-hidden="true"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<hr>
		-->
	</div>
    <div class="section mb-5 categories-content" data-sectionId="<?= $sectionId ?>">
			<?
      if($sectionIsIt == "Y") {
        $_REQUEST['ID'] = $sectionId;
        $_REQUEST['HIDDEHEAD'] = 'Y';
        $_REQUEST['TEMPLIN'] = 'Y';
        include $_SERVER['DOCUMENT_ROOT'].'/ajax/categories.php';
      }
			?>
    </div>
	<?
	$GLOBALS['FILTER_MAIN_CATALOG'] = array();
	//$GLOBALS['FILTER_PROGRAM']["PROPERTY_MAIN_PODCAST"]=false;
	$ids_cat = array($sectionId);
	if($sectionIsIt == "Y"){
		// $arFilter = Array("IBLOCK_ID" => 5,"IBLOCK_SECTION_ID" => $sectionId, "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y");
		// $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
		// while($ob = $res->GetNextElement())
		// {
		 // $arFields = $ob->GetFields();
		 // $ids_cat[] =$arFields['ID'];
		// }
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_ID" => 5,"SECTION_ID" => $sectionId, "ACTIVE" => "Y");
    $res = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, false, $arSelect);
		while($ob = $res->GetNext()) {
		 $ids_cat[] =$ob['ID'];
		}
		// $GLOBALS['FILTER_PROGRAM']["PROPERTY_CATEGORIES"]=$ids_cat;
	}
	$INCLUDE_SUBSECTIONS = "Y";
	if ($sectionId == 47) $INCLUDE_SUBSECTIONS = "N";
	$GLOBALS['FILTER_PROGRAM']["PROPERTY_CATS"] = $ids_cat;
	if ($sectionId == 47) $GLOBALS['FILTER_PROGRAM']["PROPERTY_CATS"] = array($sectionId);
	$APPLICATION->IncludeComponent("hiddenfaces:news.list","podcasts_recommended",
		Array(
			"PODCASTS_TITLE" => "Выпуски:",
			"SHOW_BANNERS" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"AJAX_MODE" => "N",
			"FILTER_NAME" => "FILTER_PROGRAM",
			"IBLOCK_TYPE" => "hidden_faces",
			"IBLOCK_ID" => "3",
			"NEWS_COUNT" => "9",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "SORT",
			"SORT_ORDER2" => "ASC",
			"FIELD_CODE" => Array("ID","DATE_CREATE","CREATED_BY","ACTIVE_FROM"),
			"PROPERTY_CODE" => Array("DESCRIPTION","PODCAST","WAVEFORM_1","WAVEFORM_2",'TIME_LENGTH',"CATEGORIES","CATS","GUESTS","COVER_VARIANTS","EIGHTEEN","AUTHORS","SOAUTHORS", "SEASON"), 
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"SET_TITLE" => "N",
			"SET_BROWSER_TITLE" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_LAST_MODIFIED" => "Y",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => $INCLUDE_SUBSECTIONS,
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600",
			"CACHE_FILTER" => "Y",
			"CACHE_GROUPS" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "",
			"PAGER_SHOW_ALWAYS" => "Y",
			"PAGER_TEMPLATE" => "",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "Y",
			"PAGER_BASE_LINK_ENABLE" => "Y",
			"SET_STATUS_404" => "Y",
			"SHOW_404" => "Y",
			"MESSAGE_404" => "",
			"PAGER_BASE_LINK" =>$SectListGet['SECTION_PAGE_URL'],
			"PAGER_PARAMS_NAME" => "arrPager",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_ADDITIONAL" => ""
		)
	);
?>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>