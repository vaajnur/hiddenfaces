<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Платные подкасты - студия Скрытые лица");
?>

<div class="container inner__container">
	<div class="row guests">
		<?
		$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				 "WITHOUT_ROW"=>"Y",
				"section__margin"=>"section__margin",
				"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
			  )
		);
		?>
		<?
		$APPLICATION->IncludeComponent("hiddenfaces:news.list","shop",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"FILTER_NAME" => "FILTER_PROGRAM",
				"IBLOCK_TYPE" => "hidden_faces",
				"IBLOCK_ID" => "12",
				"NEWS_COUNT" => "12",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FIELD_CODE" => Array("ID","DATE_CREATE","CREATED_BY","ACTIVE_FROM"),
				"PROPERTY_CODE" => Array("DESCRIPTION","STATUS","SHORT_DESCRIPTION"), 
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_LAST_MODIFIED" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"PAGER_BASE_LINK_ENABLE" => "Y",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
				"MESSAGE_404" => "",
				"PAGER_BASE_LINK" =>$_SERVER['REQUEST_URI'],
				"PAGER_PARAMS_NAME" => "arrPager",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
			)
		);
		?>
		<div class="modal buy-form" tabindex="-1" aria-hidden="true">
			<div class="bg-close-modal"></div>
			<div class="modal-container">
				<div class="modal-content">
					<div class="modal-close"><svg enable-background="new 0 0 21.9 21.9" version="1.1" viewBox="0 0 21.9 21.9" xmlns="http://www.w3.org/2000/svg"><path d="M14.1,11.3c-0.2-0.2-0.2-0.5,0-0.7l7.5-7.5c0.2-0.2,0.3-0.5,0.3-0.7s-0.1-0.5-0.3-0.7l-1.4-1.4C20,0.1,19.7,0,19.5,0  c-0.3,0-0.5,0.1-0.7,0.3l-7.5,7.5c-0.2,0.2-0.5,0.2-0.7,0L3.1,0.3C2.9,0.1,2.6,0,2.4,0S1.9,0.1,1.7,0.3L0.3,1.7C0.1,1.9,0,2.2,0,2.4  s0.1,0.5,0.3,0.7l7.5,7.5c0.2,0.2,0.2,0.5,0,0.7l-7.5,7.5C0.1,19,0,19.3,0,19.5s0.1,0.5,0.3,0.7l1.4,1.4c0.2,0.2,0.5,0.3,0.7,0.3  s0.5-0.1,0.7-0.3l7.5-7.5c0.2-0.2,0.5-0.2,0.7,0l7.5,7.5c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l1.4-1.4c0.2-0.2,0.3-0.5,0.3-0.7  s-0.1-0.5-0.3-0.7L14.1,11.3z"/></svg></div>
					<p class="modal-title">Заголовок модалки</p>
					<div class="buy__modal__subtitle">Укажите Ваши контакты:</div>
					<form action="">
						<input class="name form__input name" type="name" placeholder="Ваше имя">
						<input class="name form__input phone" type="phone" placeholder="Ваш телефон">
						<input class="product-id" type="hidden">
						<button class="write-order header__button btn">Отправить</button>
					</form>
					<br>
					<label for="personal-form" class="label__checkbox">Согласие на обработку персональных данных
						<input type="checkbox" class="check" id="personal-form" checked="">
						<span class="checkmark"></span>
					</label>
					<br>
					<br>
					<div class="buy__modal__info">
						<div style="display:none;" class="text-center success-text">С вами свяжется менеджер в течение часа</div>
						<!-- <div class="text-center">Оставьте свои данные и мы свяжемся с Вами в короткие сроки</div> -->
					</div>
				</div>
			</div>
		</div>
</div>
</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>