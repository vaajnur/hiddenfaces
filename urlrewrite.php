<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/peredachi/page([0-9]+)/#",
		"RULE" => "PAGEN_1=\$1",
		"ID" => "",
		"PATH" => "/peredachi/index.php",
	),
	array(
		"CONDITION" => "#^/kategorii/([a-z0-9_-]+)/page([0-9]+)/#",
		"RULE" => "ELEMENT_CODE=\$1&PAGEN_2=\$2",
		"ID" => "",
		"PATH" => "/kategorii/index.php",
	),
	array(
        "CONDITION" => '#^/kategorii/([/\-_.a-zA-Z0-9]+)/(.*?)/page([0-9]+)/#',
        "RULE" => "SECTION_CODE_PATH=$1&ELEMENT_CODE=$2&CODE=$2&PAGEN_2=\$3",
        "ID" => "",
        "PATH" => "/kategorii/index.php",
	),
	array(
        "CONDITION" => '#^/kategorii/([/\-_.a-zA-Z0-9]+)/(.*?)/#',
        "RULE" => "SECTION_CODE_PATH=$1&ELEMENT_CODE=$2&CODE=$2",
        "ID" => "",
        "PATH" => "/kategorii/index.php",
    ),
	array(
		"CONDITION" => "#^/kategorii/([a-z0-9_-]+)/(.*)$#",
		"RULE" => "ELEMENT_CODE=\$1&$2",
		"ID" => "",
		"PATH" => "/kategorii/index.php",
	),
	array(
		"CONDITION" => "#^/gosti/page([0-9]+)/#",
		"RULE" => "PAGEN_2=\$1",
		"ID" => "",
		"PATH" => "/gosti/index.php",
	),
	array(
		"CONDITION" => "#^/avtory/page([0-9]+)/#",
		"RULE" => "PAGEN_1=\$1",
		"ID" => "",
		"PATH" => "/avtory/index.php",
	),
	array(
		"CONDITION" => "#^/avtory/([0-9a-zA-Z_-]+)/page([0-9-]+)/(.*)#",
		"RULE" => "ELEMENT_CODE=\$1&PAGEN_5=\$2",
		"ID" => "",
		"PATH" => "/avtory/detail.php",
	),
	array(
		"CONDITION" => "#^/avtory/([0-9a-zA-Z_-]+)/(.*)$#",
		"RULE" => "ELEMENT_CODE=\$1&$2",
		"ID" => "",
		"PATH" => "/avtory/detail.php",
	),
	array(
		"CONDITION" => "#^/sitemap/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/poisk/map.php",
	),
	array(
		"CONDITION" => "#^/poisk/page([0-9]+)/#",
		"RULE" => "PAGEN_1=\$1",
		"ID" => "",
		"PATH" => "/poisk/index.php",
	),
	array(
		"CONDITION" => "#^/gosti/([a-z0-9_-]+)/page([0-9]+)/#",
		"RULE" => "ELEMENT_CODE=\$1&PAGEN_2=\$2",
		"ID" => "",
		"PATH" => "/gosti/detail.php",
	),
	array(
		"CONDITION" => "#^/gosti/([a-z0-9_-]+)/(.*)$#",
		"RULE" => "ELEMENT_CODE=\$1&$2",
		"ID" => "",
		"PATH" => "/gosti/detail.php",
	),
	array(
		"CONDITION" => "#^/gosti/page([0-9]+)/#",
		"RULE" => "PAGEN_1=\$1",
		"ID" => "",
		"PATH" => "/gosti/index.php",
	),
	array(
		"CONDITION" => "#^/gosti/([a-z0-9_-]+)/(.*)$#",
		"RULE" => "ELEMENT_CODE=\$1&$2",
		"ID" => "",
		"PATH" => "/gosti/detail.php",
	),
	array(
		"CONDITION" => "#^/peredachi/page([0-9]+)/(.*)$#",
		"RULE" => "PAGEN_1=\$1&$2",
		"ID" => "",
		"PATH" => "/peredachi/index.php",
	),
	array(
		"CONDITION" => "#^/podcast/download(.*)$#",
		"RULE" => "filename=\$1",
		"ID" => "",
		"PATH" => "/podcast/download.php",
	),
	array(
		"CONDITION" => "#^/peredachi/([0-9a-zA-Z_-]+)/page([0-9]+)/#",
		"RULE" => "SECTION_CODE=\$1&PAGEN_1=\$2&PAGEN_4=\$2&PAGEN_2=\$2&PAGEN_3=\$2&PAGEN_5=\$2", 
		"ID" => "",
		"PATH" => "/peredachi/detail.php",
	),
	array(
		"CONDITION" => "#^/peredachi/([a-z0-9_-]+)/([a-z0-9_-]+)/page([0-9]+)/$#",
		"RULE" => "ELEMENT_CODE=\$2&PAGEN_1=\$3&PAGEN_4=\$3&PAGEN_2=\$3&PAGEN_3=\$3&PAGEN_5=\$3&PAGEN_6=\$3",
		"ID" => "",
		"PATH" => "/podcast/index.php",
	),
	array(
		"CONDITION" => "#^/peredachi/([a-z0-9_-]+)/([a-z0-9_-]+)/(.*)$#",
		"RULE" => "ELEMENT_CODE=\$2&$3",
		"ID" => "",
		"PATH" => "/podcast/index.php",
	),
	array(
		"CONDITION" => "#^/peredachi/([a-z0-9_,-]+)/(.*)$#",
		"RULE" => "SECTION_CODE=\$1&$2",
		"ID" => "",
		"PATH" => "/peredachi/detail.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
);

?>