<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
$errors=0;
if(strlen($_REQUEST['email'])==0||check_email($_REQUEST['email'])==false){
    $errors++;
    echo "email-error;";
}
if(strlen($_REQUEST['name'])==0){
    echo "name-error;";
    $errors++;
}
if(strlen($_REQUEST['check'])==0){
    echo "check-error;";
    $errors++;
}
if(strlen($_REQUEST['phone'])==0){
    echo "phone-error;";
    $errors++;
}
if(strlen($_REQUEST['message'])==0){
    echo "message-error;";
    $errors++;
}
//все ок, добавляем в инфоблок и отправляем письмо
if($errors==0){
    echo "okidoki";
    CModule::IncludeModule("catalog");
    CModule::IncludeModule("iblock");
	
    //Добавляем элемент в инфоблок
    $el = new CIBlockElement;
    $PROP = array();
    $PROP[28] = $_REQUEST['email'];
    $PROP[29] = $_REQUEST['name'];
	$PROP[30] = $_REQUEST['message'];
	$PROP[31] = $_REQUEST['phone'];
	$PROP[32] = $_REQUEST['link'];
	$PROP[33] = $_REQUEST['сity'];
    $arLoadProductArray = Array(
      "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
      "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
      "IBLOCK_ID"      => 11,
      "PROPERTY_VALUES"=> $PROP,
      "NAME"           => "Новая анкета Стать Автором ".$_REQUEST['name'],
      "ACTIVE"         => "Y"
      );

    if($PRODUCT_ID = $el->Add($arLoadProductArray))
      echo "New ID: ".$PRODUCT_ID;
    else
      echo "Error: ".$el->LAST_ERROR;
    //отправляем письмо, ставим метку, что sended=1
     $arEventFields = array( 
        "EMAIL" => $_REQUEST['email'], 
        "MESSAGE" => $_REQUEST['message'],
        "NAME" => ''.$_REQUEST['name'],
		"PHONE" => ''.$_REQUEST['phone'],
		"LINK" => ''.$_REQUEST['link'],
		"CITY" => ''.$_REQUEST['сity'],
    ); 
    if (CModule::IncludeModule("main")): 
       if (CEvent::Send("NEW_ANKETA", "s1", $arEventFields,"Y",49)): 
          echo "ok<br>"; 
       endif; 
    endif;
}
?>