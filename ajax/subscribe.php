<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
require_once($_SERVER["DOCUMENT_ROOT"]."/include/unisenderApi.php"); //подключаем файл класса
$apikey=UNISENDER_API_KEY; //API-ключ к вашему кабинету
$user_list = UNISENDER_LIST;
$uni=new UniSenderApi($apikey); //создаем экземляр класса, с которым потом будем работать
CModule::IncludeModule("subscribe");
function checkEmail($email)
{
		if (!preg_match ( "/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/", $email)) {
			return FALSE;
		}	
		return TRUE;		
}
$submited=0;
$ERROR='';
if($_POST['personal']!="on"){
	echo "Не поставлена галочка согласия";
	die();
}
if($_POST['do_subscribe']=="Y")
{
	if(!checkEmail($_POST['email']))
	{
		$ERROR='Введите корректный E-mail<br>';
	}else{
		$subscription = CSubscription::GetByEmail($_POST['email']);
		$subscription->ExtractFields("str_");
		if($str_ID){ 
			$ERROR='Ваш E-mail уже находится в списке рассылок<br>';
			/*if(intval($_POST['rubric_id'])==1)
				$uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$_POST['email'],"everyday"=>"Да"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d")));
			else
				$uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$_POST['email'],"everyweek"=>"Да"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d")));*/
            $aSubscrRub = CSubscription::GetRubricArray($str_ID);
            if(!in_array($_POST['rubric_id'],$aSubscrRub)){
                array_push($aSubscrRub,$_POST['rubric_id']);
                $arFields_sub = Array(
					"NAME" => $_POST['name'],
                    "RUB_ID" => $aSubscrRub
                );
                $subscr = new CSubscription;
                if($subscr->Update($str_ID,$arFields_sub,"s1"))
                { $submited=1; echo 'ok';  die(); }
            }
		}
	}
	if(!$ERROR)
	{
		$arFields = Array(
			"USER_ID" => ($USER->IsAuthorized()? $USER->GetID():false),
			"FORMAT" => ($FORMAT <> "html"? "text":"html"),
			"EMAIL" => $_POST['email'],
			"NAME" => $_POST['name'],
			"CONFIRMED" => "Y",
      		"SEND_CONFIRM" => "N",
			"ACTIVE" => "Y",
			"RUB_ID" => isset($_POST['rubric_id'])?array($_POST['rubric_id']):array(1)
		);
		$subscr = new CSubscription;
		$ID = $subscr->Add($arFields);
		if($ID>0)
			CSubscription::Authorize($ID);
		else
			$strWarning .= "Ошибка при добавлении рассылки: ".$subscr->LAST_ERROR."";
		$submited=1;
		//если подписаны на блог - ставим сегмент подписчик блога
        /*if(intval($_POST['rubric_id'])==1)
            $uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$_POST['email'],"everyday"=>"Да"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d")));
        else
            $uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$_POST['email'],"everyweek"=>"Да"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d")));*/
	}
}
echo $ERROR;
if($_REQUEST['unsubscribe']=='Y')
{
	$res = CSubscription::Delete($str_ID);
	echo 'Рассылка удалена.';
}

if($submited){
	echo 'ok'; 
}