<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");
global $USER;
$res = CIBlockElement::GetByID($_REQUEST['ID']);
if($obRes = $res->GetNextElement())
{
  $ar_res = $obRes->GetProperty("LIKES");
	print_r($ar_res);
	if($_REQUEST['LIKE']=="Y"){
		CIBlockElement::SetPropertyValues($_REQUEST['ID'], 3,intval($ar_res['VALUE'])+1, "LIKES");
	}
	else{
		CIBlockElement::SetPropertyValues($_REQUEST['ID'], 3,intval($ar_res['VALUE'])-1, "LIKES");
	}
}
if($_REQUEST['LIKE']=="Y"){
	//неавторизованному пишем в куки что лайкнул подкаст, авторизованному в поле "понравившиеся подкасты"
	if(!$USER->IsAuthorized()){
		setcookie("PODCAST_LIKED_".$_REQUEST['ID'], "Y", time()+3600000, "/"); 
	}
	else{
		$rsUser = CUser::GetByID($USER->GetID()); 
		$arUser = $rsUser->Fetch();
		$arPodcasts = array($_REQUEST['ID']);
		if(count($arUser['UF_LIKED_PODCASTS'])>0){
			$arPodcasts=array_merge($arPodcasts,$arUser['UF_LIKED_PODCASTS']);
		}
		$user = new CUser;
		$fields = Array( 
		"UF_LIKED_PODCASTS" => $arPodcasts, 
		); 
		$user->Update($USER->GetID(), $fields);
	}
}
else{
	//неавторизованному пишем в куки что лайкнул подкаст, авторизованному в поле "понравившиеся подкасты"
	if(!$USER->IsAuthorized()){
		setcookie("PODCAST_LIKED_".$_REQUEST['ID'], "N", time()+3600000, "/"); 
	}
	else{
		$rsUser = CUser::GetByID($USER->GetID()); 
		$arUser = $rsUser->Fetch();
		$arPodcasts = array($_REQUEST['ID']);
		if(count($arUser['UF_LIKED_PODCASTS'])>0){
			
			$key = array_search($_REQUEST['ID'], $arUser['UF_LIKED_PODCASTS']);
			if ($key !== false)
			{
				unset($arUser['UF_LIKED_PODCASTS'][$key]);
			}
		}
		$user = new CUser;
		$fields = Array( 
		"UF_LIKED_PODCASTS" => $arUser['UF_LIKED_PODCASTS'], 
		); 
		$user->Update($USER->GetID(), $fields);
	}
}
?>