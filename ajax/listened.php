<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");
global $USER;
//неавторизованному пишем в куки что прослушан подкаст, авторизованному в поле "прослушанные подкасты"
$res = CIBlockElement::GetByID($_REQUEST['ID']);
if($obRes = $res->GetNextElement())
{
  $ar_res = $obRes->GetProperty("LISTENS");
	CIBlockElement::SetPropertyValues($_REQUEST['ID'], 3,intval($ar_res['VALUE'])+1, "LISTENS");
}
if(!$USER->IsAuthorized()){
	setcookie("PODCAST_LISTENED_".$_REQUEST['ID'], "Y", time()+3600000, "/"); 
}
else{
	$rsUser = CUser::GetByID($USER->GetID()); 
	$arUser = $rsUser->Fetch();
	$arPodcasts = array($_REQUEST['ID']);
	if(count($arUser['UF_LISTENED_PODCASTS'])>0){
		$arPodcasts=array_merge($arPodcasts,$arUser['UF_LISTENED_PODCASTS']);
	}
	$user = new CUser;
	$fields = Array( 
	"UF_LISTENED_PODCASTS" => $arPodcasts, 
	); 
	$user->Update($USER->GetID(), $fields);
} ?>