<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
$errors=0;
if(strlen($_REQUEST['email'])==0||check_email($_REQUEST['email'])==false){
    $errors++;
    echo "email-error;";
}
if(strlen($_REQUEST['name'])==0){
    echo "name-error;";
    $errors++;
}
if(strlen($_REQUEST['check'])==0){
    echo "check-error;";
    $errors++;
}
if(strlen($_REQUEST['message'])==0){
    echo "message-error;";
    $errors++;
}
//все ок, добавляем в инфоблок и отправляем письмо
if($errors==0){
    echo "okidoki";
    CModule::IncludeModule("catalog");
    CModule::IncludeModule("iblock");
	$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_CML2_MANUFACTURER");
    $arFilter = Array("IBLOCK_ID"=>3, "ID"=>$_REQUEST['podcast_id']);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
    while($ob = $res->GetNextElement())
    {
     $arFields = $ob->GetFields();
     $name_product=$arFields['NAME'];
    }
    //Добавляем элемент в инфоблок
    $el = new CIBlockElement;
    $PROP = array();
    $PROP[16] = $_REQUEST['email'];
    $PROP[17] = $_REQUEST['name'];
    $PROP[18] = $_REQUEST['message'];
    $arLoadProductArray = Array(
      "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
      "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
      "IBLOCK_ID"      => 8,
      "PROPERTY_VALUES"=> $PROP,
      "NAME"           => "Сообщение для автора подкаста ".$name_product." от ".$_REQUEST['name'],
      "ACTIVE"         => "Y"
      );

    if($PRODUCT_ID = $el->Add($arLoadProductArray))
      echo "New ID: ".$PRODUCT_ID;
    else
      echo "Error: ".$el->LAST_ERROR;
    //отправляем письмо, ставим метку, что sended=1
     $arEventFields = array( 
        "EMAIL" => $_REQUEST['email'], 
        "MESSAGE" => $_REQUEST['message'],
        "NAME" => ''.$_REQUEST['name'],
        "ADDRESS" => $_REQUEST['adres'],
        "PODCAST" => $name_product,
    ); 
    if (CModule::IncludeModule("main")): 
       if (CEvent::Send("SEND_AUTHOR_MESSAGE", "s1", $arEventFields,"Y",46)): 
          echo "ok<br>"; 
       endif; 
    endif;
}
?>