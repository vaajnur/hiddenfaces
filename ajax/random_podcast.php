<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");

$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y");
$arSelect = Array();
$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
if($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arProps = $ob->GetProperties();
	?>
	<h4 class="header__subtitle"><a href="<?=$arFields['DETAIL_PAGE_URL'];?>"><?=$arFields['NAME'];?></a></h4>
	<div class="header__podcast">
		<? if(count($arProps['COVER_VARIANTS']['VALUE'])>1){?>
		<? $file = CFile::ResizeImageGet($arProps['COVER_VARIANTS']['VALUE'][rand(0,count($arProps['COVER_VARIANTS']['VALUE'])-1)], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
		<div class="cover-disc" style="background-image:url(<?=$file['src'];?>);"></div>
		<?}
		else{?>
		<? $file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
		<div class="cover-disc" style="background-image:url(<?=$file['src'];?>);"></div>
		<?}?>
		
		
	  <button class="player__button player__button-header" id="button1"></button>
	  <audio class="html5-player" controls="controls" preload="none">
	  	<source src="<?=CFile::GetPath($arProps['PODCAST']['VALUE']);?>" type="audio/mpeg" /> Your browser does not support the audio element.
	  </audio>
	  <div class="player__audio" id="player1" style="display: none"></div>
	</div>
<?
}
?>