<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?

$errors=0;
if(strlen($_REQUEST['name'])==0){
    echo "name-error;";
    $errors++;
}
if(strlen($_REQUEST['phone'])==0){
    echo "phone-error;";
    $errors++;
}
if(strlen($_REQUEST['check'])==0){
    echo "check-error;";
    $errors++;
}

//все ок, добавляем в инфоблок и отправляем письмо
if($errors==0){
    echo "okidoki";
    CModule::IncludeModule("catalog");
    CModule::IncludeModule("iblock");
	
    $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_CML2_MANUFACTURER");
	$arFilter = Array("IBLOCK_ID"=>3, "ID"=>$_REQUEST['productid']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
	while($ob = $res->GetNextElement())
	{
	 $arFields = $ob->GetFields();
	 $name_product="<a href='".$arFields['DETAIL_PAGE_URL']."'>".$arFields['NAME']."</a>";
	}
    //отправляем письмо, ставим метку, что sended=1
     $arEventFields = array( 
        "NAME" => $_REQUEST['name'], 
        "PHONE" => $_REQUEST['phone'],
		 "PRODUCT" => $name_product
    ); 
    if (CModule::IncludeModule("main")): 
       if (CEvent::Send("ORDER_SHOP", "s1", $arEventFields,"Y",51)): 
          echo "ok<br>"; 
       endif; 
    endif;
}
?>