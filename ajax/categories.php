<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");

$sections = array();
$categories = array();
// если $ID не задан или это не число, тогда 
// $ID будет =0, выбираем корневые разделы
$ID = IntVal($_REQUEST['ID']);
$PID = IntVal($_REQUEST['PID']);
// выберем папки из информационного блока $BID и раздела $ID
$items = GetIBlockSectionList(5, $ID, Array("sort"=>"asc"), 12);
while($arItem = $items->GetNext())
{
  $arItem['_subCheck'] = false;
  $itemsIn = GetIBlockSectionList(5, $arItem['ID'], Array("sort"=>"asc"), 12);
  while($arItemIn = $itemsIn->GetNext()) $arItem['_subCheck'] = 'Y';
  $sections[$arItem['ID']] = $arItem;
  // Корневые выводим все
  /*
  if ($ID == 0) {
    $sections[$arItem['ID']] = $arItem;
  } else {
    // $arItem['_subCheck'] = 'Y';
    $arSelect = Array("ID", "NAME", "PROPERTY_ICON","SECTION_PAGE_URL");
    $arFilter = Array("IBLOCK_ID"=>5,"IBLOCK_SECTION_ID"=>$arItem['ID'], "ACTIVE"=>"Y");
    // $res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
    $res = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, false, $arSelect);
    while($arFields = $res->GetNext()){
        // $arFields = $ob->GetFields();
        // $arItem['_subCheck'] = false;
        $arSelect_item = Array("ID","NAME");
        $arFilter_item = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'PROPERTY_CATS'  => array($arFields['ID']));
        $res_item = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC","SORT"=>"ASC"), $arFilter_item, false, Array("nPageSize"=>1), $arSelect_item);
        if($ob_item = $res_item->GetNextElement()){
            $sections[$arItem['ID']] = $arItem;
        }
    }
  }
  */
}
/*
$arSelect = Array("ID", "NAME", "PROPERTY_ICON","DETAIL_PAGE_URL","IBLOCK_SECTION_ID");
$arFilter = Array("IBLOCK_ID"=>5,"IBLOCK_SECTION_ID"=>$ID, "ACTIVE"=>"Y");
if($ID == 0){
    $arFilter = Array("IBLOCK_ID"=>5,"IBLOCK_SECTION_ID"=>"", "ACTIVE"=>"Y");
}
// $res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    if($ID == 0 && $arFields['IBLOCK_SECTION_ID'] != ""){ continue; }
    $arSelect_item = Array("ID","NAME");
    $arFilter_item = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'PROPERTY_CATEGORIES'  => array($arFields['ID']));
    $res_item = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC","SORT"=>"ASC"), $arFilter_item, false, Array("nPageSize"=>1), $arSelect_item);
    if($ob_item = $res_item->GetNextElement()){ 
        $categories[] = $arFields;
	  }
}
*/
?>
<div class="container categories-container" data-id="<?=$ID;?>">
  <? if (empty($_REQUEST['TEMPLIN'])) { ?>
	<h2 class="section__title">
		Категории
        <?
        if($ID > 0){
            $navChain = CIBlockSection::GetNavChain(5, $ID); 
            while ($arNav=$navChain->GetNext()):
                 ?>
                 <img src="/local/templates/hiddenfaces/img/category-cards/arrow.svg" alt="" class="category-arrow-icon"><?=$arNav['NAME'];?>
		        <br class="d-md-none">
            <?
            endwhile;
        }?>
	</h2>
  <? } ?>
	<div class="category-card-row row">
        <? foreach($sections as $section){?>
        <div class="category-card-col col-4 col-md-2">
			<div class="category-card card">
				<div class="card-body">
					<div class="icon" style="background-image: url(<?=CFile::GetPath($section['PICTURE']);?>);"></div>
          <? if (!empty($_REQUEST['TEMPLIN']) or empty($section['_subCheck'])) { ?>
					<h3 class="title"><a href="<?=$section['SECTION_PAGE_URL'];?>" class="stretched-link category-parent" data-id="<?=$section['ID'];?>"><?=$section['NAME'];?></a></h3>
          <? } else { ?>
					<h3 class="title"><a onclick="loadCategory(<?=$section['ID'];?>,<?=$ID;?>)" href="javascript:void(0);" class="stretched-link category-parent" data-id="<?=$section['ID'];?>"><?=$section['NAME'];?></a></h3>
          <? } ?>
				</div>
			</div>
		</div>
        <?}?>
        <? /*foreach($categories as $category){?>
        <div class="category-card-col col-4 col-md-2">
			<div class="category-card card">
				<div class="card-body">
					<div class="icon" style="background-image: url(<?=CFile::GetPath($category['PROPERTY_ICON_VALUE']);?>);"></div>
					<h3 class="title"><a href="<?=$category['DETAIL_PAGE_URL'];?>" class="stretched-link"><?=$category['NAME'];?></a></h3>
				</div>
			</div>
		</div>
        <?}*/?>
	</div>
  <? if (empty($_REQUEST['TEMPLIN'])) { ?>
    <? if($ID > 0){?>
	<a href="javascript:void(0);" data-back="<?=$PID;?>" onclick="loadCategory(<?=$PID;?>,0)" class="category-back">
		<img src="/local/templates/hiddenfaces/img/category-cards/arrow_back.svg" alt="" class="category-back-icon">
		Вернуться
	</a>
    <?}?>
  <?}?>
</div>
  <? if (empty($_REQUEST['TEMPLIN'])) { ?>
<script>
function loadCategory(id, pid){
	$.ajax({
		type:"POST",
		url:"/ajax/categories.php",
		data:{
			ID:id,
			PID:pid
		},
		success: function(response){
			$(".categories-content").html(response);
		}
	});
}
</script>
  <?}?>