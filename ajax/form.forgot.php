<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $USER;
$recaptcha="N";
if (empty($_REQUEST['g-recaptcha-response'])) {
    echo "recaptcha-error";
    die();
}
$url = 'https://www.google.com/recaptcha/api/siteverify';

$recaptcha = $_REQUEST['g-recaptcha-response'];
$secret = '6LcHWVAUAAAAAJNiViAZIVLBXVG-BEeFSW_OUT9D';
$ip = $_SERVER['REMOTE_ADDR'];

$url_data = $url.'?secret='.$secret.'&response='.$recaptcha.'&remoteip='. $ip;

$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, $url_data);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$res = curl_exec($curl);
curl_close($curl);

$res = json_decode($res);
if($res->success) {
    $recaptcha="Y";
}
else {
    echo "recaptcha-error";
    die();
}
if(strlen($_REQUEST['login'])>0&&$recaptcha=="Y"){
if (!is_object($USER)) $USER = new CUser;

    $UserLogin = $_REQUEST['login'];
	$cUser = new CUser; 
	$sort_by = "ID";
	$sort_ord = "ASC";
	$arFilter = array(
	   "=EMAIL" => $_REQUEST['login']
	);
	$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
	while ($arUser = $dbUsers->Fetch()) 
	{
	   $UserLogin = $arUser["LOGIN"];
	}
    $rsUser = CUser::GetByLogin($UserLogin);
    if($arUser = $rsUser->Fetch())
    {
         $fields["login"]=$UserLogin;
         $arResult = $USER->SendPassword($UserLogin, $UserLogin);
         if($arResult["TYPE"] == "OK"){
            echo explode("@",$arUser["EMAIL"])[1];
         } 
    } 
    else {
         echo 'login-error';
    }
}
else{
    echo 'login-error';
}
?>