<section class="subscribe" id="sub-banners">
	<div class="row">
		<div class="col-12 col-lg-6">							
			<div class="mainpage__shadowpanel mainpage__banner">
				<a href="/gosti/"><img src="<?=SITE_TEMPLATE_PATH?>/img/content/main-banner-2.png" class="img-responsive" alt=""></a>
			</div>
		</div>
		<div class="col-12 col-lg-6">							
			<div class="mainpage__shadowpanel mainpage__banner author__panel">
				<h2 class="title">СЛЕДИТЕ ЗА НАМИ<br>ВКОНТАКТЕ</h2>
				<p class="text">Слушайте первыми!<br>Комментируйте и делитесь с друзьями!</p>
				<a href="https://vk.com/hiddenfacesru" target="_blank" class="btn">Присоединиться</a>
			</div>
		</div>
	</div>
</section>