<section class="subscribe" id="subscribe">
	<div class="row <?=$arParams['section__margin'];?>">
		<div class="col-12 col-lg-6">							
			<div class="subscribe__panel mainpage__shadowpanel">
				<h4 class="subscribe__title">Получайте анонсы новых подкастов в удобном ритме</h4>
				<?$APPLICATION->IncludeComponent(
					"hiddenfaces:subscribe.form",
					"main_subscribe_form",
					Array(
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"CODE_FORM" => "MAINPAGE",
						"PAGE" => "/ajax/subscribe.php",
						"RUBRIC_ID" => 1,
						"SHOW_HIDDEN" => "Y",
						"USE_PERSONALIZATION" => "Y"
					)
				);?>
			</div>
		</div>
		<div class="col-12 col-lg-6">
			<div class="instagram mainpage__shadowpanel">
				<h4 class="instagram__title">Подписывайтесь <br>на наш инстаграм: <br><a target="_blank" href="https://www.instagram.com/podcaststudio/"class="instagram__link">@podcaststudio</a></h4>
				<div class="instagram__row">
					<? $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE");
					$arFilter = Array("IBLOCK_ID"=>9,"ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, Array("nPageSize"=>8), $arSelect);
					$photos=array();
					while($ob = $res->GetNextElement()){
						$fields=$ob->GetFields();
						$photos[]=CFile::GetPath($fields['PREVIEW_PICTURE']);
					}?>
					<div class="item" style="background-image:url(<?=$photos[0]?>)"></div>
					<div class="item"></div>
					<div class="item" style="background-image:url(<?=$photos[1]?>)"></div>
					<div class="item" style="background-image:url(<?=$photos[2]?>)"></div>
					<div class="item"></div>
					<div class="item" style="background-image:url(<?=$photos[3]?>)"></div>
					<div class="item"></div>
					<div class="item" style="background-image:url(<?=$photos[4]?>)"></div>
					<div class="item" style="background-image:url(<?=$photos[5]?>)"></div>
					<div class="item"></div>
					<div class="item" style="background-image:url(<?=$photos[6]?>)"></div>
					<div class="item"></div>
				</div>
			</div>
		</div>
	</div>
</section>