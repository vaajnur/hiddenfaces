<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как стать автором подкаст студии «Скрытые лица»");
?>

<div class="container inner__container">
	<div class="row guests">
		<h1 class="col-12 no-bot-wrap">Как стать автором</h1>
		<div class="bread col-12">
			<a href="/">Главная</a>
			<span> > </span>
			<a>Как стать автором подкаст студии «Скрытые лица»</a>
		</div>
		<div class="col-12">
			<p class="author-p">Добро пожаловать в семью «Скрытых лиц»!</p>
			<br />
			<p class="author-p">Чтобы стать автором и размещать свои подкасты на нашем сайте, нужно совсем немного.</p>
			<br />
			<ul class="iphone-ul">
				<li>1. Разделять наши ценности и настроения</li>
				<li>2. Заполнить короткую анкету</li>
				<li>3. Прислать ссылку на свой подкаст</li>
			</ul>
			<br />
			<p class="author-p">Это бесплатно. Мы ответим вам в течение трех дней. </p>
			<br />
			<hr>
			<form action="" class="back-form">
				<div class="row align-items-stretch">
					<h2 class="author-title col-12">Анкета:</h2>
					<div class="sm-space"></div>
					<div class="col-6">
						<input class="name" type="text" placeholder="Введите Ф.И.О.">
						<input class="phone" type="text" placeholder="Введите номер телефона">
					</div>
					<div class="col-6">
						<textarea class="message" rows="3" placeholder="О чем будут выпуски ваших подкастов?"></textarea>
					</div>
					<div class="sm-space"></div>
					<div class="col-6">
						<input class="email" type="text" placeholder="Введите E-Mail">
					</div>
					<div class="col-6">
						<input class="city" type="text" placeholder="Укажите ваш город">
					</div>
					<div class="sm-space"></div>
					<div class="col-12">
						<p class="author-p">Запишите небольшой подкаст на 5-10 минут на интересную для вас тему:</p>
						<input class="link" type="text" placeholder="Вставьте ссылку на подкаст">
					</div>
					<div class="col-12">
						<div class="sm-space"></div>
						<label for="personal-form" class="label__checkbox">Согласие на обработку персональных данных
							<input type="checkbox" class="check" id="personal-form" checked="">
							<span class="checkmark"></span>
						</label>
						<button class="header__button write-anketa btn">Отправить</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>