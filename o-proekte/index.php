<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О проекте");
?>
<div class="container inner__container">
	<div class="row guests">
		<h1 class="col-12 no-bot-wrap">О проекте</h1>
		<div class="bread col-12">
			<a href="/">Главная</a>
			<span> > </span>
			<a>О проекте</a>
		</div>
		<div class="col-lg-7">
			<h4 class="header__subtitle">Скрытые Лица - видео</h4>
			<video id="video1" controls="">
				<source src="../img/HF_edit.mp4" type="video/mp4">
				Your browser does not support HTML5 video.
			</video>
		</div>
	  <div class="col-lg-5 header__column header__column-right">
        	<?
			$arSelect = Array("ID", "NAME");
			$arFilter = Array("IBLOCK_ID"=>3,"!PROPERTY_MAIN_PODCAST"=>false, "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			if($ob = $res->GetNextElement()){
				$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y","ID"=>19); 
			}
			else{
				$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y","ID"=>19);  
			}
			$arSelect = Array();
			$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			if($ob = $res->GetNextElement()){ 
				$arFields = $ob->GetFields();  
				$arProps = $ob->GetProperties();
				?>
				
				<h4 class="header__subtitle"><?=$arFields['NAME'];?></h4>
				<div class="header__podcast">
					<? if(count($arProps['COVER_VARIANTS']['VALUE'])>1){?>
					<? $file = CFile::ResizeImageGet($arProps['COVER_VARIANTS']['VALUE'][rand(0,count($arProps['COVER_VARIANTS']['VALUE'])-1)], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
					<div class="cover-disc" style="background-image:url(<?=$file['src'];?>);"></div>
					<?}
					else{?>
					<? $file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
					<div class="cover-disc" style="background-image:url(<?=$file['src'];?>);"></div>
					<?}?>
					
					
				  <button class="player__button player__button-header" id="button1"></button>
				  <audio class="html5-player" controls="controls" preload="none">
				  	<source src="<?=CFile::GetPath($arProps['PODCAST']['VALUE']);?>" type="audio/mpeg" /> Your browser does not support the audio element.
				  </audio>
				  <div class="player__audio" id="player1" style="display: none"></div>
				</div>
          <?
			}
			?>
        </div>
		<h2 class="col-12">Подкаст Студия «Скрытые лица»</h2>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/3.jpg?anticache=123" alt="">
				</div>
				<div>
					<p class="guest-title">Мария Павлович</p>
					<span>Основатель студии "Скрытые лица"</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/1.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Анна Ловчева</p>
					<span>Главный звукорежиссер</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/9.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Дарья Альшевская</p>
					<span>Звукорежиссер</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/11.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Кристина Сахарова</p>
					<span>Главный редактор</span>
				</div>
			</div>
		</div>
		<h2 class="col-12">Авторы Подкастов</h2>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/4.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Владимир Марковский</p>
					<span>Трейдер, путешественник.</span>
					<span>«Люблю новые впечатления, да и старые тоже, если приятные. Вода, но чаще вино или виски.»</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/5.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Павел Эйлер</p>
					<span>Художник-постановщик, печник, краевед, книжный червь.</span>
					<span>«Родился 22 сентября 1975 года, в Москве, в доме номер 10 по Большой Садовой улице (302 БИС). Мужчина. Русский. Знаю историю моих предков дальше времен Иоанна Грозного. Коренной москвич. Монархист. Империалист. Книгу люблю - больше чем шмотку. Зверя и птицу - больше человека. Природа Мой Дом. Россия мое Отечество.»</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/2.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Анатолий Петров</p>
					<span>Преподаватель математики, столяр.</span>
					<span>«Говорят что наличие неизменного Я это иллюзия, поэтому самому о себе сказать нечего.»</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/12.jpg?cache=3" alt="">
				</div>
				<div>
					<p class="guest-title">Мария Павлович</p>
					<span>Автор 4х книг. Две экранизации. Пишу. Снимаю. Говорю в микрофон.</span>
					<span>Девиз: от Темноты к Свету.</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/7.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Митя Якушкин aka Буба</p>
					<span>Специалист по хип-хопу и НБА, синефил.</span>
					<span>Канал <a href="https://www.youtube.com/channel/UC96FRlOu8B0lt8FmPj-P3qw" target="_blank">Bubba на youtube.com</a></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/8.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Матвей "Мотя" Хапаев aka 41 Savage</p>
					<span>Штраус, NBA, Метцингер.</span>
					<span>«Родился 1 сентября 1976 года и с тех пор держу глаза и уши открытыми.»</span>
					<span>Я на <a href="http://www.facebook.com/samtakoy" target="_blank">Facebook</a></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12">
			<div class="guest-item with-title-info">
				<div class="guest-item-img">
					<img src="../img/10.jpg" alt="">
				</div>
				<div>
					<p class="guest-title">Александр Снегирев</p>
					<span>Родился в Москве в клинике имени доктора Снегирёва. С тех пор пребывает либо в маниакальном, либо в депрессивном состоянии. Возбуждается от литературы и живописи. Женат.</span>
					<span><a href="https://www.facebook.com/alexander.snegirev.1" target="_blank">Facebook</a></span>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>