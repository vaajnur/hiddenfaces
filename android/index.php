<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Android");
?>

<div class="container inner__container">
	<div class="row guests">
		<h1 class="col-12 no-bot-wrap">Android</h1>
		<div class="bread col-12">
			<a href="/">Главная</a>
			<span> > </span>
			<a>Android</a>
		</div>
		<div class="col-12">
			<p class="iphone-p">Все выпуски подкастов студии «Скрытые лица» можно слушать через приложение «<a href="https://play.google.com/store/apps/details?id=ru.yandex.music&hl=ru" class="iphone-a" target="_blank">Яндекс.Музыка</a>»</p>
			<br />
			<ul class="iphone-ul">
				<li>1. Откройте приложение</li>
				<li>2. Откройте раздел поиска</li>
				<li>3. Введите название передачи, например: «Скрытые лица - Беседы»</li>
				<li>4. Откройте передачу «Скрытые лица - Беседы» и нажмите «подписаться»</li>
				<li>5. Выберите интересующий ваш подкаст и нажмите «play»</li>
			</ul>
		</div>
	</div>
</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>