<?php
function imagefon(&$image) {
	$width = imagesx($image);
	$height = imagesy($image);
	$gray = imagecolorat($image, 0, 0);
	for($x = 0; $x < $width; $x++) {
		for($y = 0; $y < $height; $y++) {
			$rgb = imagecolorat($image, $x, $y);
			if ( $gray == $rgb ) {
				imagesetpixel($image, $x, $y, imagecolortransparent($image, imagecolorallocate($image, 0, 0, 0)));
			} else {
				imagesetpixel($image, $x, $y, imagecolorallocate($image, 4, 153, 208));
			}
		}
	}
}

$im = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'].'/test/testtesttest.png');
imagefon($im);
imagepng($im, $_SERVER['DOCUMENT_ROOT'].'/test/testtesttest-new-color.png');
imagedestroy($im);
?>