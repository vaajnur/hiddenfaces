<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Слушать подкасты бесплатно онлайн - студия Скрытые лица");
?>
        <div class="header">
          <div class="container inner__container">
            <div class="row header__row">
              <div class="col-lg-6 header__column header__column-left">
                <h1 class="header__title">Скрытые лица<span class="obj-2"></span></h1>
								<p class="header__description d-sm-block">Слушайте аудиопередачи на разные темы в любое удобное для вас время. Бесплатно.</p>
								<p class="header__description d-block d-sm-none">Слушайте нас на любых телефонах в этих приложениях в любое удобное для вас время. Бесплатно.</p>
								<div class="itunes-mobile-container">
									<a target="_blank" href="https://itunes.apple.com/ru/artist/%D1%81%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D0%B5-%D0%BB%D0%B8%D1%86%D0%B0/1329972836?mt=2&app=podcast">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/itunes.svg" alt="">
									</a>
									<a target="_blank" href="https://www.google.com/podcasts?feed=aHR0cHM6Ly9oaWRkZW5mYWNlcy5ydS9yc3MvaGlkZGVuZmFjZXMtc2tyeXR5ZS1saXRzYS1vLWtpbm8ucnNz">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/google.svg" alt="">
									</a>
									<a target="_blank" href="https://soundstream.media/channel/skrytyye-litsa">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/ss.svg" alt="">
									</a>
									<a target="_blank" href="https://music.yandex.ru/album/7045724">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/ym.svg" alt="">
									</a>
									<a target="_blank" href="https://open.spotify.com/search/%D0%A1%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D0%B5%20%D0%BB%D0%B8%D1%86%D0%B0">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/listen-on-spotify.png" alt="">
									</a>
									<a target="_blank" href="http://101.ru/podcasts/skrytye-litsa-besedy">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/101.svg" alt="">
									</a>
									<a target="_blank" href="https://www.litres.ru/skrytye-lica/">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/litres.png" alt="">
									</a>
									<a target="_blank" href="https://kiozk.ru/lp2">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/kiozk.png" alt="">
									</a>
								</div>
								<h2 class="popular-categories-title title-unic"><a class="unic-cards-format-link" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Чем уникален формат подкастов?</a></h2>
			  </div>
              <div class="col-lg-1"></div>
              <div class="col-lg-5 header__column header__column-right">
              	<?
				$arSelect = Array("ID", "NAME",'IBLOCK_SECTION_ID');
				$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'PROPERTY_MAIN_PODCAST_VALUE' => "Да");
				$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC","SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
				if($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					$name_today=$arFields['NAME'];
					if($arFields['IBLOCK_SECTION_ID']==29){
						$name_today="Детектив - ".$arFields['NAME'];
					}
					$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'PROPERTY_MAIN_PODCAST_VALUE'  => "Да");
				}
				else{
					$name_today="Сегодня в эфире";
					$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'<DATE_ACTIVE_FROM'  => date('d.m.Y H:i:s'),"!ID"=>19);  
				}
				$arSelect = Array();
				$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC","SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
				if($ob = $res->GetNextElement()){ 
					$arFields = $ob->GetFields();  
					$arProps = $ob->GetProperties();
					$arSelect2 = Array("ID","NAME");
					$arFilter2 = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$arFields["IBLOCK_SECTION_ID"], "ACTIVE"=>"Y"); 
					$res2 = CIBlockElement::GetList(Array("ACTIVE_FROM" => "ASC"), $arFilter2, false, Array("nPageSize"=>1000), $arSelect2);
					$index=0;
					$section_index=0;
					while($ob2 = $res2->GetNextElement()){ 
						$index++;
						$arFields2 = $ob2->GetFields();
						if($arFields2['ID']==$arFields["ID"])
							$section_index=$index;
					}
					?>
					
					<h4 class="header__subtitle"><?=$name_today;?></h4>
					<div class="header__podcast"><a href="<?=$arFields['DETAIL_PAGE_URL'];?>">
						<? if(count($arProps['COVER_VARIANTS']['VALUE'])>1){?>
						<? $file = CFile::ResizeImageGet($arProps['COVER_VARIANTS']['VALUE'][rand(0,count($arProps['COVER_VARIANTS']['VALUE'])-1)], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
						<div class="cover-disc" style="background-image:url(<?=$file['src'];?>);"></div>
						<?}
						else{?>
						<? $file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
						<div class="cover-disc" style="background-image:url(<?=$file['src'];?>);"></div>
						<?}?>
						
						
					  <button class="player__button link_button player__button-header" onclick="yaCounter46865727.reachGoal('click_play_peredacha_<?=$arFields['IBLOCK_SECTION_ID'];?>');" id="button1"></button>
					  <audio class="html5-player" controls="controls" preload="none">
					  	<source src="<?=CFile::GetPath($arProps['PODCAST']['VALUE']);?>" type="audio/mpeg" /> Your browser does not support the audio element.
					  </audio>
					  <div class="player__audio" id="player1" style="display: none"></div>
					</a></div>
					<p class="name-under">#<?=$section_index;?>  <?=$arFields['NAME'];?></p>
                <?
				}
				?>
              </div>
			</div>
			
								<div class="collapse" id="collapseExample">
									<div class="card card-body uniq-card_color">
										<!-- <div class="card-body h-100"> -->
											<div class="row justify-content-between ">
												<div class="col-4">
													<div class="pr-5">
														<h4 class="advantages-title">Всегда вовремя</h4>
														<p class="advantages-description">Подкасты выходят в записи. Слушайте интересные программы в удобное время. Стоите в пробке, готовите ужин, вышли на пробежку? Включите подкаст!</p>
													</div>
												</div>
												<div class="col-4">
													<div class="px-5">
														<h4 class="advantages-title">Без цензуры</h4>
														<p class="advantages-description">Делиться идеями и мыслями без цензцуры и редактуры - бесценно. Искренность и прямолинейность это то, что отличает подкасты от радио. Некоторые передачи в которых говорят, что думают, помечены знаком 18+.</p>
													</div>
												</div>
												<div class="col-4">
													<div class="pl-5">
														<h4 class="advantages-title">Люди беседуют с людьми</h4>
														<p class="advantages-description">Только слушая подкаст, вы сможете почувствовать особую атмосферу присутствия, погружения и доверия с ведущими. Попробуйте, вам обязательно понравится!</p>
													</div>
												</div>
											</div>
											<!-- <div class="uniq-toggle"></div> -->
										<!-- </div> -->
									</div>
								</div>
							
            <hr class="separator d-sm-block">
          </div>
        </div>
        <div class="section mb-5">
        	<div class="container">
        		<div class="uniq-wrapper">
        			<div class="row justify-content-between">
        				<div class="col-sm-6 d-none d-lg-block">
        					<h2 class="popular-categories-title">Слушать в приложении для смартфонов:</h2>
        					<div class="card uniq-card m-0">
        						<div class="card-body card-body-click">
									<div class="itunes-sub itunes-sub_flex">
									<a target="_blank" href="https://itunes.apple.com/ru/artist/%D1%81%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D0%B5-%D0%BB%D0%B8%D1%86%D0%B0/1329972836?mt=2&app=podcast">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/itunes.svg" alt="">
									</a>
									<a target="_blank" href="https://www.google.com/podcasts?feed=aHR0cHM6Ly9oaWRkZW5mYWNlcy5ydS9yc3MvaGlkZGVuZmFjZXMtc2tyeXR5ZS1saXRzYS1vLWtpbm8ucnNz">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/google.svg" alt="">
									</a>
									<a target="_blank" href="https://soundstream.media/channel/skrytyye-litsa">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/ss.svg" alt="">
									</a>
									<a target="_blank" href="https://music.yandex.ru/album/7045724">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/ym.svg" alt="">
									</a>
									<a target="_blank" href="https://open.spotify.com/search/%D0%A1%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D0%B5%20%D0%BB%D0%B8%D1%86%D0%B0">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/listen-on-spotify.png" alt="">
									</a>
									<a target="_blank" href="http://101.ru/podcasts/skrytye-litsa-besedy">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/101.svg" alt="">
									</a>
									<a target="_blank" href="https://www.litres.ru/skrytye-lica/">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/litres.png" alt="">
									</a>
									<a target="_blank" href="https://kiozk.ru/lp2">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/kiozk.png" alt="">
									</a>
								</div>
        						</div>
        					</div>
        				</div>
        				<div class="col-auto">
        					<h2 class="popular-categories-title">Популярные категории этой недели</h2>
							<?
							$populars = array();										
			                  $arSelect = Array("ID", "PROPERTY_POPS_SECT");
			                  $arFilter = Array("ID"=>1753, "ACTIVE"=>"Y");
			                  $popres = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>2), $arSelect);
									while($popar_res = $popres->GetNext()){
			                    $populars[] = $popar_res['PROPERTY_POPS_SECT_VALUE'];
			                  }
							?>
        					<div class="row popular-categories-row">

								<? foreach($populars as $popular){
                  					$section_name = '';
									$res = CIBlockSection::GetByID($popular);
									if($ar_res = $res->GetNext()){
										$section_name= $ar_res['NAME'];
										$section_id = $ar_res['ID'];
										$section_url = $ar_res['SECTION_PAGE_URL'];
										$section_pic = CFile::GetPath($ar_res['PICTURE']);
									}
			                  if (empty($section_name)) {
			                    $arSelect = Array("ID", "NAME", "PROPERTY_ICON","DETAIL_PAGE_URL");
			                    $arFilter = Array("ID"=>$popular, "ACTIVE"=>"Y");
			                    $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
			                    if($ar_res = $res->GetNext()){
			                      $section_name= $ar_res['NAME'];
			                      $section_id = $ar_res['ID'];
			                      $section_url = $ar_res['DETAIL_PAGE_URL'];
			                      $section_pic = CFile::GetPath($ar_res['PROPERTY_ICON_VALUE']);
			                    }
									}
									?>
        						<div class="col-6 popular-categories-col">
        							<div class="card category-popular-card popular-bg-1 m-0">
        								<div class="card-body popular-categories-card-body">
											<div class="icon" style="background-image: url(<?=$section_pic;?>);"></div>
        									<h3 class="title"><a href="<?=$section_url;?>" class="stretched-link"><?=$section_name;?></a></h3>
        								</div>
        							</div>
        						</div>
									<?}?>
        						<!-- <div class="col-6 popular-categories-col">
        							<div class="card category-popular-card fullsize-icon-card popular-bg-1 m-0">
        								<div class="card-body popular-categories-card-body">
											<div class="icon" style="background-image: url(https://hiddenfaces.ru/upload/resize_cache/iblock/ec3/400_400_1/ec327d8a5c8d8048f0a79245760dde77.jpg);"></div>
        									<h3 class="title"><a href="https://hiddenfaces.ru/peredachi/stikhi-na-sluchai-zhizni/sbornik-vi/" class="stretched-link">СБОРНИК VI</a></h3>
        								</div>
        							</div>
        						</div>
        						 -->

        					</div>
        				</div>
        			</div>
					<div class="card uniq-card uniq-card-full uniq-card_color">
						<div class="card-body h-100">
							<div class="row justify-content-between ">
								<div class="col-4">
									<div class="pr-5">
										<h4 class="advantages-title">Всегда вовремя</h4>
										<p class="advantages-description">Подкасты выходят в записи. Слушайте интересные программы в удобное время. Стоите в пробке, готовите ужин, вышли на пробежку? Включите подкаст!</p>
									</div>
								</div>
								<div class="col-4">
									<div class="px-5">
										<h4 class="advantages-title">Без цензуры</h4>
										<p class="advantages-description">Делиться идеями и мыслями без цензцуры и редактуры - бесценно. Искренность и прямолинейность это то, что отличает подкасты от радио. Некоторые передачи в которых говорят, что думают, помечены знаком 18+.</p>
									</div>
								</div>
								<div class="col-4">
									<div class="pl-5">
										<h4 class="advantages-title">Люди беседуют с людьми</h4>
										<p class="advantages-description">Только слушая подкаст, вы сможете почувствовать особую атмосферу присутствия, погружения и доверия с ведущими. Попробуйте, вам обязательно понравится!</p>
									</div>
								</div>
							</div>
							<div class="uniq-toggle"></div>
						</div>
					</div>
        		</div>
        	</div>
        </div>
        <div class="section mb-5 categories-content">
			<?
			include $_SERVER['DOCUMENT_ROOT'].'/ajax/categories.php';
			?>
        </div>
		<?
			$GLOBALS['FILTER_MAIN_CATALOG']=array();
			//$GLOBALS['FILTER_PROGRAM']["PROPERTY_MAIN_PODCAST"]=false;
			$APPLICATION->IncludeComponent(
	"hiddenfaces:news.list", 
	"podcasts_main_2", 
	array(
		"CATEGORIES" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "hidden_faces",
		"IBLOCK_ID" => "3", 
		"NEWS_COUNT" => "9",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "FILTER_PROGRAM",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "ACTIVE_FROM",
			2 => "DATE_CREATE",
			3 => "CREATED_BY",
			4 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "EIGHTEEN",
			1 => "TIME_LENGTH",
			2 => "SEASON",
			3 => "DESCRIPTION",
			4 => "PODCAST",
			5 => "WAVEFORM_1",
			6 => "WAVEFORM_2",
			7 => "CATEGORIES",
			8 => "CATS",
			9 => "GUESTS",
			10 => "AUTHORS",
			11 => "SOAUTHORS",
			12 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "podcasts_main_2",
		"STRICT_SECTION_CHECK" => "N",
		"FILE_404" => ""
	),
	false
);?>
</div>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>