<?
    include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404","Y");
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("404 Not Found");
?>
<!-- <script>
    $('.page__content').addClass('missed');
</script> -->
<div class="missed-text">
    <p class="animated">Упс!</p>
    <span>Кажется такой страницы не существует!<br>Попробуйте воспользоваться поиском.</span>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>