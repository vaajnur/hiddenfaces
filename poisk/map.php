<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карта сайта");
?>

<div class="container inner__container">
	<div class="row guests">
	<?
	$APPLICATION->AddChainItem("Карта сайта", "");
	?>
	<?
	$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"WITHOUT_FOOTER"=>"Y",
			 "WITHOUT_ROW"=>"Y",
			"section__margin"=>"section__margin",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);
	?>
	<?$APPLICATION->IncludeComponent("hiddenfaces:main.map", ".default", Array(
		"LEVEL"	=>	"3",
		"COL_NUM"	=>	"2",
		"SHOW_DESCRIPTION"	=>	"Y",
		"SET_TITLE"	=>	"Y",
		"CACHE_TIME"	=>	"36000000"
		)
	);?>
	</div>
</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>