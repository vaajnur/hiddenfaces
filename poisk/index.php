<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?>
<?
//собираем айдишники гостей
$guests_ids=array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>6,"%NAME"=>$_REQUEST['q'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
while($ob = $res->GetNextElement()){
	$fields=$ob->GetFields();
	$guests_ids[]=$fields["ID"];
}

//собираем айдишники авторов
$authors_ids=array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>7,"%NAME"=>$_REQUEST['q'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
while($ob = $res->GetNextElement()){
	$fields=$ob->GetFields();
	$authors_ids[]=$fields["ID"];
}
//собираем юзеров
$users_ids=array();
if(count($authors_ids)>0){
	$filter = Array("UF_AUTHOR_INFO" => $authors_ids);
	$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
	while ($arUser = $rsUsers->Fetch()) {
	  $users_ids[] = $arUser["ID"];
	}
}
//собираем айдишники категорий
$categories_ids=array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>5,"%NAME"=>$_REQUEST['q'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
while($ob = $res->GetNextElement()){
	$fields=$ob->GetFields();
	$categories_ids[]=$fields["ID"];
}
//собираем айдишники подкастов
$podcasts_ids=array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>3,"%NAME"=>$_REQUEST['q'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
while($ob = $res->GetNextElement()){
	$fields=$ob->GetFields();
	$podcasts_ids[]=$fields["ID"];
}
//собираем айдишники передач

$section_ids=array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>3,"%NAME"=>$_REQUEST['q'], "ACTIVE"=>"Y");
$rsSections = CIBlockSection::GetList(array(), $arFilter,array(),array(),array("nPageSize"=>100)); //Получили список разделов из инфоблока
while($arSection = $rsSections->GetNext())
{
    $section_ids[] = $arSection['ID'];//Сохранили выборку в $arResult для передачи в шаблон
}
//собираем айдишники подкастов по дате
$arSelect = Array("ID", "NAME","DATE_ACTIVE_FROM");
$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement()){
	$fields=$ob->GetFields();
	if(strpos($fields["DATE_ACTIVE_FROM"],$_REQUEST['q'])!==false)
		$podcasts_ids[]=$fields["ID"];
}
//собираем айдишники подкастов по тексту
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>3,"%DETAIL_TEXT"=>$_REQUEST['q'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
while($ob = $res->GetNextElement()){
	$fields=$ob->GetFields();
	$podcasts_ids[]=$fields["ID"];
}
//собираем айдишники подкастов по передаче
if(count($section_ids)>0){
	$arSelect = Array("ID", "NAME");
	$arFilter = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$section_ids, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
	while($ob = $res->GetNextElement()){
		$fields=$ob->GetFields();
		$podcasts_ids[]=$fields["ID"];
	}
}
//собираем айдишники подкастов по категории
$arSelect = Array("ID", "NAME");
if(count($categories_ids)>0){
	$arFilter = Array("IBLOCK_ID"=>3,"PROPERTY_CATEGORIES"=>$categories_ids, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
	while($ob = $res->GetNextElement()){
		$fields=$ob->GetFields();
		$podcasts_ids[]=$fields["ID"];
	}
}
//собираем айдишники подкастов по гостям
$arSelect = Array("ID", "NAME");
if(count($guests_ids)>0){
	$arFilter = Array("IBLOCK_ID"=>3,"PROPERTY_GUESTS"=>$guests_ids, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
	while($ob = $res->GetNextElement()){
		$fields=$ob->GetFields();
		$podcasts_ids[]=$fields["ID"];
	}
}
//собираем айдишники подкастов по авторам
$arSelect = Array("ID", "NAME");
if(count($authors_ids)>0){
	$arFilter = Array("IBLOCK_ID"=>3,"PROPERTY_AUTHORS"=>$authors_ids, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
	while($ob = $res->GetNextElement()){
		$fields=$ob->GetFields();
		$podcasts_ids[]=$fields["ID"];
	}
}
//собираем айдишники подкастов по авторам
$arSelect = Array("ID", "NAME");
if(count($users_ids)>0){
	$arFilter = Array("IBLOCK_ID"=>3,"CREATED_BY"=>$users_ids, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
	while($ob = $res->GetNextElement()){
		$fields=$ob->GetFields();
		$podcasts_ids[]=$fields["ID"];
	}
}
?>

<div class="container inner__container">
	<div class="row guests">
		<h1 class="col-12 no-bot-wrap">Поиск</h1>
		<div class="bread col-12">
			<a href="/">Главная</a>
			<span> > </span>
			<a>Поиск</a>
		</div>
		<div class="navigation__item navigation__item-search full-width-search">
			<i class="fa fa-search navigation__icon navigation__icon-search" aria-hidden="true"></i>
			<form action="/poisk/">
				<input type="text" name="q" placeholder="Введите название подкаста/имя гостя/автора/дату" value="<?=htmlspecialcharsbx($_REQUEST['q']);?>" class="navigation__search">
			</form>
		</div>
		<div class="space"></div>
		<div class="col-12">
		<?
		$GLOBALS['FILTER_PROGRAM']=array();
		$GLOBALS['FILTER_PROGRAM']["ID"]=$podcasts_ids;
		$GLOBALS['FILTER_PROGRAM']["PROPERTY_MAIN_PODCAST"]=false;
		if(count($podcasts_ids)>0){
			$APPLICATION->IncludeComponent("hiddenfaces:news.list","podcasts_recommended",
				Array(
					"PODCASTS_TITLE" => "Вот, что мы нашли:",
					"SHOW_BANNERS" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"FILTER_NAME" => "FILTER_PROGRAM",
					"IBLOCK_TYPE" => "hidden_faces",
					"IBLOCK_ID" => "3",
					"NEWS_COUNT" => "20",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FIELD_CODE" => Array("ID","DATE_CREATE","CREATED_BY"),
					"PROPERTY_CODE" => Array("DESCRIPTION","PODCAST","WAVEFORM_1","WAVEFORM_2",'TIME_LENGTH',"CATEGORIES","COVER_VARIANTS","GUESTS","EIGHTEEN","AUTHORS","SOAUTHORS", "SEASON"), 
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_LAST_MODIFIED" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"PAGER_BASE_LINK_ENABLE" => "Y",
					"SET_STATUS_404" => "Y",
					"SHOW_404" => "Y",
					"MESSAGE_404" => "",
					"PAGER_BASE_LINK" =>"/poisk/",
					"PAGER_PARAMS_NAME" => "arrPager",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
			);
		}
		?>
	</div>
	</div>
</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>