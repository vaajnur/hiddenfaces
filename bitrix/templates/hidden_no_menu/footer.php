
                                    <table class="row footer" style="background-color:#000;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                        <tbody>
                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                <th class="column social-column small-12 large-4 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:50px;padding-left:16px;padding-right:8px;padding-top:50px;text-align:left;width:177.33px">
                                                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                <p style="Margin:0;Margin-bottom:10px;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left">Подписывайтесь на нас:</p>
                                                                <a href="https://www.facebook.com/hiddenfaces.ru/" style="Margin:0;color:#fff;display:inline-block;font-family:Helvetica,Arial,sans-serif;font-size:32px;font-weight:700;line-height:1.3;margin:10px 0;padding:0;text-align:left;text-decoration:none">fb</a>
                                                                <a href="https://twitter.com/hiddenfacesru" style="Margin:0;color:#fff;display:inline-block;font-family:Helvetica,Arial,sans-serif;font-size:32px;font-weight:700;line-height:1.3;margin:10px 0;padding:0;text-align:left;text-decoration:none">tw</a>
                                                                <a href="https://vk.com/hiddenfacesru" style="Margin:0;color:#fff;display:inline-block;font-family:Helvetica,Arial,sans-serif;font-size:32px;font-weight:700;line-height:1.3;margin:10px 0;padding:0;text-align:left;text-decoration:none">vk</a>
<a href="https://www.instagram.com/podcaststudio/" style="Margin:0;color:#fff;display:inline-block;font-family:Helvetica,Arial,sans-serif;font-size:32px;font-weight:700;line-height:1.3;margin:10px 0;padding:0;text-align:left;text-decoration:none">in</a>
                                                                <p class="privacy" style="Margin:0;Margin-bottom:10px;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left">Ⓒ 2017 Скрытые лица Все права защищены</p>
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </th>
                                                <th class="column links-columns small-12 large-4 columns" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:50px;padding-left:8px;padding-right:8px;padding-top:50px;text-align:left;width:177.33px">
                                                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                <a href="http://hiddenfaces.ru/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:none">Главная</a>
                                                                <a href="http://hiddenfaces.ru/peredachi/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:none">Передачи</a>
                                                                <a href="http://hiddenfaces.ru/gosti/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:none">Гости</a>
                                                                <a href="http://hiddenfaces.ru/o-proekte/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:none">О проекте</a>
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </th>
                                                <th class="column links-columns small-12 large-4 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:50px;padding-left:8px;padding-right:16px;padding-top:50px;text-align:left;width:177.33px">
                                                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                <a href="http://hiddenfaces.ru/stat-avtorom/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:none">Стать автором</a>
                                                                <a href="http://hiddenfaces.ru/iphone/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:none">iPhone</a>
                                                                <a href="http://hiddenfaces.ru/android/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:none">Android</a>
                                                                <a href="http://hiddenfaces.ru/kontakty/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;text-decoration:none">Контакты</a>
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </center>
            </td>
        </tr>
    </table>
    <!-- prevent Gmail on iOS font size manipulation -->
    <div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>

</html>