<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отписаться от рассылки");
?>

<div class="container inner__container">
	<div class="row guests">
		<?
		$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				 "WITHOUT_ROW"=>"Y",
				"section__margin"=>"section__margin",
				"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
			  )
		);
		?>
		<div class="col-12">
			<?
				CModule::IncludeModule("subscribe");
				$subscription = CSubscription::GetByEmail($_REQUEST['email']); 
				$subscr=new CSubscription;
				if($arSub = $subscription->Fetch()){
					$subscr->Delete($arSub['ID']);
				}
			?>
			<p>Вы отписались от рассылки</p>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>