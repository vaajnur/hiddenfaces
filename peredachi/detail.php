<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Hidden Faces - Передача");
?>
<?
$arFilter = array('IBLOCK_ID' => 3, 'CODE' => $_REQUEST['SECTION_CODE']);
$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter, array(), array("ID", "NAME", "PICTURE", "DESCRIPTION", "UF_*", 'SECTION_PAGE_URL'));
if ($arSection = $rsSections->GetNext()) {
	$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues(
		$arSection["IBLOCK_ID"],
		$arSection["ID"]
	);

	$arSection["IPROPERTY_VALUES"] = $ipropValues->getValues();
	if (strlen($arSection["IPROPERTY_VALUES"]['SECTION_META_TITLE']) > 0)
		$APPLICATION->SetPageProperty("title", $arSection["IPROPERTY_VALUES"]['SECTION_META_TITLE']);
	if (strlen($arSection["IPROPERTY_VALUES"]['SECTION_META_KEYWORDS']) > 0)
		$APPLICATION->SetPageProperty("keywords", $arSection["IPROPERTY_VALUES"]['SECTION_META_KEYWORDS']);
	if (strlen($arSection["IPROPERTY_VALUES"]['SECTION_META_DESCRIPTION']) > 0)
		$APPLICATION->SetPageProperty("description", $arSection["IPROPERTY_VALUES"]['SECTION_META_DESCRIPTION']);
	$APPLICATION->AddChainItem($arSection['NAME'], "");
	$APPLICATION->SetTitle($arSection["NAME"]);
} else {
	define(ERROR_404, "Y");
}
?>
<? $file = CFile::ResizeImageGet($arSection['PICTURE'], array('width' => 500, 'height' => 500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
<? $APPLICATION->AddHeadString('<meta property="og:title" content="' . $arSection["NAME"] . '" />', true) ?>
<? $APPLICATION->AddHeadString('<meta property="og:description" content="' . htmlspecialchars(strip_tags($arSection['DESCRIPTION'])) . '" />', true) ?>
<? $APPLICATION->AddHeadString('<link type="application/rss+xml" rel="alternate" title="' . $arSection["NAME"] . '" href="https://hiddenfaces.ru/rss/hiddenfaces-' . $arSection['CODE'] . '.rss"/>', true) ?>
<? $APPLICATION->AddHeadString('<meta property="og:type" content="website" />', true) ?>
<? $APPLICATION->AddHeadString('<meta property="og:url" content="' . SITE_URL_WITH_HTTP . $_SERVER['REQUEST_URI'] . '" />', true) ?>
<? $APPLICATION->AddHeadString('<meta property="og:image" content="' . SITE_URL_WITH_HTTP . $file['src'] . '" />', true) ?>
	<div class="container">
		<div class="row">
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
					"AREA_FILE_SUFFIX" => "",
					"EDIT_TEMPLATE" => "",
					"WITHOUT_FOOTER" => "Y",
					"WITHOUT_ROW" => "Y",
					"section__margin" => "section__margin",
					"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
				)
			);

			?>
			<div class="col-12 col-lg-6">

				<img src="<?= $file['src']; ?>" class="img-responsive" alt="">
				<span class="img-author"><? if (strlen($arSection['UF_COVER_AUTHOR']) > 0) { ?>Автор обложки: <?= $arSection['UF_COVER_AUTHOR']; ?><? } ?></span>
			</div>
			<div class="col-12 col-lg-6 pod-page-player column-space-between">
				<div class="pod-info"><?= html_entity_decode($arSection['DESCRIPTION']); ?> <br> <br> <span class="pod-date"><? if (strlen($arSection['UF_SCHEDULE']) > 0) { ?>Передача <?= strtolower($arSection['UF_SCHEDULE']); ?><? } ?></span></div>
				<div class="panel__footer">
					<? if (strlen($arSection['UF_ITUNES_LINK']) > 0) { ?>
						<a data-title="Слушайте в iTunes" target="_blank" href="<?= $arSection['UF_ITUNES_LINK'] ?>" class="d-flex-link visible-mac">
							<svg width="38" height="38" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd"
									  d="M21.95 19.409C21.732 19.962 21.23 20.74 20.518 21.289C20.107 21.606 19.616 21.908 18.941 22.108C18.222 22.321 17.337 22.393 16.235 22.393H6.158C5.056 22.393 4.171 22.321 3.452 22.108C2.778 21.908 2.287 21.605 1.875 21.289C1.163 20.741 0.66 19.962 0.443 19.409C0.00499985 18.295 0 17.031 0 16.235V6.158C0 5.362 0.00399983 4.098 0.443 2.984C0.661 2.431 1.163 1.653 1.875 1.104C2.286 0.788 2.777 0.485 3.452 0.285C4.171 0.0720003 5.056 0 6.158 0H16.235C17.337 0 18.222 0.0720003 18.941 0.285C19.615 0.485 20.106 0.788 20.518 1.104C21.23 1.652 21.733 2.431 21.95 2.984C22.388 4.098 22.393 5.362 22.393 6.158V16.235C22.393 17.031 22.389 18.295 21.95 19.409Z"
									  fill="url(#paint0_linear)"/>
								<path d="M12.8201 12.871C12.4841 12.517 11.8941 12.289 11.1971 12.289C10.5001 12.289 9.91005 12.516 9.57405 12.871C9.39905 13.056 9.30605 13.246 9.27705 13.516C9.22005 14.039 9.25206 14.49 9.31306 15.21C9.37106 15.896 9.48205 16.811 9.62705 17.744C9.73005 18.407 9.81405 18.765 9.89005 19.022C10.0131 19.437 10.4751 19.801 11.1971 19.801C11.9191 19.801 12.3801 19.438 12.5041 19.022C12.5801 18.766 12.6641 18.408 12.7671 17.744C12.9121 16.812 13.0231 15.897 13.0811 15.21C13.1421 14.49 13.1741 14.039 13.1181 13.516C13.0891 13.246 12.9961 13.056 12.8201 12.871ZM9.36705 9.71397C9.36705 10.726 10.1871 11.547 11.2001 11.547C12.2131 11.547 13.0331 10.727 13.0331 9.71397C13.0331 8.70197 12.2121 7.88097 11.2001 7.88097C10.1881 7.88097 9.36705 8.70197 9.36705 9.71397ZM11.1771 2.48197C6.89106 2.49397 3.37905 5.97097 3.32605 10.257C3.28305 13.729 5.49905 16.699 8.59405 17.787C8.66905 17.813 8.74505 17.751 8.73405 17.672C8.69305 17.403 8.65505 17.132 8.62005 16.865C8.60805 16.771 8.54805 16.69 8.46105 16.652C6.01505 15.583 4.30705 13.13 4.33405 10.289C4.37005 6.55997 7.41505 3.51997 11.1441 3.48897C14.9561 3.45797 18.0681 6.55097 18.0681 10.356C18.0681 13.169 16.3671 15.592 13.9401 16.652C13.8531 16.69 13.7931 16.771 13.7811 16.865C13.7461 17.131 13.7081 17.402 13.6671 17.671C13.6551 17.75 13.7311 17.812 13.8071 17.786C16.8721 16.708 19.0751 13.784 19.0751 10.355C19.0741 6.00697 15.5291 2.46897 11.1771 2.48197ZM11.0341 6.09497C13.4641 6.00097 15.4701 7.94997 15.4701 10.359C15.4701 11.584 14.9511 12.69 14.1211 13.47C14.0511 13.536 14.0131 13.63 14.0181 13.726C14.0341 14.019 14.0281 14.304 14.0091 14.625C14.0041 14.711 14.1001 14.765 14.1711 14.717C15.5621 13.766 16.4771 12.168 16.4771 10.359C16.4771 7.38097 13.9961 4.97097 10.9931 5.08797C8.20206 5.19597 5.96205 7.49697 5.92705 10.291C5.90405 12.128 6.82406 13.755 8.23306 14.718C8.30406 14.766 8.40005 14.711 8.39405 14.626C8.37405 14.304 8.36805 14.019 8.38505 13.726C8.39005 13.63 8.35305 13.536 8.28205 13.47C7.42605 12.667 6.90105 11.516 6.93505 10.245C6.99605 8.00797 8.79905 6.18097 11.0341 6.09497Z"
									  fill="white"/>
								<defs>
									<linearGradient id="paint0_linear" x1="11.1965" y1="0" x2="11.1965" y2="22.3931" gradientUnits="userSpaceOnUse">
										<stop stop-color="#F452FF"/>
										<stop offset="1" stop-color="#832BC1"/>
									</linearGradient>
								</defs>
							</svg>
						</a>
					<? } ?>
					<? if (strlen($arSection['UF_SOUNDSTREAM_LINK']) > 0) { ?>
						<a data-title="Слушайте в SoundStream" href="<?= $arSection['UF_SOUNDSTREAM_LINK'] ?>" class="d-flex-link visible-mac" target="_blank">
							<svg width="38" height="38" viewBox="0 0 120 120" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd" d="M25.5 119.9H94.4C108.4 119.9 119.9 108.4 119.9 94.4V25.5C119.9 11.5 108.4 0 94.4 0H25.5C11.5 0 0 11.5 0 25.5V94.4C0 108.4 11.5 119.9 25.5 119.9Z" fill="#EE4E5D"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M42.2001 21.4C43.2001 20.4 44.3001 20.2 45.6001 20.4C46.4001 20.6 48.7001 23.2 49.5001 23.9C50.4001 24.7 50.4001 25.9 49.5001 26.7C38.5001 36.8 38.5001 54.4 49.4001 64.5C50.4001 65.4 50.4001 66.7 49.3001 67.6C48.4001 68.4 47.5001 69.3 46.7001 70.2C45.8001 71.2 43.7001 71.4 42.7001 70.5C28.8001 57 28.5001 35.4 42.2001 21.4Z" fill="white"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M52.7001 32.0999C53.7001 31.0999 54.7001 30.8999 55.9001 30.9999C56.9001 31.0999 59.2001 33.7999 60.0001 34.4999C60.9001 35.3999 60.9001 36.4999 60.0001 37.3999C54.8001 42.1999 55.0001 49.2999 60.1001 54.0999C60.9001 54.8999 61.0001 56.0999 60.1001 56.8999C59.1001 57.8999 58.1001 58.8999 57.1001 59.8999C56.2001 60.7999 54.2001 60.7999 53.3001 59.9999C45.3001 52.0999 44.7001 40.2999 52.7001 32.0999Z" fill="white"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M77.6001 98.5C76.6001 99.5 75.5002 99.7 74.2002 99.5C73.4002 99.4 71.0002 96.7 70.3002 96C69.5002 95.2 69.4002 93.9999 70.3002 93.1999C81.3002 83.0999 81.3001 65.4999 70.4001 55.3999C69.4001 54.3999 69.4001 53.1999 70.5001 52.2999C71.4001 51.4999 72.3001 50.5999 73.1001 49.6999C74.0001 48.6999 76.0001 48.4999 77.0001 49.3999C91.1001 62.8999 91.4001 84.5 77.6001 98.5Z" fill="white"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M67.2002 87.8001C66.2002 88.8001 65.2002 89.1001 64.0002 88.9001C63.0002 88.8001 60.7002 86.1001 59.9002 85.4001C59.0002 84.5001 59.0002 83.4001 59.9002 82.5001C65.1002 77.7001 64.9002 70.7001 59.9002 65.8001C59.0002 65.0001 59.0002 63.8001 59.8002 63.0001C60.8002 62.0001 61.8002 61.0001 62.8002 60.0001C63.7002 59.1001 65.7002 59.1001 66.6002 59.9001C74.6002 67.8001 75.2002 79.6001 67.2002 87.8001Z" fill="white"/>
							</svg>
						</a>
					<? } ?>
					<? if (strlen($arSection['UF_GOOGLE_LINK']) > 0) { ?>
						<a data-title="Слушайте в Google Podcasts" href="<?= $arSection['UF_GOOGLE_LINK'] ?>" class="d-flex-link visible-mac yandex-music" target="_blank">
							<img src="/local/templates/hiddenfaces/img/google-podcasts.svg" alt="">
						</a>
					<? } ?>
					<? if (strlen($arSection['UF_YANDEX_LINK']) > 0) { ?>
						<a data-title="Слушайте на Яндекс.Музыка" class="d-flex-link visible-mac" target="_blank" href="<?= $arSection['UF_YANDEX_LINK'] ?>">
							<img src="/local/templates/hiddenfaces/img/icon_ym_header.svg" alt="">
						</a>
					<? } ?>
					<? if (strlen($arSection['UF_101_LINK']) > 0) { ?>
						<a data-title="Слушайте на радио 101.ru" class="d-flex-link visible-mac yandex-music" target="_blank" href="<?= $arSection['UF_101_LINK'] ?>">
							<img src="/local/templates/hiddenfaces/img/101.svg" alt="">
						</a>
					<? } ?>
					<?
					CModule::IncludeModule("subscribe");
					// Вывод рубрик можно производить таким способом
					$arOrder = Array("SORT" => "ASC", "NAME" => "ASC");
					$arFilter = Array("ACTIVE" => "Y");
					$rsRubric = CRubric::GetList($arOrder, $arFilter);
					$arRubrics = array();
					$rubric_id = 0;
					while ($arRubric = $rsRubric->GetNext()) {
						if ($arSection["ID"] > 0) {
							if ($arSection["ID"] == $arRubric['DESCRIPTION']) {
								$rubric_id = $arRubric['ID'];
							}
						}
					}
					if ($rubric_id == 0) {
						$rubric = new CRubric;
						if ($arSection["ID"] > 0) {
							$DESCRIPTION = $arSection["ID"];
						}
						$name = $arSection["NAME"];
						//echo $name;
						$arFields = Array(
							"ACTIVE" => "Y",
							"NAME" => $name,
							"DESCRIPTION" => $DESCRIPTION,
							"LID" => "s1"
						);
						$rubric_id = $rubric->Add($arFields);
					}

					?>
					<div id="modal-subscription" class="white-popup-block mfp-hide sub-modal">
						<h2 class="modal-title-sub">Подпишитесь<br> на новые подкасты:</h2>

						<? $APPLICATION->IncludeComponent(
							"hiddenfaces:subscribe.form",
							"modal_subscribe_form",
							array(
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"CODE_FORM" => $arSection["CODE"],
								"PAGE" => "/ajax/subscribe.php",
								"RUBRIC_ID" => $rubric_id,
								"SHOW_HIDDEN" => "Y",
								"USE_PERSONALIZATION" => "Y",
								"COMPONENT_TEMPLATE" => "modal_subscribe_form"
							),
							false
						); ?>
					</div>
					<a href="#modal-subscription" class="btn-detail popup-with-form">Подписаться</a>
					<div class="panel__social social">
						<span>Поделиться:</span>
						<ul class="social__list">
							<li class="social__item">
								<a onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?= SITE_URL_WITH_HTTP . $arSection['SECTION_PAGE_URL']; ?>','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>
							</li>
							<li class="social__item">
								<a onclick="window.open('http://vk.com/share.php?url=<?= SITE_URL_WITH_HTTP . $arSection['SECTION_PAGE_URL']; ?>&image=<? echo SITE_URL_WITH_HTTP . $arSection["PICTURE"]['SRC'] ?>&noparse=true','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
									<i class="fa fa-vk" aria-hidden="true"></i>
								</a>
							</li>
							<li class="social__item">
								<a onclick="window.open('http://twitter.com/share?url=<?= SITE_URL_WITH_HTTP . $arSection['SECTION_PAGE_URL']; ?>&image-src=<? echo SITE_URL_WITH_HTTP . $arSection["PICTURE"]['SRC'] ?>','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
									<i class="fa fa-twitter" aria-hidden="true"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<hr>
		</div>
		<?
		if (strlen($arSection["UF_PODCAST_ABOUT"]) > 0) {
			$GLOBALS['FILTER_PROGRAM'] = array();
			$GLOBALS['FILTER_PROGRAM']["ID"] = array($arSection["UF_PODCAST_ABOUT"]);
			$APPLICATION->IncludeComponent("hiddenfaces:news.list", "podcasts_recommended",
				Array(
					"SHOW_BANNERS" => "N",
					"PODCASTS_TITLE" => "О передаче",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"FILTER_NAME" => "FILTER_PROGRAM",
					"IBLOCK_TYPE" => "hidden_faces",
					"IBLOCK_ID" => "3",
					"NEWS_COUNT" => "9",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FIELD_CODE" => Array("ID", "DATE_CREATE", "CREATED_BY", "ACTIVE_FROM"),
					"PROPERTY_CODE" => Array("DESCRIPTION", "PODCAST", "WAVEFORM_1", "WAVEFORM_2", 'TIME_LENGTH', "CATEGORIES", "CATS", "GUESTS", "COVER_VARIANTS", "EIGHTEEN", "AUTHORS", "SOAUTHORS","SEASON"),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_LAST_MODIFIED" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"SET_STATUS_404" => "Y",
					"SHOW_404" => "Y",
					"MESSAGE_404" => "",
					"PAGER_BASE_LINK" => $arSection["SECTION_PAGE_URL"],
					"PAGER_PARAMS_NAME" => "arrPager",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
			);
		}
		?>
		<?
		$GLOBALS['FILTER_PROGRAM'] = array();
		//$GLOBALS['FILTER_PROGRAM']["PROPERTY_MAIN_PODCAST"]=false;
		$GLOBALS['FILTER_PROGRAM']["IBLOCK_SECTION_ID"] = $arSection['ID'];
		$APPLICATION->IncludeComponent("hiddenfaces:news.list", "podcasts_recommended",
			Array(
				"PODCASTS_TITLE" => "Выпуски:",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"FILTER_NAME" => "FILTER_PROGRAM",
				"IBLOCK_TYPE" => "hidden_faces",
				"IBLOCK_ID" => "3",
				"NEWS_COUNT" => "9",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FIELD_CODE" => Array("ID", "DATE_CREATE", "CREATED_BY", "ACTIVE_FROM"),
				"PROPERTY_CODE" => Array("DESCRIPTION", "PODCAST", "WAVEFORM_1", "WAVEFORM_2", 'TIME_LENGTH', "CATEGORIES", "CATS", "GUESTS", "COVER_VARIANTS", "EIGHTEEN", "AUTHORS", "SOAUTHORS", "SEASON"),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_LAST_MODIFIED" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"PAGER_BASE_LINK_ENABLE" => "Y",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
				"MESSAGE_404" => "",
				"PAGER_BASE_LINK" => $arSection["SECTION_PAGE_URL"],
				"PAGER_PARAMS_NAME" => "arrPager",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
			)
		);
		?>
	</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>