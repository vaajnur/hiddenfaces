<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Список передач и свежие аудио подкасты - студия Скрытые лица");
$APPLICATION->SetTitle("Передачи");
?>

<div class="container">
	<?
	$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"WITHOUT_FOOTER"=>"Y",
			"section__margin"=>"section__margin",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);
	?> 
	<? $APPLICATION->IncludeComponent(
	"hiddenfaces:podcasts.programs",
	".default",
	Array(
		"IBLOCK_ID"=>3,
		"ON_PAGE"=>12,
		// "SORT_FIELD"=>"element_cnt",
		"SORT_FIELD"=>"SORT",
		"SORT_ORDER"=>"ASC",
		"PAGE"=>strlen($_REQUEST["PAGEN_1"])>0?$_REQUEST["PAGEN_1"]:"1"
	),
	false
	);?>
	
</div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>