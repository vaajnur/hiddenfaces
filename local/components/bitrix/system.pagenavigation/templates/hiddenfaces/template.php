<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

/** @var PageNavigationComponent $component */
$component = $this->getComponent();

$this->setFrameMode(true);
if(strlen($arParams['CURRENT_PAGE'])>0)
	$arResult["CURRENT_PAGE"]=$arParams['CURRENT_PAGE'];
$url=$arResult["URL"];
$url=explode("?",$url);
$url=$url[0];
$url=preg_replace('/page+[0-9]{4}/i', '', $url, -1, $count);
$url=preg_replace('/page+[0-9]{3}/i', '', $url, -1, $count);
$url=preg_replace('/page+[0-9]{2}/i', '', $url, -1, $count);
$url=preg_replace('/page+[0-9]{1}/i', '', $url, -1, $count);
$url=str_replace("//","/",$url);
$arResult["URL"]=$url;
?>
<nav aria-label="Page navigation">
	<ul class="pagination">
	<?if ($arResult["CURRENT_PAGE"] > 1):?>
		<?if ($arResult["CURRENT_PAGE"] > 2):?>
			<li class="pagination__item pagination__prev">
				<a class="pagination__link"  href="<?=htmlspecialcharsbx($arResult["URL"])?>page<?=$arResult["CURRENT_PAGE"]-1;?>/">
					<i class="fa fa-chevron-left"></i>
				</a>
			</li><div class="pagination__numbers">
		<?else:?>
			<li class="pagination__item pagination__prev">
				<a class="pagination__link"  href="<?=htmlspecialcharsbx($arResult["URL"])?>">
					<i class="fa fa-chevron-left"></i>
				</a>
			</li><div class="pagination__numbers">
		<?endif?>
			<li class="pagination__item"><a class="pagination__link" href="<?=htmlspecialcharsbx($arResult["URL"])?>"><span>1</span></a></li>
	<?else:?>
			<li class="pagination__item pagination__prev">
				<a class="pagination__link"  href="#">
					<i class="fa fa-chevron-left"></i>
				</a>
			</li><div class="pagination__numbers">
			<li class="pagination__item is-active"><a class="pagination__link" href="#">1</a></li>
	<?endif?>

	<?
	$page = $arResult["START_PAGE"] + 1;
	while($page <= $arResult["END_PAGE"]-1):
	?>
		<?if ($page == $arResult["CURRENT_PAGE"]):?>
			<li class="pagination__item is-active"><a class="pagination__link" href="#"><?=$page?></a></li>
		<?else:?>
			<li class="pagination__item"><a class="pagination__link" href="<?=htmlspecialcharsbx($arResult["URL"])?>page<?=$page;?>/"><?=$page?></a></li>
		<?endif?>
		<?$page++?>
	<?endwhile?>

	<?if($arResult["CURRENT_PAGE"] < $arResult["PAGE_COUNT"]):?>
		<?if($arResult["PAGE_COUNT"] > 1):?>
			<li class="pagination__item"><a class="pagination__link" href="<?=htmlspecialcharsbx($arResult["URL"])?>page<?=$arResult["PAGE_COUNT"];?>/"><?=$arResult["PAGE_COUNT"]?></a></li>
			
		<?endif?>
			</div>
			<li class="pagination__item pagination__next">
				<a class="pagination__link" href="<?=htmlspecialcharsbx($arResult["URL"])?>page<?=$arResult["CURRENT_PAGE"]+1;?>/">
					<i class="fa fa-chevron-right"></i>
				</a>
			</li>
	<?else:?>
		<?if($arResult["PAGE_COUNT"] > 1):?>
			<li class="pagination__item is-active"><a class="pagination__link" href="#"><?=$arResult["PAGE_COUNT"]?></a></li>
		<?endif?>
			</div>
			<li class="pagination__item pagination__next">
				<a class="pagination__link" href="#">
					<i class="fa fa-chevron-right"></i>
				</a>
			</li>
	<?endif?>

		</ul>
</div>
