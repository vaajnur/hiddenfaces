<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if($arResult['nEndPage']==1){
	return;
}
if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<nav aria-label="Page navigation">
	<ul class="pagination">
<?if($arResult["bDescPageNumbering"] === true):?>

	<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if($arResult["bSavePage"]):?>
			
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a>
			|
		<?else:?>
			<?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
				<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_prev")?></a>
				|
			<?else:?>
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a>
				|
			<?endif?>
		<?endif?>
	<?else:?>
		<?=GetMessage("nav_prev")?>&nbsp;|
	<?endif?>

	<?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
		<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<b><?=$NavRecordGroupPrint?></b>
		<?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
			<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a>
		<?else:?>
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a>
		<?endif?>

		<?$arResult["nStartPage"]--?>
	<?endwhile?>

	|

	<?if ($arResult["NavPageNomer"] > 1):?>
		<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_next")?></a>
	<?else:?>
		<?=GetMessage("nav_next")?>
	<?endif?>

<?else:?>
	<?
	$arResult["sUrlPath"]=preg_replace('/page+[0-9]{4}/i', '',$arResult["sUrlPath"], -1, $count);
	$arResult["sUrlPath"]=preg_replace('/page+[0-9]{3}/i', '', $arResult["sUrlPath"], -1, $count);
	$arResult["sUrlPath"]=preg_replace('/page+[0-9]{2}/i', '', $arResult["sUrlPath"], -1, $count);
	$arResult["sUrlPath"]=preg_replace('/page+[0-9]{1}/i', '', $arResult["sUrlPath"], -1, $count);
	$arResult["sUrlPath"]=str_replace("//","/",$arResult["sUrlPath"]);
	?>
	<?if ($arResult["NavPageNomer"] > 1):?>

		<?if($arResult["bSavePage"]):?>
			<li class="pagination__item pagination__prev">
				<a class="pagination__link"  href="<?=$arResult["sUrlPath"]?>page<?=($arResult["NavPageNomer"]-1)?>/">
					<i class="fa fa-chevron-left"></i>
				</a>
			</li><div class="pagination__numbers">
			|
		<?else:?>
			<?if ($arResult["NavPageNomer"] > 2):?>
				<li class="pagination__item pagination__prev">
					<a class="pagination__link"  href="<?=$arResult["sUrlPath"]?>page<?=($arResult["NavPageNomer"]-1)?>/">
						<i class="fa fa-chevron-left"></i>
					</a>
				</li><div class="pagination__numbers">
			<?else:?>
				<li class="pagination__item pagination__prev">
					<a class="pagination__link"  href="<?=$arResult["sUrlPath"]?>">
						<i class="fa fa-chevron-left"></i>
					</a>
				</li><div class="pagination__numbers">
			<?endif?>
		<?endif?>

	<?else:?>
		<li class="pagination__item pagination__prev">
			<a class="pagination__link"  href="<?=$arResult["sUrlPath"]?>">
				<i class="fa fa-chevron-left"></i>
			</a>
		</li><div class="pagination__numbers">
	<?endif?>

	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<li class="pagination__item is-active"><a class="pagination__link" href="<?=$arResult["sUrlPath"]?>"><?=$arResult["nStartPage"]?></a></li>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
			<li class="pagination__item"><a class="pagination__link" href="<?=$arResult["sUrlPath"]?>"><?=$arResult["nStartPage"]?></a></li>
		<?else:?>
		<li class="pagination__item"><a class="pagination__link" href="<?=$arResult["sUrlPath"]?>page<?=$arResult["nStartPage"]?>/"><?=$arResult["nStartPage"]?></a></li>
		<?endif?>
		<?$arResult["nStartPage"]++?>
	<?endwhile?>

	<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		</div><li class="pagination__item pagination__next">
				<a class="pagination__link" href="<?=$arResult["sUrlPath"]?>page<?=($arResult["NavPageNomer"]+1)?>/">
					<i class="fa fa-chevron-right"></i>
				</a>
			</li>
	<?else:?>
		</div><li class="pagination__item pagination__next">
			<a class="pagination__link" href="">
				<i class="fa fa-chevron-right"></i>
			</a>
		</li>
	<?endif?>

<?endif?>

		</ul>
</div>