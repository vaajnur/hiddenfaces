<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()



$strReturn .= '<div class="bread col-12">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '<a title="'.$title.'" href="'.$arResult[$index]["LINK"].'">'.$title.'</a><span> &gt; </span>';
	}
	else
	{
		
		if(strlen($GLOBALS['SECTION_INDEX'])>0)
			$strReturn .= '<a>#'.$GLOBALS['SECTION_INDEX'].'</a>';
		else
			$strReturn .= '<a>'.$title.'</a>';
	}
}

$index = $itemSize-2;
$backLink = $arResult[$index];
	if($backLink["LINK"] <> "" and strpos($backLink["LINK"],'kategorii')!==false and $backLink["LINK"]!='/kategorii/') $strReturn .= '<br><a href="'.$backLink["LINK"].'" class="category-back">
		<img src="/local/templates/hiddenfaces/img/category-cards/arrow_back.svg" alt="" class="category-back-icon">
		В категорию выше
	</a>';
$strReturn .= ' </div>';


return $strReturn;
