<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section__margin">
	<h2 class="page__subtitle__h2">Свежие выпуски:</h2>
	<div class="novelties">
		<div class="row">	
<? foreach($arResult["ITEMS"] as $arItem): ?>
			<div class="col-sm-2">
				<a href="<?=$arItem['DETAIL_PAGE_URL'];?>">
					<? $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
					<img src="<?=$file['src'];?>" alt="" class="img-responsive">  
					<span class="novelties__title">
						<?=$arItem['NAME'];?>
					</span>
				</a>
			</div>
<? endforeach; ?>
		</div>
	</div>
</div>