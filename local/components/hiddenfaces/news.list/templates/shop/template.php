<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if(count($arResult["ITEMS"])>0){?>

<?
$index=0;
foreach($arResult["ITEMS"] as $arItem):
	$index++;
	if($index==5){?>
		<?$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				"PATH" => "/include/subscribe-block.php" //Указываем путь к файлу
			  )
		);?>
	<?}
	if($index==9){?>
		<?$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				"PATH" => "/include/banners.php" //Указываем путь к файлу
			  )
		);?>
	<?}
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="col-12 col-xl-4 col-lg-6">
			<div class="guest-item">
				<? $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
				<div class="guest-item-img" style="background-image:url(<?=$file['src'];?>)"></div>
				<div>
					<p class="guest-title"><?=$arItem['NAME'];?></p>
					<span><?=$arItem['PREVIEW_TEXT'];?></span>
					<p class="guest-desc"><?
						$rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arItem['ID'], 'CATALOG_GROUP_ID' => "1")); 
						if ($arPrice = $rsPrices->Fetch()) 
						{ 
							echo number_format($arPrice["PRICE"], 0, ',', '')." ";
						}
						?>
						<svg width="510.13px" height="510.13px" enable-background="new 0 0 510.127 510.127" version="1.1" viewBox="0 0 510.127 510.127" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path d="m34.786 428.96h81.158v69.572c0 3.385 1.083 6.156 3.262 8.322 2.173 2.18 4.951 3.27 8.335 3.27h60.502c3.14 0 5.857-1.09 8.152-3.27 2.295-2.166 3.439-4.938 3.439-8.322v-69.572h182.96c3.377 0 6.156-1.076 8.334-3.256 2.18-2.178 3.262-4.951 3.262-8.336v-46.377c0-3.365-1.082-6.156-3.262-8.322-2.172-2.18-4.957-3.27-8.334-3.27h-182.97v-42.754h123.18c48.305 0 87.73-14.719 118.29-44.199 30.551-29.449 45.834-67.49 45.834-114.12 0-46.604-15.283-84.646-45.834-114.12-30.557-29.45-69.989-44.199-118.29-44.199h-195.28c-3.385 0-6.157 1.089-8.335 3.256-2.173 2.179-3.262 4.969-3.262 8.335v227.9h-81.152c-3.384 0-6.157 1.145-8.335 3.439-2.172 2.295-3.262 5.012-3.262 8.151v53.978c0 3.385 1.083 6.158 3.262 8.336 2.179 2.18 4.945 3.256 8.335 3.256h81.158v42.754h-81.158c-3.384 0-6.157 1.09-8.335 3.27-2.172 2.166-3.262 4.951-3.262 8.322v46.377c0 3.385 1.083 6.158 3.262 8.336 2.178 2.181 4.95 3.257 8.335 3.257zm164.84-351.78h115.94c25.6 0 46.248 7.485 61.953 22.46 15.697 14.976 23.549 34.547 23.549 58.691 0 24.156-7.852 43.733-23.549 58.691-15.705 14.988-36.354 22.473-61.953 22.473h-115.94v-162.32z"/></svg></p>
					<button class="header__button btn open-form open-order-form" product-name="<?=$arItem['NAME'];?>" product-id="<?=$arItem['ID'];?>" formname="buy-form">Заказать</button>
				</div>
			</div>
		</div>
<?endforeach;?>
</div>
<div class="row">
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
<?}?>