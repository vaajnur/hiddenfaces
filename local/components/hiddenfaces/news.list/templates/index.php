<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Слушать подкасты бесплатно онлайн - студия Скрытые лица");
?>
        <div class="header">
          <div class="container inner__container">
            <div class="row header__row">
              <div class="col-lg-6 header__column header__column-left">
                <h1 class="header__title">Скрытые лица<span class="obj-2"></span></h1>
                <p class="header__description">Слушайте аудиопередачи на разные темы в любое удобное для вас время. Бесплатно.</p>
								<div class="itunes-sub">
									<a class="itunes-link" target="_blank" href="https://itunes.apple.com/ru/artist/%D1%81%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D0%B5-%D0%BB%D0%B8%D1%86%D0%B0/1329972836?mt=2&app=podcast"></a>
									<a class="soundstream" href="https://soundstream.media/channel/skrytyye-litsa" target="_blank">
															<img src="<?=SITE_TEMPLATE_PATH?>/img/e696baa.png" alt="">
														</a>
														<a class="yandex-music" target="_blank" href="https://music.yandex.ru/album/7045724">
		<img src="<?=SITE_TEMPLATE_PATH?>/img/icon_ym_header.svg" alt="">
	</a>
								</div>
              </div>
              <div class="col-lg-1"></div>
              <div class="col-lg-5 header__column header__column-right">
              	<?
				$arSelect = Array("ID", "NAME",'IBLOCK_SECTION_ID');
				$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'PROPERTY_MAIN_PODCAST_VALUE' => "Да");
				$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC","SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
				if($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					$name_today=$arFields['NAME'];
					if($arFields['IBLOCK_SECTION_ID']==29){
						$name_today="Детектив - ".$arFields['NAME'];
					}
					$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'PROPERTY_MAIN_PODCAST_VALUE'  => "Да");
				}
				else{
					$name_today="Сегодня в эфире";
					$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'<DATE_ACTIVE_FROM'  => date('d.m.Y H:i:s'),"!ID"=>19);  
				}
				$arSelect = Array();
				$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC","SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
				if($ob = $res->GetNextElement()){ 
					$arFields = $ob->GetFields();  
					$arProps = $ob->GetProperties();
					?>
					
					<h4 class="header__subtitle"><?=$name_today;?></h4>
					<div class="header__podcast"><a href="<?=$arFields['DETAIL_PAGE_URL'];?>?autoplay=Y">
						<? if(count($arProps['COVER_VARIANTS']['VALUE'])>1){?>
						<? $file = CFile::ResizeImageGet($arProps['COVER_VARIANTS']['VALUE'][rand(0,count($arProps['COVER_VARIANTS']['VALUE'])-1)], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
						<div class="cover-disc" style="background-image:url(<?=$file['src'];?>);"></div>
						<?}
						else{?>
						<? $file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
						<div class="cover-disc" style="background-image:url(<?=$file['src'];?>);"></div>
						<?}?>
						
						
					  <button class="player__button link_button player__button-header" onclick="yaCounter46865727.reachGoal('click_play_peredacha_<?=$arFields['IBLOCK_SECTION_ID'];?>');" id="button1"></button>
					  <audio class="html5-player" controls="controls" preload="none">
					  	<source src="<?=CFile::GetPath($arProps['PODCAST']['VALUE']);?>" type="audio/mpeg" /> Your browser does not support the audio element.
					  </audio>
					  <div class="player__audio" id="player1" style="display: none"></div>
					</a></div>
                <?
				}
				?>
              </div>
            </div>
            <hr class="separator">
			<?$APPLICATION->IncludeComponent("hiddenfaces:news.list","advantages",
				Array(
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => "hidden_faces",
					"IBLOCK_ID" => "4",
					"NEWS_COUNT" => "3",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => Array("ID"),
					"PROPERTY_CODE" => Array("DESCRIPTION"),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_LAST_MODIFIED" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Преимущества",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "Y",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"PAGER_BASE_LINK_ENABLE" => "Y",
					"SET_STATUS_404" => "Y",
					"SHOW_404" => "Y",
					"MESSAGE_404" => "",
					"PAGER_BASE_LINK" => "",
					"PAGER_PARAMS_NAME" => "arrPager",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
			);?>
          </div>
        </div>
		<?
			$GLOBALS['FILTER_MAIN_CATALOG']=array();
			//$GLOBALS['FILTER_PROGRAM']["PROPERTY_MAIN_PODCAST"]=false;
			$APPLICATION->IncludeComponent("hiddenfaces:news.list","podcasts_main",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "hidden_faces",
				"IBLOCK_ID" => "3",
				"NEWS_COUNT" => "9",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "FILTER_PROGRAM",
				"FIELD_CODE" => Array("ID","DATE_CREATE","CREATED_BY","ACTIVE_FROM"),
				"PROPERTY_CODE" => Array("DESCRIPTION","PODCAST","WAVEFORM_1","WAVEFORM_2",'TIME_LENGTH',"CATEGORIES","GUESTS","EIGHTEEN","AUTHORS","SOAUTHORS", "SEASON"), 
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_LAST_MODIFIED" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "Y",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"PAGER_BASE_LINK_ENABLE" => "Y",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
				"MESSAGE_404" => "",
				"PAGER_BASE_LINK" => "",
				"PAGER_PARAMS_NAME" => "arrPager",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
			)
		);?>
</div>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>