<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if(count($arResult["ITEMS"])>0){?>

<?
if(!function_exists(plural_form)){
	function plural_form($number,$before,$after) {
	  $cases = array(2,0,1,1,1,2);
	  echo $before[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]].' '.$number.' '.$after[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]];
	}
}
$index=0;
foreach($arResult["ITEMS"] as $arItem):
	$index++;
	if($index==5){?>
		<?$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				"PATH" => "/include/subscribe-block.php" //Указываем путь к файлу
			  )
		);?>
	<?}
	if($index==9){?>
		<?$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				"PATH" => "/include/banners.php" //Указываем путь к файлу
			  )
		);?>
	<?}
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="col-12 col-lg-6">
			<div class="guest-item">
				<? $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
				<a style="background-image:url()" href="<?=$arItem['DETAIL_PAGE_URL'];?>">
					<img src="<?=$file['src'];?>" alt="">
				</a>
				<div>
					<p class="guest-title"><a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><?=$arItem['NAME'];?></a></p>
					<span><?=html_entity_decode($arItem['DISPLAY_PROPERTIES']['SHORT_DESCRIPTION']["VALUE"]["TEXT"]);?></span>
					<p class="guest-desc"><?=html_entity_decode($arItem['DISPLAY_PROPERTIES']['STATUS']["VALUE"]["TEXT"]);?></p>
					<?
					$arSelect = Array("ID", "NAME");
					$arFilter = Array("IBLOCK_ID"=>3,"PROPERTY_MAIN_PODCAST"=>false,"PROPERTY_GUESTS"=>array($arItem['ID']), "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>100), $arSelect);
					$count_guests_podcasts=0;	   
					while($ob = $res->GetNextElement()){
						$count_guests_podcasts++;
					}
					$arSelect = Array("ID", "NAME");
					$arFilter = Array("IBLOCK_ID"=>3,"PROPERTY_MAIN_PODCAST"=>false,"PROPERTY_AUTHORS"=>array($arItem['ID']), "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>10000), $arSelect); 
					$authors=0;
					while($ob = $res->GetNextElement()){
						$authorArFields=$ob->GetFields();
						$authorsIds[]=$authorArFields['ID'];
						$authors++;
						$count_guests_podcasts++;
					}
					$arSelect = Array("ID", "NAME");
					$arFilter = Array("IBLOCK_ID"=>3,"PROPERTY_MAIN_PODCAST"=>false,"PROPERTY_SOAUTHORS"=>array($arItem['ID']), "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>10000), $arSelect); 
					while($ob = $res->GetNextElement()){
						$authorArFields=$ob->GetFields();
						$authorsIds[]=$authorArFields['ID'];
						$authors++;
						$count_guests_podcasts++;
					}
					$rsUsers = CUser::GetList($by="", $order="", array('=UF_AUTHOR_INFO' => $arItem['ID']));
					while($arr = $rsUsers->GetNext()) :
						$arSelect = Array("ID", "NAME");
						$arFilter = Array("IBLOCK_ID"=>3,"PROPERTY_MAIN_PODCAST"=>false,"PROPERTY_AUTHORS"=>false,"CREATED_BY"=>array($arr['ID']), "ACTIVE"=>"Y");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>10000), $arSelect);
						$count_guests_podcasts=0;	   
						while($ob = $res->GetNextElement()){
							$count_guests_podcasts++;
						}
					endwhile;
					?>
					<div class="programs__more">
						<a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="guest-link">
							<span>
								<? plural_form(
								$count_guests_podcasts,
								/* варианты написания для количества 1, 2 и 5 */
								array('','',''),
								array('подкаст','подкаста','подкастов')
								);?>
							</span>
							<i class="play-icon"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
<?endforeach;?>
</div>
<div class="row">
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
<?}?>