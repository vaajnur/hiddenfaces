<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?
echo "<!--";
// print_r($arResult["ITEMS"]);
echo "-->";


foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	// print_r($arItem['PROPERTIES']);
	$arItem["PREVIEW_GIF_SRC"] = CFile::GetPath($arItem["PROPERTIES"]["PREVIEW_GIF"]['VALUE']);
	// echo "<pre>";
	// print_r($arItem["PROPERTIES"]["VIDEO"]);
	// echo "</pre>";
	// print_r($arItem["PREVIEW_GIF_SRC"]);
	?>

	<!-- //id="<?=$this->GetEditAreaId($arItem['ID']);?>" -->

	<div class="col-lg-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="video-projects__item section__margin">
			<div class="video-projects__item-block">
				<div class="">
					<div class="video-projects__img-container">
							<div class="video-projects__img" style="background: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>') 0 0 cover no-repeat;"></div>
							<img class='video-project__play-btn' src="/upload/videoproekty/playBtn.svg">
							<video controls class='video-project__player' preload="auto">
							 <source type="video/mp4" src="<?=$arItem["PROPERTIES"]["VIDEO"]['VALUE']['path'];?>">
							</video>
					</div>
				</div>
				<div class="">
					<div class="video-projects__content">
						<h2 class="video-projects__title"><?echo $arItem["NAME"]?></h2>
						<div class="video-projects__date">Сделано для Матч ТВ</div>
						<div class="video-projects__desc">
							<p><?echo $arItem["PREVIEW_TEXT"];?></p>
						</div>
						<? if(!empty($arItem["PROPERTIES"]["YOUTUBE_LINK"]['VALUE'])):?>
						<div class="video-projects__more">
							<?
							foreach($arItem["PROPERTIES"]["YOUTUBE_LINK"]['VALUE'] as $k => $val):
							?>
							<a class='mb-4 d-block' target="_blank" href="<?=$val;?>">
								<span><?=$arItem["PROPERTIES"]["YOUTUBE_LINK"]['DESCRIPTION'][$k];?></span>
								<i class="play-icon"></i>
							</a>
						<? endforeach;?>
						</div>
						<?endif;?>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- 2-й вариант -->
	<div class="col-lg-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="video-projects__item section__margin">
			<div class="video-projects__item-block">
				<div class="">
					<div class="video-projects__img-container">
							<!-- <div class="video-projects__img" style="background: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"></div>
							<img class='video-project__play-btn' src="/upload/videoproekty/playBtn.svg"> -->
							<video controls class='video-project__player' preload="auto">
							 <source type="video/mp4" src="<?=$arItem["PROPERTIES"]["VIDEO"]['VALUE']['path'];?>">
							</video>
					</div>
				</div>
				<div class="">
					<div class="video-projects__content">
						<h2 class="video-projects__title"><?echo $arItem["NAME"]?></h2>
						<div class="video-projects__date">Сделано для Матч ТВ</div>
						<div class="video-projects__desc">
							<p><?echo $arItem["PREVIEW_TEXT"];?></p>
						</div>
						<? if(!empty($arItem["PROPERTIES"]["YOUTUBE_LINK"]['VALUE'])):?>
						<div class="video-projects__more">
							<?
							foreach($arItem["PROPERTIES"]["YOUTUBE_LINK"]['VALUE'] as $k => $val):
							?>
							<a class='mb-4 d-block' target="_blank" href="<?=$val;?>">
								<span><?=$arItem["PROPERTIES"]["YOUTUBE_LINK"]['DESCRIPTION'][$k];?></span>
								<i class="play-icon"></i>
							</a>
						<? endforeach;?>
						</div>
						<?endif;?>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- 	<div class="col-lg-4">
		<div class="video-projects__item section__margin">
			<div class="video-projects__item-block">
				<div class="">
					<div class="video-projects__img-container">
							<div class="video-projects__img"></div>
							<img class='video-project__play-btn' src="/upload/videoproekty/playBtn.svg">
							<video controls class='video-project__player' preload="auto">
							 <source type="video/mp4" src="/upload/videoproekty/test.mp4">
							</video>
					</div>
				</div>
				<div class="">
					<div class="video-projects__content">
						<h2 class="video-projects__title"><?echo $arItem["NAME"]?></h2>
						<div class="video-projects__date">Сделано для Матч ТВ</div>
						<div class="video-projects__desc">
							<p><?echo $arItem["PREVIEW_TEXT"];?></p>
						</div>
						<? if(!empty($arItem["PROPERTIES"]["YOUTUBE_LINK"]['VALUE'])):?>
						<div class="video-projects__more">
							<?
							foreach($arItem["PROPERTIES"]["YOUTUBE_LINK"]['VALUE'] as $k => $val):
							?>
							<a class='mb-4 d-block' target="_blank" href="<?=$val;?>">
								<span><?=$arItem["PROPERTIES"]["YOUTUBE_LINK"]['DESCRIPTION'][$k];?></span>
								<i class="play-icon"></i>
							</a>
						<? endforeach;?>
						</div>
						<?endif;?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="video-projects__item section__margin">
			<div class="video-projects__item-block">
				<div class="">
					<div class="video-projects__img-container">
							<div class="video-projects__img"></div>
							<img class='video-project__play-btn' src="/upload/videoproekty/playBtn.svg">
							<video controls class='video-project__player' preload="auto">
							 <source type="video/mp4" src="/upload/videoproekty/test.mp4">
							</video>
					</div>
				</div>
				<div class="">
					<div class="video-projects__content">
						<h2 class="video-projects__title"><?echo $arItem["NAME"]?></h2>
						<div class="video-projects__date">Сделано для Матч ТВ</div>
						<div class="video-projects__desc">
							<p><?echo $arItem["PREVIEW_TEXT"];?></p>
						</div>
						<? if(!empty($arItem["PROPERTIES"]["YOUTUBE_LINK"]['VALUE'])):?>
						<div class="video-projects__more">
							<?
							foreach($arItem["PROPERTIES"]["YOUTUBE_LINK"]['VALUE'] as $k => $val):
							?>
							<a class='mb-4 d-block' target="_blank" href="<?=$val;?>">
								<span><?=$arItem["PROPERTIES"]["YOUTUBE_LINK"]['DESCRIPTION'][$k];?></span>
								<i class="play-icon"></i>
							</a>
						<? endforeach;?>
						</div>
						<?endif;?>
					</div>
				</div>
			</div>
		</div>
	</div>
 -->

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
