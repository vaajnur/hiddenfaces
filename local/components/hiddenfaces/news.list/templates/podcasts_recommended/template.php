<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if(count($arResult["ITEMS"])>0){?>
<section class="section podcast" id="podcast">
	<h2 class="section__title podcast__title"><? echo $arParams["PODCASTS_TITLE"];?></h2>
	
<?
foreach($arResult["ITEMS"] as $arItem):
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="podcast__panel panel">
			<div class="container-fluid">
				<div class="row panel__row">
					<div class="col-sm-3 col-lg-2">
						<? $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
						<a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><div class="cover-avatar" style="background-image:url(<? echo $file['src']?>)"></div></a>
					</div>
					<div class="col-sm-9 col-lg-5">
						<a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="panel__name">
							<span class="season"><?=$arItem['DISPLAY_PROPERTIES']['SEASON']['VALUE']!=false?'S'.$arItem['DISPLAY_PROPERTIES']['SEASON']['VALUE']:'';?></span>
							<? if($arItem["IBLOCK_SECTION_ID"]!=29){?>
							<span>#<?
								$res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
								if($ar_res = $res->GetNext()){
									$section_name= $ar_res['NAME'];
									$section_id= $ar_res['ID'];
								}
									
								$arSelect = Array("ID","NAME");
								$arFilter = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$arItem["IBLOCK_SECTION_ID"], "ACTIVE"=>"Y"); 
								$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "ASC"), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
								$index=0;
								while($ob = $res->GetNextElement()){ 
									$index++;
									$arFields = $ob->GetFields();
									if($arFields['ID']==$arItem["ID"])
										$section_index=$index;
								}
								echo $section_index;
								?>
							</span>
							<?}
							else{
								$res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
								if($ar_res = $res->GetNext()){
									$section_name= $ar_res['NAME'];
									$section_id= $ar_res['ID'];
								}
							}?>
							<?echo $arItem["NAME"]?>
						</a>
						<p class="panel__description">
							<?
							$arSelect = Array("ID", "NAME","DETAIL_PAGE_URL");
							$arFilter = Array("IBLOCK_ID"=>6);
							$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
							while($ob = $res->GetNextElement()){
								$arFields=$ob->GetFields();
                // if (!empty($_GET['traced'])) var_dump('<pre>',trim($arFields['NAME']),$arFields['NAME'],strpos($arItem['PREVIEW_TEXT'],$arFields['NAME']),'</pre>');
								if(strpos($arItem['PREVIEW_TEXT'],$arFields['NAME'])!==false){
									$arItem['PREVIEW_TEXT']=str_replace($arFields['NAME'],"<a class='guest-podcast-link' href='".$arFields['DETAIL_PAGE_URL']."'>".$arFields['NAME']."</a>",$arItem['PREVIEW_TEXT']);
								}
							}
							?>
							<?echo $arItem["PREVIEW_TEXT"]?>
						</p>
						<div class="panel__info info">
							<time class="info__time">
								<? if (strlen($arItem["ACTIVE_FROM"])>0) echo explode(" ",$arItem["ACTIVE_FROM"])[0]; else echo explode(" ",$arItem["DATE_CREATE"])[0]?>
							</time>
							<?
							$categories="";
							$index_c=0;
              // if(count($arResult['DISPLAY_PROPERTIES']['CATEGORIES']['VALUE'])>0){
              if(count($arItem['DISPLAY_PROPERTIES']['CATS']['VALUE'])>0){
                // $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
                $arSelect = Array("ID", "NAME", "SECTION_PAGE_URL");
                $arFilter = Array("IBLOCK_ID"=>5,"ID"=>$arItem['DISPLAY_PROPERTIES']['CATS']['VALUE'], "ACTIVE"=>"Y");
                // $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5), $arSelect);
                $res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
                $index_c=0;
                while($ob = $res->GetNext()){
                  // $arFields = $ob->GetFields();  
                  // $arProps = $ob->GetProperties();
                  $categories.=",".$index_c.":'".$ob['NAME']."'";
                  if($index_cat%3==1&&$index_cat>1){?>
                  <?}
                  $index_c++;
                  ?>

                <a class="info__link" href="<?=$ob['SECTION_PAGE_URL'];?>">
                  <?echo $ob['NAME']?>
                </a>
                <?
                }
                /*
							if(count($arItem['DISPLAY_PROPERTIES']['CATEGORIES']['VALUE'])>0){
								$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
								$arFilter = Array("IBLOCK_ID"=>5,"ID"=>$arItem['DISPLAY_PROPERTIES']['CATEGORIES']['VALUE'], "ACTIVE"=>"Y");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5), $arSelect);
								while($ob = $res->GetNextElement()){
									$arFields = $ob->GetFields();  
									$arProps = $ob->GetProperties();
									if($index_cat%3==1&&$index_cat>1){?>
									<?}
									?>
								<a class="info__link" href="<?=$arFields['DETAIL_PAGE_URL'];?>">
									<?echo $arFields['NAME']?>
									<?
									$categories.=",".$index_c.":'".$arFields['NAME']."'";
									$index_c++;
									?>
								</a>
								<?
								}
                */
							}
							$categories = trim($categories, ",");
							$guests="";
							$index_c=0;
							if(count($arItem['DISPLAY_PROPERTIES']['GUESTS']['VALUE'])>0){
								$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
								$arFilter = Array("IBLOCK_ID"=>6,"ID"=>$arItem['DISPLAY_PROPERTIES']['GUESTS']['VALUE'], "ACTIVE"=>"Y");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
								while($ob = $res->GetNextElement()){
									$arFields = $ob->GetFields();
									?>
									<?
									$guests.=",".$index_c.":'".$arFields['NAME']."'";
									$index_c++;
									?>
								<?
								}
							}
							$guests = trim($guests, ",");
							$author="";
							$rsUser = CUser::GetByID($arItem["CREATED_BY"]); 
							$arUser = $rsUser->Fetch(); 
							$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
							if(count($arItem['DISPLAY_PROPERTIES']['AUTHORS']['VALUE'])==0)
								$arFilter = Array("IBLOCK_ID"=>7,"ID"=>$arUser['UF_AUTHOR_INFO'], "ACTIVE"=>"Y");
							else
								$arFilter = Array("IBLOCK_ID"=>7,"ID"=>$arItem['DISPLAY_PROPERTIES']['AUTHORS']['VALUE'], "ACTIVE"=>"Y");	
							$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
							
							while($ob = $res->GetNextElement()){
								$arFields = $ob->GetFields();?>
								<a class="info__link" href="<?=$arFields['DETAIL_PAGE_URL'];?>">
									<?echo $arFields['NAME']?>
									<? $author=$arFields['NAME']; ?>
								</a>

							<?}
							$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
							if(count($arItem['DISPLAY_PROPERTIES']['SOAUTHORS']['VALUE'])>0){
								$arFilter = Array("IBLOCK_ID"=>7,"ID"=>$arItem['DISPLAY_PROPERTIES']['SOAUTHORS']['VALUE'], "ACTIVE"=>"Y");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);

								while($ob = $res->GetNextElement()){
									$arFields = $ob->GetFields();?>
									<a class="info__link" href="<?=$arFields['DETAIL_PAGE_URL'];?>">
										<?echo $arFields['NAME']?>
									</a>

							<?}}
							?>
						</div>
					</div>
					<div class="col-sm-8 col-lg-4 player__container">
						<div class="panel__player player">
							<button onclick="yaCounter46865727.reachGoal('click_play_peredacha_<?=$section_id;?>');" class="player__button" podcast-id="<?=$arItem['ID'];?>" id="button<?=$indexx;?>"></button>
							<div class="player__audio" style="overflow:hidden;" id="player<?=$indexx;?>">
								<div class="player__waveform-1" style="background-image:url(<?=CFile::GetPath($arItem['DISPLAY_PROPERTIES']['WAVEFORM_1']['VALUE']);?>)"></div>
								<div class="player__waveform-2-wrapper"><div class="player__waveform-2" style="background-image:url(<?=CFile::GetPath($arItem['DISPLAY_PROPERTIES']['WAVEFORM_2']['VALUE']);?>)"></div></div>
								<audio class="html5-player" controls="controls" preload="none">
									<source src="<?=CFile::GetPath($arItem['DISPLAY_PROPERTIES']['PODCAST']['VALUE']);?>" type="audio/mpeg" /> Your browser does not support the audio element.
								</audio>
							</div>
							<div class="audio-forwarding">
								<button class="audio-backward"><i class="fa fa-backward" aria-hidden="true"></i></button>
								<button class="audio-forward"><i class="fa fa-forward" aria-hidden="true"></i></button>
							</div>
							<?
							$rsUser = CUser::GetByID($USER->GetID()); 
							$arUser = $rsUser->Fetch();
							$liked="";
							if($USER->IsAuthorized()&&in_array($arItem['ID'],$arUser['UF_LIKED_PODCASTS'])&&count($arUser['UF_LIKED_PODCASTS'])>0){
								$liked="player__like-checked";
							}
							if(strlen($_COOKIE['PODCAST_LIKED_'.$arItem['ID']])>0){
								$liked="player__like-checked";
							}
							?>
							<button podcast-id="<?=$arItem['ID'];?>" class="player__like <?=$liked;?>">
								<i class="fa fa-heart" aria-hidden="true"></i>
							</button>
						</div>
						<div class="time">
							<div class="time__current">
								<span class="time__minutes" id="current_minutes<?=$indexx;?>">00</span>
								<span>:</span>
								<span class="time__seconds" id="current_seconds<?=$indexx;?>">00</span>
							</div>
							<div class="time__duration">
								<?=$arItem['DISPLAY_PROPERTIES']['TIME_LENGTH']['VALUE'];?>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-lg-1">
						<div class="panel__footer">
							<div class="panel__download download">
								<a target="_blank" onclick="var goalParams = {guests: {<?=$guests;?>},categories: {<?=$categories;?>}, author: '<?=$author;?>',section: '<?=$section_name;?>'}; yaCounter46865727.reachGoal('download', goalParams);" download href="<?=CFile::GetPath($arItem['DISPLAY_PROPERTIES']['PODCAST']['VALUE']);?>" class="download__button">
									<? $arFile=CFile::GetById($arItem['DISPLAY_PROPERTIES']['PODCAST']['VALUE']); ?>
										<i class="fa fa-download" aria-hidden="true"></i>&nbsp;<span class="download__size">
											<? echo number_format(intval($arFile->arResult[0]['FILE_SIZE'])/1048576, 2, '.', ' ')."&nbsp;мб" ;?>
										</span>
								</a>
							</div>
							<div class="panel__social social">
								<ul class="social__list">
									<li class="social__item">
										<a  onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?=SITE_URL_WITH_HTTP.$arItem['DETAIL_PAGE_URL'];?>','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
											<i class="fa fa-facebook" aria-hidden="true"></i>
										</a>
									</li>
									<li class="social__item">
										<a onclick="window.open('http://vk.com/share.php?url=<?=SITE_URL_WITH_HTTP.$arItem['DETAIL_PAGE_URL'];?>&image=<? echo SITE_URL_WITH_HTTP.$arItem["PREVIEW_PICTURE"]['SRC']?>&noparse=true','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
											<i class="fa fa-vk" aria-hidden="true"></i>
										</a>
									</li>
									<li class="social__item">
										<a onclick="window.open('http://twitter.com/share?url=<?=SITE_URL_WITH_HTTP.$arItem['DETAIL_PAGE_URL'];?>&image-src=<? echo SITE_URL_WITH_HTTP.$arItem["PREVIEW_PICTURE"]['SRC']?>','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
											<i class="fa fa-twitter" aria-hidden="true"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?
			$rsUser = CUser::GetByID($USER->GetID()); 
			$arUser = $rsUser->Fetch();
			$liked="";
			if($USER->IsAuthorized()&&in_array($arItem['ID'],$arUser['UF_LISTENED_PODCASTS'])&&count($arUser['UF_LISTENED_PODCASTS'])>0){
				$class="podcast__label-listened";
				$text="Прослушано";
			}
			else if(strlen($_COOKIE['PODCAST_LISTENED_'.$arItem['ID']])>0){
				$class="podcast__label-listened";
				$text="Прослушано";
			}					   
			else{
				$class="podcast__label-latest";
				$text="Новинка";
			}
			?>
			<div class="podcast__label__wrapper">
				<div class="rating_r"><? echo strlen($arItem['DISPLAY_PROPERTIES']['EIGHTEEN']['VALUE'])>0?"18+":"";?></div>
				<div class="podcast__label <?=$class;?>">
					<span><?=$text;?></span>
				</div>
			</div>
		</div>
<?endforeach;?>
<? if($arParams['SHOW_BANNERS']!="N"){?>
	<?$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"WITHOUT_FOOTER"=>"Y",
			"PATH" => "/include/subscribe-block.php" //Указываем путь к файлу
		  )
	);?>
<?}?>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
	</section>
<?}?>