<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arFilter = array(
    "IBLOCK_ID" => $arParams["IBLOCK_ID"], 
	"ACTIVE"=>"Y",
);

$arSort = array(
   $arParams["SORT_FIELD"] => $arParams["SORT_ORDER"]
);
$arFilter = Array( 
"IBLOCK_ID"=>$arParams["IBLOCK_ID"], 
"ACTIVE"=>"Y",
"DEPTH_LEVEL"=>1 
); 

$arResult['COUNT_ITEMS']=CIBlockSection::GetCount($arFilter);
$rsSections = CIBlockSection::GetList($arSort, $arFilter,array("CNT_ACTIVE"=>"Y"),array("UF_*"),array("nPageSize"=>$arParams['ON_PAGE'],"iNumPage"=>$arParams['PAGE'])); //Получили список разделов из инфоблока
$rsSections->SetUrlTemplates(); //Получили строку URL для каждого из разделов (по формату из настроек инфоблока)
while($arSection = $rsSections->GetNext())
{
    $arResult["SECTIONS"][] = $arSection;//Сохранили выборку в $arResult для передачи в шаблон
}
$this->IncludeComponentTemplate();
?>