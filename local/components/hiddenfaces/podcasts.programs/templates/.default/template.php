<div class="mobile-layout">
<div class="row">
<?
$index=0;
if(!function_exists(plural_form)){
	function plural_form($number,$before,$after) {
	  $cases = array(2,0,1,1,1,2);
	  echo $before[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]].' '.$number.' '.$after[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]];
	}
}
foreach($arResult["SECTIONS"] as $arSection){
	if($index%2==0&&$index<count($arResult["SECTIONS"])&&$index>0){
	?></div>
	<? if($index==4){
		//6 новых подкастов
		$APPLICATION->IncludeComponent("hiddenfaces:news.list","podcasts_fresh",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "hidden_faces",
				"IBLOCK_ID" => "3",
				"NEWS_COUNT" => "6",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => Array("ID","DATE_CREATE","CREATED_BY"),
				"PROPERTY_CODE" => Array("DESCRIPTION","PODCAST","WAVEFORM_1","WAVEFORM_2",'TIME_LENGTH',"CATEGORIES","COVER_VARIANTS", "SEASON"), 
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_LAST_MODIFIED" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "Y",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"PAGER_BASE_LINK_ENABLE" => "Y",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
				"MESSAGE_404" => "",
				"PAGER_BASE_LINK" => "",
				"PAGER_PARAMS_NAME" => "arrPager",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
			)
		);
	}
	if($index==8){
		//форма подписки
		$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				"section__margin"=>"section__margin",
				"PATH" => "/include/subscribe-block.php" //Указываем путь к файлу
			  )
		);
	}?>
	
	<div class="row"><?
	}?>
	<div class="col-md-6">
			<div class="programs__item section__margin">
				<div class="row">
					<div class="col-sm-6">
						<? $file = CFile::ResizeImageGet($arSection['PICTURE'], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
						<a href="<?=$arSection['SECTION_PAGE_URL'];?>"><img src="<?=$file['src'];?>" class="img-responsive" alt=""></a>
					</div>
					<div class="col-sm-6">
						<div class="programs__content">
							<h2 class="programs__title"><a href="<?=$arSection['SECTION_PAGE_URL'];?>"><?=$arSection['NAME'];?></a></h2>
							<div class="programs__date"><? if(strlen($arSection['UF_SCHEDULE'])>0){?><?=$arSection['UF_SCHEDULE'];?><?}?></div>
							<div class="programs__desc">
								<?
								$ar_result=CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$arSection['IBLOCK_ID'], "ID"=>$arSection['ID']),false, Array("UF_SHORT_DESCRIPTION")); 
								?>
								<p><? if($res=$ar_result->GetNext()){ if(strlen($res["UF_SHORT_DESCRIPTION"])>0) echo $res["UF_SHORT_DESCRIPTION"]; else echo $arSection['DESCRIPTION']; }?></p>
							</div>
							<div class="programs__more">
								<a href="<?=$arSection['SECTION_PAGE_URL'];?>">
									<?
									$activeElements = CIBlockSection::GetSectionElementsCount($arSection['ID'], Array("CNT_ACTIVE"=>"Y"));
									
									?>
									<span>
									<? plural_form(
									  $activeElements,
									  /* варианты написания для количества 1, 2 и 5 */
									  array('','',''),
									  array('подкаст','подкаста','подкастов')
									);?>
									</span>
									<i class="play-icon"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?	
	$index++;
}
?>
</div>
</div>
<?
$filter = array("IBLOCK_ID"=>$arParams['IBLOCK_ID']);

$nav = new \Bitrix\Main\UI\PageNavigation();
$nav->allowAllRecords(true)
   ->setPageSize($arParams['ON_PAGE'])
   ->initFromUri();

$newsList = \Bitrix\Iblock\ElementTable::getList(
   array(
      "filter" => $filter,
      "offset" => ($offset = $nav->getOffset()),
      "limit" => (($limit = $nav->getLimit()) > 0? $limit + 1 : 0),
   )
);

$n = 0;

$nav->setRecordCount($arResult['COUNT_ITEMS']);
?>

<?
$APPLICATION->IncludeComponent(
   "hiddenfaces:main.pagenavigation",
   "",
   array(
      "NAV_OBJECT" => $nav,
      "SEF_MODE" => "Y",
      "SHOW_COUNT" => "N",
	   "CURRENT_PAGE" => $arParams['PAGE']
   ),
   false
);
?>
