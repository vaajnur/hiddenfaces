<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="container">
	<div class="row">
		<?
		$res = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
		if($ar_res = $res->GetNext()){
			$section_name= $ar_res['NAME'];
			$section_id= $ar_res['ID'];
		}
		$arSelect = Array("ID","NAME");
		$arFilter = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$arResult["IBLOCK_SECTION_ID"], "ACTIVE"=>"Y"); 
		$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "ASC"), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
		$index=0;
		while($ob = $res->GetNextElement()){ 
			$index++;
			$arFields = $ob->GetFields();
			if($arFields['ID']==$arResult["ID"])
				$section_index=$index;
		}
		if($arResult["IBLOCK_SECTION_ID"]==29){
			$breadtitle=$arResult["NAME"];
		}
		else{
			$breadtitle="<b>#".$section_index."</b> ".$arResult["NAME"];
		}
		$GLOBALS['SECTION_INDEX']=$section_index;
		$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				"WITHOUT_ROW"=>"Y",
				"TITLE"=>$breadtitle,
				"EIGHTEEN"=>strlen($arResult['DISPLAY_PROPERTIES']['EIGHTEEN']['VALUE'])>0?"18+":"",
				 "SECTION_INDEX"=>$section_index,
				"section__margin"=>"section__margin",
				"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
			  )
		);
		?>
		<?
		if($arResult["IBLOCK_SECTION_ID"]==29){
			$APPLICATION->SetTitle("#".$section_index." ".$arResult["NAME"]);
		}
		?>
		<? $file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
		<? $file2 = CFile::ResizeImageGet($arResult["PREVIEW_PICTURE"], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
		<?$APPLICATION->AddHeadString('<meta property="og:title" content="#'.$section_index.' '.$arResult["NAME"].'" />',true)?>
		<?$APPLICATION->AddHeadString('<meta property="og:description" content="'.htmlspecialchars(strip_tags($arResult['PREVIEW_TEXT'])).'" />',true)?>
		<?$APPLICATION->AddHeadString('<meta property="og:type" content="website" />',true)?>
		<?$APPLICATION->AddHeadString('<meta property="og:url" content="'.SITE_URL_WITH_HTTP.$arResult['DETAIL_PAGE_URL'].'" />',true)?>
		<?$APPLICATION->AddHeadString('<meta property="og:image" content="'.SITE_URL_WITH_HTTP.$file2["src"].'" />',true)?>
		<div class="col-12 col-lg-3">
			
			<div class="cover-disc" style="background-image:url(<? if(strlen($file["src"])>0) echo $file["src"]; else $file2["src"]; ?>)"></div>
			<span class="img-author"><? if(strlen($arResult['DISPLAY_PROPERTIES']['COVER_AUTHOR']['VALUE'])>0){?>Автор обложки: <?=$arResult['DISPLAY_PROPERTIES']['COVER_AUTHOR']['VALUE'];?><?}?></span>
			
		</div>
		<div class="col-12 col-lg-6">
			<div class="pod-info"><?
			$arSelect = Array("ID", "NAME","DETAIL_PAGE_URL");
			$arFilter = Array("IBLOCK_ID"=>6);
			$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNextElement()){
				$arFields=$ob->GetFields();
				if(strpos($arResult['DETAIL_TEXT'],$arFields['NAME'])!==false){
					$arResult['DETAIL_TEXT']=str_replace($arFields['NAME'],"<a class='guest-podcast-link' href='".$arFields['DETAIL_PAGE_URL']."'>".$arFields['NAME']."</a>",$arResult['DETAIL_TEXT']);
				}
				if(strpos($arResult['PREVIEW_TEXT'],$arFields['NAME'])!==false){
					$arResult['PREVIEW_TEXT']=str_replace($arFields['NAME'],"<a class='guest-podcast-link' href='".$arFields['DETAIL_PAGE_URL']."'>".$arFields['NAME']."</a>",$arResult['PREVIEW_TEXT']);
				}
			}
			if(strlen($arResult['DETAIL_TEXT'])>0) echo html_entity_decode($arResult['DETAIL_TEXT']); else echo html_entity_decode($arResult['PREVIEW_TEXT']);?></div>
			<div class="space desktop__hidden"></div>
		</div>
		<div class="col-lg-3">
			<? if(count($arResult['DISPLAY_PROPERTIES']['GUESTS']['VALUE'])>0){
				$guests="Сегодняшний гость - ";
				if(count($arResult['DISPLAY_PROPERTIES']['GUESTS']['VALUE'])>1){
					$guests="Сегодняшние гости - ";
				}
				?>
				<h2 class="podcast__title">Гости:</h2>
				<? foreach($arResult['DISPLAY_PROPERTIES']['GUESTS']['VALUE'] as $guest){?>
				<?
				$arSelect = Array("ID", "NAME","PROPERTY_SHORT_DESCRIPTION","PREVIEW_PICTURE","DETAIL_PAGE_URL");
				$arFilter = Array("IBLOCK_ID"=>6,"ID"=>$guest);
				$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
				if($ob = $res->GetNextElement()){
					$arFields=$ob->GetFields();?>
					<div class="invited-person">
					<? $file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
					<div class="guest-avatar" style="background-image:url(<?=$file['src'];?>)"></div>
					<div>
						<p><a href="<?=$arFields['DETAIL_PAGE_URL'];?>"><?=$arFields['NAME'];?></a></p>
						<? $guests.=$arFields['NAME'].", "; ?>
						<p><?=$arFields['PROPERTY_SHORT_DESCRIPTION_VALUE']['TEXT'];?></p>
					</div>
				</div>
				<?}?>

				<?}?>
			<?
			$guests=trim($guests);
			$guests=trim($guests,",");
			$description = $APPLICATION->GetProperty("description");
			$GLOBALS['guests']=$guests;
			}?>
		</div>
	</div>
	<div class="row pod-page-player detail-player">
		<div class="col-12">
			<div class="panel__player player">
				<button class="player__button"  onclick="yaCounter46865727.reachGoal('click_play_peredacha_<?=$section_id;?>');" podcast-id="<?=$arResult['ID'];?>" id="button<?=$indexx;?>"></button>
				<div class="player__audio" style="overflow:hidden;" id="player<?=$indexx;?>">
					<div class="player__waveform-1" style="background-image:url(<?=CFile::GetPath($arResult['DISPLAY_PROPERTIES']['WAVEFORM_1']['VALUE']);?>)"></div>
					<div class="player__waveform-2-wrapper"><div class="player__waveform-2" style="background-image:url(<?=CFile::GetPath($arResult['DISPLAY_PROPERTIES']['WAVEFORM_2']['VALUE']);?>)"></div></div>
					<audio class="html5-player" controls="controls" preload="none">
						<source src="<?=CFile::GetPath($arResult['DISPLAY_PROPERTIES']['PODCAST']['VALUE']);?>" type="audio/mpeg" /> Your browser does not support the audio element.
					</audio>
					
				</div>
				<div class="audio-forwarding">
					<button class="audio-backward"><i class="fa fa-backward" aria-hidden="true"></i></button>
					<button class="audio-forward"><i class="fa fa-forward" aria-hidden="true"></i></button>
				</div>
				<?
				$rsUser = CUser::GetByID($USER->GetID()); 
				$arUser = $rsUser->Fetch();
				$liked="";
				if(in_array($arResult['ID'],$arUser['UF_LIKED_PODCASTS'])&&count($arUser['UF_LIKED_PODCASTS'])>0){
					$liked="player__like-checked";
				}
				if(strlen($_COOKIE['PODCAST_LIKED_'.$arResult['ID']])>0){
					$liked="player__like-checked";
				}
				?>

			</div>
			<div class="time">
				<div class="time__current">
					<span class="time__minutes" id="current_minutes<?=$indexx;?>">00</span>
					<span>:</span>
					<span class="time__seconds" id="current_seconds<?=$indexx;?>">00</span>
				</div>
				<div class="time__duration">
					<?=$arResult['DISPLAY_PROPERTIES']['TIME_LENGTH']['VALUE'];?>
				</div>
			</div>
			<div class="panel__footer-info">
				<div class="panel__footer">
					<button podcast-id="<?=$arResult['ID'];?>" class="player__like <?=$liked;?>">
						<i class="fa fa-heart" aria-hidden="true"></i>
					</button>
					<div class="panel__download download">

						<a onclick="downloadYa();" target="_blank" download href="<?=CFile::GetPath($arResult['DISPLAY_PROPERTIES']['PODCAST']['VALUE']);?>" class="download__button">
							<span>Скачать </span>
							<i class="fa fa-download" aria-hidden="true"></i>
							<? $arFile=CFile::GetById($arResult['DISPLAY_PROPERTIES']['PODCAST']['VALUE']); ?>
								<span class="download__size">
									<? echo number_format(intval($arFile->arResult[0]['FILE_SIZE'])/1048576, 2, '.', ' ')." мб" ;?>
								</span>
						</a>
					</div>
					<div class="panel__social social">
						<span>Поделиться </span>
						<ul class="social__list">
							<li class="social__item">
								<a  onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?=SITE_URL_WITH_HTTP.$arResult['DETAIL_PAGE_URL'];?>','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>
							</li>
							<li class="social__item">
								<a onclick="window.open('http://vk.com/share.php?url=<?=SITE_URL_WITH_HTTP.$arResult['DETAIL_PAGE_URL'];?>&image=<? echo SITE_URL_WITH_HTTP.$arResult["PREVIEW_PICTURE"]['SRC']?>&noparse=true','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
									<i class="fa fa-vk" aria-hidden="true"></i>
								</a>
							</li>
							<li class="social__item">
								<a onclick="window.open('http://twitter.com/share?url=<?=SITE_URL_WITH_HTTP.$arResult['DETAIL_PAGE_URL'];?>&image-src=<? echo SITE_URL_WITH_HTTP.$arResult["PREVIEW_PICTURE"]['SRC']?>','facebook-share-dialog','width=626,height=436');return false;" href="#" class="social__link">
									<i class="fa fa-twitter" aria-hidden="true"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="panel__info info">
					<time class="info__time"><? if (strlen($arResult["ACTIVE_FROM"])>0) echo explode(" ",$arResult["ACTIVE_FROM"])[0]; else echo explode(" ",$arResult["DATE_CREATE"])[0]?></time>
					<?
					$categories="";
					// if(count($arResult['DISPLAY_PROPERTIES']['CATEGORIES']['VALUE'])>0){
					if(count($arResult['DISPLAY_PROPERTIES']['CATS']['VALUE'])>0){
						// $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
						$arSelect = Array("ID", "NAME", "SECTION_PAGE_URL");
						$arFilter = Array("IBLOCK_ID"=>5,"ID"=>$arResult['DISPLAY_PROPERTIES']['CATS']['VALUE'], "ACTIVE"=>"Y");
						// $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5), $arSelect);
						$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
						$index_c=0;
						while($ob = $res->GetNext()){
							// $arFields = $ob->GetFields();  
							// $arProps = $ob->GetProperties();
							$categories.=",".$index_c.":'".$ob['NAME']."'";
							if($index_cat%3==1&&$index_cat>1){?>
							<?}
							$index_c++;
							?>

						<a class="info__link" href="<?=$ob['SECTION_PAGE_URL'];?>">
							<?echo $ob['NAME']?>
						</a>
						<?
						}
					}
					$categories = trim($categories, ",");
					$guests="";
					$index_c=0;
					if(count($arResult['DISPLAY_PROPERTIES']['GUESTS']['VALUE'])>0){
						$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
						$arFilter = Array("IBLOCK_ID"=>6,"ID"=>$arResult['DISPLAY_PROPERTIES']['GUESTS']['VALUE'], "ACTIVE"=>"Y");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
						while($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields();
							?>
							<?
							$guests.=",".$index_c.":'".$arFields['NAME']."'";
							$index_c++;
							?>
						<?
						}
					}
					$guests = trim($guests, ",");
					$author="";
					$rsUser = CUser::GetByID($arResult["CREATED_BY"]); 
					$arUser = $rsUser->Fetch(); 
					$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
					if(count($arResult['DISPLAY_PROPERTIES']['AUTHORS']['VALUE'])==0)
						$arFilter = Array("IBLOCK_ID"=>7,"ID"=>$arUser['UF_AUTHOR_INFO'], "ACTIVE"=>"Y");
					else
						$arFilter = Array("IBLOCK_ID"=>7,"ID"=>$arResult['DISPLAY_PROPERTIES']['AUTHORS']['VALUE'], "ACTIVE"=>"Y");			   
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
					while($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields();?>
						<a class="info__link" href="<?=$arFields['DETAIL_PAGE_URL'];?>">
							<?echo $arFields['NAME']?>
							<? $author=$arFields['NAME']; ?>
						</a>

					<?}
					$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
					if(count($arResult['DISPLAY_PROPERTIES']['SOAUTHORS']['VALUE'])>0){
						$arFilter = Array("IBLOCK_ID"=>7,"ID"=>$arResult['DISPLAY_PROPERTIES']['SOAUTHORS']['VALUE'], "ACTIVE"=>"Y");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);

						while($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields();?>
							<a class="info__link" href="<?=$arFields['DETAIL_PAGE_URL'];?>">
								<?echo $arFields['NAME']?>
							</a>

					<?}}
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="space"></div>
	<form action="" class="back-form">
		<div class="row align-items-stretch">
			<h2 class="podcast__title col-12">Напишите автору:</h2>
			<div class="sm-space"></div>
			<div class="col-12 col-md-6">
				<input class="name" type="text" placeholder="Введите имя">
				<input class="email" type="text" placeholder="Введите e-mail">
			</div>
			<div class="col-12 col-md-6">
				<textarea class="message" rows="3" placeholder="Ваше сообщение..."></textarea>
			</div>
			<div class="col-12">
				<div class="sm-space"></div>
				<label for="personal-form" class="label__checkbox">Согласие на обработку персональных данных
					<input type="checkbox" class="check" id="personal-form" checked="">
					<span class="checkmark"></span>
				</label>
				<button class="header__button write-author btn">Отправить</button>
			</div>
		</div>
	</form>
	<?
	//3 рекомендуемых
	$GLOBALS['FILTER_PROGRAM']=array();
	$GLOBALS['FILTER_PROGRAM']["IBLOCK_SECTION_ID"]=$arResult["IBLOCK_SECTION_ID"];
	$GLOBALS['FILTER_PROGRAM']["!ID"]=$arResult['ID'];
	$APPLICATION->IncludeComponent("hiddenfaces:news.list", "podcasts_recommended",
			Array(
				"PODCASTS_TITLE" => "Выпуски:",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"FILTER_NAME" => "FILTER_PROGRAM",
				"IBLOCK_TYPE" => "hidden_faces",
				"IBLOCK_ID" => "3",
				"NEWS_COUNT" => "9",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FIELD_CODE" => Array("ID", "DATE_CREATE", "CREATED_BY", "ACTIVE_FROM"),
				"PROPERTY_CODE" => Array("DESCRIPTION", "PODCAST", "WAVEFORM_1", "WAVEFORM_2", 'TIME_LENGTH', "CATEGORIES", "CATS", "GUESTS", "COVER_VARIANTS", "EIGHTEEN", "AUTHORS", "SOAUTHORS", "SEASON"),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_LAST_MODIFIED" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"PAGER_BASE_LINK_ENABLE" => "Y",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
				"MESSAGE_404" => "",
				"PAGER_BASE_LINK" => $arResult['DETAIL_PAGE_URL'],
				"PAGER_PARAMS_NAME" => "arrPager",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
			)
		);
	?>
</div>
<script>
function downloadYa(){
	var goalParams = {guests: {<?=$guests;?>},categories: {<?=$categories;?>}, author: '<?=$author;?>',section: '<?=$section_name;?>'};
	yaCounter46865727.reachGoal('download', goalParams);
}
jQuery(document).ready(function($){
	<? if($_REQUEST['autoplay']=="Y"){?>
	setTimeout(function() { yaCounter46865727.reachGoal('click_play_peredacha_<?=$arResult['IBLOCK_SECTION_ID'];?>'); }, 2000);
	$(".detail-player .player__button").attr("onclick","");
	$(".detail-player .player__button").trigger("click");
	<?}?>
});
</script>