<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<p class="active">
		  <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		</p>
	<?else:?>
		<p>
		  <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		</p>
	<?endif?>
	
<?endforeach?>

<?endif?>