<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<form id="subscribe<?=$arParams['CODE_FORM'];?>" method="post" action="<?=$arResult["FORM_ACTION"]?>">
	<div class="input-group">
		<input type="hidden" name="do_subscribe" value="Y">
		<input type="hidden" name="rubric_id" value="<?=$arParams['RUBRIC_ID'];?>">
		<input type="email" class="emaill" name="email" placeholder="info@komilfo-butik.com" value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>" />
		<button class="btn btn-fullorange">ПОДПИСАТЬСЯ</button>
	</div>
	
</form>
<script>
$("form#subscribe<?=$arParams['CODE_FORM'];?>").submit(function(){
	$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Загрузка...");
        $.ajax({
            type: "POST",
            url: "<?=$arResult["FORM_ACTION"]?>",
            data:$(this).serialize(),
             success: function(response){
				 //console.log(response);
				if(response!="ok"){
					$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("ПОДПИСАТЬСЯ");
					$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").addClass("error");
				}
				else{
					yaCounter46865727.reachGoal('SUB');
					gtag('event', 'submit', {'event_category': 'subscribe','event_label': 'submitSubscribe'});
					$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписка оформлена");
					$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").removeClass("error");
					$("#subscribe<?=$arParams['CODE_FORM'];?>").addClass("success");
					setTimeout(function() {
                        if($( ".popup.firsttime" ).hasClass( "opened_popup" )){
                            $(".popup.firsttime").fadeOut().removeClass('opened_popup');
                        }
						$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").val("");
						$("#subscribe<?=$arParams['CODE_FORM'];?>").removeClass("success");
						$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("ПОДПИСАТЬСЯ");
					}, 5000);
				}		
			}
        });
		return false;
     });
</script>
