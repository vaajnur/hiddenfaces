<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<form class="subscribe__form form" id="subscribe<?=$arParams['CODE_FORM'];?>" method="post" action="<?=$arResult["FORM_ACTION"]?>">
	<input type="hidden" name="do_subscribe" value="Y">
	<div class="form__group form__group-type">
      <label for="daily" class="form__label">Ежедневно
        <input type="radio" name="rubric_id" class="form__input" id="daily" value="1" checked="">
        <span class="checkmark"></span>
      </label>
      <label for="weekly" class="form__label">Еженедельно
        <input type="radio" name="rubric_id" class="form__input" value="2" id="weekly">
        <span class="checkmark"></span>
      </label>
    </div>
    <div class="form__group form__group-subscription">
      <i class="fa fa-paper-plane subscribe__icon" aria-hidden="true"></i>
      <input type="text" class="form__input form__input-subscription emaill" name="email" value="<?=$arResult["EMAIL"]?>" placeholder="E-mail адрес">
      <button class="subscribe__button">Подписаться</button>
    </div>
    <div class="form__group form__group-personal">
      <label for="personal">Согласие на обработку персональных данных
        <input type="checkbox" name="personal"  class="form__input" id="personal" checked="">
        <span class="checkmark"></span>
      </label>
    </div>
	
</form>
<?
	// включаем буфер
	ob_start();

	// выводим информацию
	?>
	 <script>
		addWaveForm(<?=$indexx;?>,"<?=CFile::GetPath($arItem['DISPLAY_PROPERTIES']['PODCAST']['VALUE']);?>");
	</script>
	 <?
	// сохраняем всё что есть в буфере в переменную $content
	$GLOBALS['ADVANCED_SCRIPTS'].= ob_get_contents();

	// отключаем и очищаем буфер
	ob_end_clean();
	?>
<script>
$("form#subscribe<?=$arParams['CODE_FORM'];?>").submit(function(){
	$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Загрузка...");
        $.ajax({
            type: "POST",
            url: "<?=$arResult["FORM_ACTION"]?>",
            data:$(this).serialize(),
             success: function(response){
				console.log(response);
				if(response!="ok"){
					$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписаться");
					$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").parents(".form__group").addClass("form__group-error");
				}
				else{
					yaCounter46865727.reachGoal('SUB');
					gtag('event', 'submit', {'event_category': 'subscribe','event_label': 'submitSubscribe'});
					fbq('track', 'Lead');
					$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Готово");
					$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").parents(".form__group").removeClass("form__group-error");
					$("#subscribe<?=$arParams['CODE_FORM'];?> .form__group").addClass("form__group-success");
					setTimeout(function() {
                        if($( ".popup.firsttime" ).hasClass( "opened_popup" )){
                            $(".popup.firsttime").fadeOut().removeClass('opened_popup');
                        }
						$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").val("");
						$("#subscribe<?=$arParams['CODE_FORM'];?> .form__group").removeClass("form__group-success");
						$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписаться");
					}, 5000);
				}		
			}
        });
		return false;
     });
</script>
