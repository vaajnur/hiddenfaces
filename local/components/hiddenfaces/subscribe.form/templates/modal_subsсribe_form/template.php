<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<form class="subscribe__form form" id="subscribe<?=$arParams['CODE_FORM'];?>" method="post" action="<?=$arResult["FORM_ACTION"]?>">
	<input type="hidden" name="do_subscribe" value="Y">
	<input type="hidden" name="rubric_id" class="form__input" value="<?=$arParams['RUBRIC_ID'];?>" checked="">
	<div class="row">
		<div class="col-lg-6">
				<label for="" class="modal-label">
					<input class="name input modal-input" type="text" name="name" value="<?=$arResult["NAME"]?>">
					<span class="input-name">Ваше имя</span>
				</label>
		</div>
		<div class="col-lg-6">
				<label for="" class="modal-label">
					<input type="text" class="email input modal-input" name="email" value="<?=$arResult["EMAIL"]?>" placeholder="E-mail адрес">
					<span class="input-name">Ваша почта</span>
				</label>
		</div>
	</div>
	<div class="row modal-footer">
		<div class="col-lg-6 modal-column-left">
			<button class="modal-btn subscribe__button">Отправить</button>
		</div>
		<div class="col-lg-6 modal-column-right">
			<label for="personal-form" class="label__checkbox">
				<span class="text">Вы соглашаетесь с условиями обработки персональных данных</span>
				<input type="checkbox" class="check" name="personal" id="personal-form" checked="">
				<span class="checkmark"></span>
			</label>
		</div>
	</div>
</form>
<?
	// включаем буфер
	ob_start();

	// выводим информацию
	?>
	 <script>
		addWaveForm(<?=$indexx;?>,"<?=CFile::GetPath($arItem['DISPLAY_PROPERTIES']['PODCAST']['VALUE']);?>");
	</script>
	 <?
	// сохраняем всё что есть в буфере в переменную $content
	$GLOBALS['ADVANCED_SCRIPTS'].= ob_get_contents();

	// отключаем и очищаем буфер
	ob_end_clean();
	?>
<script>
$("form#subscribe<?=$arParams['CODE_FORM'];?>").submit(function(){
	$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Загрузка...");
        $.ajax({
            type: "POST",
            url: "<?=$arResult["FORM_ACTION"]?>",
            data:$(this).serialize(),
             success: function(response){
				console.log(response);
				if(response!="ok"){
					$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписаться");
					$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").parents(".form__group").addClass("form__group-error");
				}
				else{
					yaCounter46865727.reachGoal('SUB');
					ga('send',  'event',  'subscribe',  'submit',  'submitSubscribe'); 
					fbq('track', 'Lead');
					$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Готово");
					$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").parents(".form__group").removeClass("form__group-error");
					$("#subscribe<?=$arParams['CODE_FORM'];?> .form__group").addClass("form__group-success");
					setTimeout(function() {
						$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").val("");
						$("#subscribe<?=$arParams['CODE_FORM'];?> .form__group").removeClass("form__group-success");
						$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписаться");
					}, 5000);
				}		
			}
        });
		return false;
     });
</script>
