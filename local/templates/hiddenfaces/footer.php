<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
		
		</div>	
	<? $not_sub="Y";
	if($USER->IsAuthorized()){
		CModule::IncludeModule("subscribe");
		$subscription = CSubscription::GetByEmail($USER->GetEmail());
		$subscription->ExtractFields("str_");
		if($str_ID){ 
			$not_sub="N";
		}
	}
	?>
	<? if(!isset($_COOKIE['COOKIE_ALERT'])&&$not_sub=="Y"){?>
	<div class="cookie subscribe">
		<div class="subscribe__panel mainpage__shadowpanel">
			<button class="closecookie"><i class="fa fa-close"></i></button>
				<h4 class="subscribe__title">Получайте анонсы новых подкастов в удобном ритме</h4>
				<form class="subscribe__form form" id="subscribeCOOKIE" method="post" action="/ajax/subscribe.php">
	<input type="hidden" name="do_subscribe" value="Y">
	<div class="form__group form__group-type">
      <label for="dailysubscribeCOOKIE" class="form__label">Ежедневно
        <input type="radio" name="rubric_id" class="form__input" id="dailysubscribeCOOKIE" value="1" checked="">
        <span class="checkmark"></span>
      </label>
      <label for="weeklysubscribeCOOKIE" class="form__label">Еженедельно
        <input type="radio" name="rubric_id" class="form__input" value="2" id="weeklysubscribeCOOKIE">
        <span class="checkmark"></span>
      </label>
    </div>
    <div class="form__group form__group-subscription">
      <i class="fa fa-paper-plane subscribe__icon" aria-hidden="true"></i>
      <input type="text" class="form__input form__input-subscription emaill" name="email" value="" placeholder="E-mail адрес">
      <button class="subscribe__button">Подписаться</button>
    </div>
    <div class="form__group form__group-personal">
      <label for="personalsubscribeCOOKIE">Согласие на обработку персональных данных
        <input type="checkbox" name="personal" class="form__input" id="personalsubscribeCOOKIE" checked="">
        <span class="checkmark"></span>
      </label>
    </div>
	
</form>
<script>
$("form#subscribeCOOKIE").submit(function(){
	$("#subscribeCOOKIE button").text("Загрузка...");
        $.ajax({
            type: "POST",
            url: "/ajax/subscribe.php",
            data:$(this).serialize(),
             success: function(response){
				console.log(response);
				if(response!="ok"){
					$("#subscribeCOOKIE button").text("Подписаться");
					$("#subscribeCOOKIE input[name='email']").parents(".form__group").addClass("form__group-error");
				}
				else{
					yaCounter46865727.reachGoal('SUB');
					gtag('event', 'submit', {'event_category': 'subscribe','event_label': 'submitSubscribe'});
					$("#subscribeCOOKIE button").text("Готово");
					var d = new Date();
        			d.setDate(d.getDate() + 365);
       				 setCookie("COOKIE_ALERT","1",{expires: d,path: "/"});
					$("#subscribeCOOKIE input[name='email']").parents(".form__group").removeClass("form__group-error");
					$("#subscribeCOOKIE .form__group").addClass("form__group-success");
					$('.cookie.subscribe .subscribe__panel.mainpage__shadowpanel').animate({
						bottom: '-100px'
					}, 500);
					setTimeout(function() {
                        if($( ".popup.firsttime" ).hasClass( "opened_popup" )){
                            $(".popup.firsttime").fadeOut().removeClass('opened_popup');
                        }
						$("#subscribeCOOKIE input[name='email']").val("");
						$("#subscribeCOOKIE .form__group").removeClass("form__group-success");
						$("#subscribeCOOKIE button").text("Подписаться");
					}, 5000);
				}		
			}
        });
		return false;
     });
</script>
			</div>
		</div>	
	<?}?>			
		<div class="footer">
				<div class="container">
					<div class="row footer__row">
						<div class="col-sm-2 col-lg-2">
							<img class="footer-logo" src="<?=SITE_TEMPLATE_PATH?>/img/footer/logo_footer.png" alt="" class="img-responsive">
						</div>
						<div class="col-sm-4 col-lg-4">
							<div class="footer__social">
								<p>Подписывайтесь на нас:</p>
								<ul class="footer__list">
									<li class="footer__item">
										<a href="https://vk.com/hiddenfacesru" target="_blank" class="item">
											<i class="fa fa-vk" aria-hidden="true"></i>
										</a>
									</li>
									<li class="footer__item">
										<a href="https://www.facebook.com/hiddenfaces.ru/" target="_blank" class="item">
											<i class="fa fa-facebook" aria-hidden="true"></i>
										</a>
									</li>
									<li class="footer__item">
										<a href="https://twitter.com/hiddenfacesru" target="_blank" class="item">
											<i class="fa fa-twitter" aria-hidden="true"></i>
										</a>
									</li>
									<li class="footer__item">
										<a href="https://www.instagram.com/podcaststudio/" target="_blank" class="item">
											<i class="fa fa-instagram" aria-hidden="true"></i>
										</a>
									</li>
									<li class="footer__item">
										<a href="https://www.youtube.com/channel/UCGauKoZxKx4-z276hIVxY2A" target="_blank" class="item">
											<i class="fa fa-youtube-play" aria-hidden="true"></i>
										</a>
									</li>
								</ul>
								<div class="footer__info mobile__hidden">
									<p>&#9400; 2017 Скрытые лица</p>
									<p>Все права защищены</p>
									<a href="/politika-konfidencialnosti/">Политика конфиденциальности</a>
									<br>
									<a href="/sitemap/">Карта сайта</a>
								</div>
							</div>
						</div>
						<!-- <div class="col-lg-1 mobile__hidden"></div> -->
						<div class="col-6 col-sm-3 col-lg-2"><ul>
								<?$APPLICATION->IncludeComponent("hiddenfaces:menu", "bottom", array(
									"ROOT_MENU_TYPE" => "bottom",
									"MENU_CACHE_TYPE" => "Y",
									"MENU_CACHE_TIME" => "36000000",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(
									),
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "left",
									"USE_EXT" => "N",
									"ALLOW_MULTI_SELECT" => "N"
									),
									false
								);?>
								<!-- <li class="desktop__hidden">
									<a class="arkvision" href="http://arkvision.pro/" target="_blank">Разрабтано с умом<br>
										<img src="<?=SITE_TEMPLATE_PATH?>/img/ark-logo-white.svg" alt="arkvision-logo"></a>
								</li> -->
							</ul></div>
						<div class="col-6 col-sm-3 col-lg-2">
							<ul>
								<?$APPLICATION->IncludeComponent("hiddenfaces:menu", "bottom", array(
									"ROOT_MENU_TYPE" => "bottom2",
									"MENU_CACHE_TYPE" => "Y",
									"MENU_CACHE_TIME" => "36000000",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(
									),
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "left",
									"USE_EXT" => "N",
									"ALLOW_MULTI_SELECT" => "N"
									),
									false
								);?>
								<!-- <li class="desktop__hidden"><a href="/kontakty/">Контакты</a></li> -->
							</ul>
						</div>
						<div class="col-lg-2 footer__last">
							<a class="applepostcard" href="https://itunes.apple.com/ru/artist/%D1%81%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D0%B5-%D0%BB%D0%B8%D1%86%D0%B0/1329972836?mt=2&app=podcast" target="_blank">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/podcast-lrg.svg" alt="">
							</a>
							<a class="soundstream" href="https://soundstream.media/channel/skrytyye-litsa" target="_blank">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/e696baa.png" alt="">
							</a>
							<a class="yandex-music" target="_blank" href="https://music.yandex.ru/album/7045724">
		<img src="<?=SITE_TEMPLATE_PATH?>/img/icon_ym_footer.svg" alt="">
	</a>
							<p class="mobile__hidden"><?$APPLICATION->IncludeComponent("hiddenfaces:menu", "bottom3", array(
								"ROOT_MENU_TYPE" => "bottom3",
								"MENU_CACHE_TYPE" => "Y",
								"MENU_CACHE_TIME" => "36000000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(
								),
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "left",
								"USE_EXT" => "N",
								"ALLOW_MULTI_SELECT" => "N"
								),
								false
							);?></p>
							<a class="arkvision" href="http://arkvision.pro/" target="_blank">Разработано с умом<br>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/ark-logo-white.svg" alt="arkvision-logo"></a>
							<div class="footer__info desktop__hidden">
									<p>&#9400; 2017 Скрытые лица</p>
									<p>Все права защищены</p>                  
									<a href="/politika-konfidencialnosti/">Политика конфиденциальности</a>
									<a href="/sitemap/">Карта сайта</a>
								</div>
						</div>
					</div>
				</div>
			</div>
			<? if(!$USER->IsAuthorized()){
				if(CModule::IncludeModule("socialservices"))
				{
					$oAuthManager = new CSocServAuthManager();
					$arServices = $oAuthManager->GetActiveAuthServices($arResult);
				
					if(!empty($arServices))
					{
						$arResult["AUTH_SERVICES"] = $arServices;
						if(isset($_REQUEST["auth_service_id"]) && $_REQUEST["auth_service_id"] <> '' && isset($arResult["AUTH_SERVICES"][$_REQUEST["auth_service_id"]]))
						{
							$arResult["CURRENT_SERVICE"] = $_REQUEST["auth_service_id"];
							if(isset($_REQUEST["auth_service_error"]) && $_REQUEST["auth_service_error"] <> '')
							{
								$arResult['ERROR_MESSAGE'] = $oAuthManager->GetError($arResult["CURRENT_SERVICE"], $_REQUEST["auth_service_error"]);
							}
							elseif(!$oAuthManager->Authorize($_REQUEST["auth_service_id"]))
							{
								$ex = $APPLICATION->GetException();
								if ($ex)
									$arResult['ERROR_MESSAGE'] = $ex->GetString();
							}
						}
					}
				}	
			?>
			<div class="popup-background authorization">
				<div class="popup">
					<div class="closecontent">
						<span></span>
						<span></span>
					</div>
					<h3>Авторизация</h3>
					<div class="contentauth">
						<p>Быстрая авторизация через соцсети:</p>
						<div class="linkcontent">
							<?
								$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat",
									array(
										"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
										"SUFFIX"=>"form",
									),
									$component,
									array("HIDE_ICONS"=>"Y")
								);
							?>
						</div>
					</div>
					<input type="email" name="login" placeholder="E-Mail">
					<input type="password" name="password" placeholder="Пароль">
					<button>Войти в личный кабинет</button>
					<a href="#" class="losepass">Забыли пароль?</a>
				</div>
			</div>
			<div class="popup-background registr">
				<div class="popup">
					<div class="closecontent">
						<span></span>
						<span></span>
					</div>
					<h3>Регистрация на сайте</h3>
					<div class="contentauth">
						<p>Быстрая регистрация через соцсети:</p>
						<div class="linkcontent">
							<?
								$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat",
									array(
										"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
										"SUFFIX"=>"form",
									),
									$component,
									array("HIDE_ICONS"=>"Y")
								);
							?>
						</div>
					</div>
					<input type="text" name="USER_NAME" placeholder="Ваше имя">
					<input type="email" name="USER_EMAIL" placeholder="E-Mail">
					<input type="password" name="USER_PASSWORD" placeholder="Пароль">
					<button>Зарегистрироваться</button>
					<a href="#" class="entrynow">Уже зарегистрированы? Войти</a>
					<p class="notes">Ваши контакты не попадут в руки 3-х лиц: мы бережно их храним в соответсвии с 152-ФЗ &#171;О защите персональных данных&#187;</p>
				</div>
			</div>

			<div class="popup-background misspassword">
				<div class="popuppass passw">
					<div class="closecontent">
						<span></span>
						<span></span>
					</div>
					<h3>Забыли пароль</h3>
					<form>
					<input type="email" name="login" placeholder="E-mail">
					<script src='https://www.google.com/recaptcha/api.js'></script>
					<div class="g-recaptcha" data-sitekey="6LcHWVAUAAAAAOgxC1wDSI_Hamoh5g9diBxPIi8Z"></div>
					<button>Восстановить</button>
					</form>
				</div>
			</div>
			<?}?>
	</div>
	<? if($USER->IsAuthorized()&&!isset($_COOKIE['logined'])){?>
	<script>
	jQuery(document).ready(function($){
		var d = new Date();
       d.setDate(d.getDate() + 30);
       setCookie("logined","1",{expires: d,path: "/"});
	});
	</script>
	<?}?>

	<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46865727 = new Ya.Metrika({ id:46865727, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/46865727" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
	<? //echo $GLOBALS['ADVANCED_SCRIPTS']; ?>
</body>
</html>