<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
CModule::IncludeModule("iblock");
CModule::IncludeModule("main");
CModule::IncludeModule("sale");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<title><?$APPLICATION->ShowTitle()?></title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css.map">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style-alex.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/lightslider.min.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/magnific-popup.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/1.4.0/wavesurfer.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jplayer.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/lightslider.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.magnific-popup.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.snow.min.1.0.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/script-back.js"></script>
		<script src='<?=SITE_TEMPLATE_PATH?>/js/jquery.cookie.js'></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114325285-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-114325285-1');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '193809368018156');
		fbq('track', 'PageView');
	</script>
	<noscript>
		<img height="1" width="1"
			 src="https://www.facebook.com/tr?id=193809368018156&ev=PageView
&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	<?$APPLICATION->ShowHead();?>
</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<? if(!isset($_COOKIE['itunes-sub'])) {?>
	<!--<div class="itunes-sub">
		<a class="itunes-link" href="https://itunes.apple.com/ru/artist/%D1%81%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D0%B5-%D0%BB%D0%B8%D1%86%D0%B0/1329972836?mt=2&app=podcast"></a>
		<a class="soundstream" href="https://soundstream.media/channel/skrytyye-litsa" target="_blank">
								<img src="<1?=SITE_TEMPLATE_PATH?>/img/e696baa.png" alt="">
							</a>
		<a class="yandex-music" target="_blank" href="https://music.yandex.ru/album/7045724">
		<img src="<1?=SITE_TEMPLATE_PATH?>/img/icon_ym_header.svg" alt="">
	</a>
		<button class="close-itunes-sub"><i class="fa fa-close"></i></button>
	</div> -->
	<?}?>
	<div class="page">
		<div class="page__content">
			<div class="container no-padding relativecont">

			<nav class="navigation" data-spy="affix" data-offset-top="47">
				<a href="/" class="navigation__logo">
				<img class="img-responsive" src="<?=SITE_TEMPLATE_PATH?>/img/logo_final.png" alt="logo">
				</a>
				<ul class="navigation__list">
				<?$APPLICATION->IncludeComponent("hiddenfaces:menu", "top", array(
					"ROOT_MENU_TYPE" => "top",
					"MENU_CACHE_TYPE" => "Y",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "left",
					"USE_EXT" => "N",
					"ALLOW_MULTI_SELECT" => "N"
					),
					false
				);?>
				<li class="navigation__item navigation__item-search">
					<i class="fa fa-search navigation__icon navigation__icon-search" aria-hidden="true"></i>
					<form action="/poisk/">
							<input type="text" name="q" placeholder="Поиск" class="navigation__search">
					</form>
				</li>
				<li class="navigation__item lkcontent">
					<? if(!$USER->IsAuthorized()){ ?>
					<a href="#" class="entrypopup">Вход</a>
					<span>|</span>
					<a href="#" class="regpopup">Регистрация</a>
					<?} else{?>
					<a href="/lk/" class="">Личный кабинет</a>
					<?}?>

				</li>
				</ul>
				<div class="navigation__mobile">
				<span></span>
				<span></span>
				<span></span>
				</div>
				<!--<div class="obj-1"></div>-->
			</nav>
		</div>
		