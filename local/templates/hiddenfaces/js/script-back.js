jQuery(document).ready(function($){
	var intervalForward;
	var timeForward=0;
	
	$(".audio-forward").on('mousedown', function(e) {
		var d = new Date();
		d.setDate(d.getDate() + 30);
		timeForward=0;
		var elem=$(this);
		setCookie("podcasts_forwarding",1,{expires: d,path: "/"});
		elem.parents(".panel__player").find(".player__button").trigger("click");
		elem.parents(".panel__player").find(".player__button").trigger("click");
		
		intervalForward=setInterval(function() {
			timeForward=timeForward+200;
			if(timeForward>500){
				elem.parents(".panel__player").find(".html5-player")[0].currentTime=elem.parents(".panel__player").find(".html5-player")[0].currentTime+1;
			}
		  }, 200);
	});
	$(".audio-forward").on('mouseup', function(e) {
		deleteCookie("podcasts_forwarding");
		var elem=$(this);
		if(timeForward<=500){
			elem.parents(".panel__player").find(".html5-player")[0].currentTime=elem.parents(".panel__player").find(".html5-player")[0].currentTime+15;
		}
		clearInterval(intervalForward);
	});
	$(".audio-backward").on('mousedown', function(e) {
		var d = new Date();
		d.setDate(d.getDate() + 30);
		timeForward=0;
		var elem=$(this);
		setCookie("podcasts_forwarding",1,{expires: d,path: "/"});
		elem.parents(".panel__player").find(".player__button").trigger("click");
		elem.parents(".panel__player").find(".player__button").trigger("click");
		intervalForward=setInterval(function() {
			timeForward=timeForward+200;
			if(timeForward>500){
				elem.parents(".panel__player").find(".html5-player")[0].currentTime=elem.parents(".panel__player").find(".html5-player")[0].currentTime-1;
			}
		  }, 200);
	});
	$(".audio-backward").on('mouseup', function(e) {
		deleteCookie("podcasts_forwarding");
		var elem=$(this);
		if(timeForward<=500){
			elem.parents(".panel__player").find(".html5-player")[0].currentTime=elem.parents(".panel__player").find(".html5-player")[0].currentTime-15;
		}
		clearInterval(intervalForward);
	});
	$('.html5-player').prop("muted",true);
	$(".player__button").append('<i class="fa fa-spinner" aria-hidden="true"></i>');
	$(".header__random-podcast").on('click', function(e) {
		var oldtxt=$(this).text();
		$(this).text("Загрузка...");
		$.ajax({
			type:"POST",
			url:"/ajax/random_podcast.php",
			data:{
			},
			success: function(response){
				$(".header__column-right").html(response);
				$(".header__random-podcast").text(oldtxt);
				//$(".header__podcast .html-player").trigger("click");
			}
		});
	});
	
	var likeButton = $('.player__like');

	likeButton.on('click', function() {
		var podcast_id=$(this).attr("podcast-id");
		if($(this).hasClass("player__like-checked")){
			$.ajax({
				type:"POST",
				url:"/ajax/liked.php",
				data:{
					ID:podcast_id,
					LIKE:"N"
				},
				success: function(response){
					console.log(response);
				}
			});
		}
		else{
			$.ajax({
				type:"POST",
				url:"/ajax/liked.php",
				data:{
					ID:podcast_id,
					LIKE:"Y"
				},
				success: function(response){
					yaCounter46865727.reachGoal('like_podcast');
					console.log(response);
				}
			});
		}
	  	$(this).toggleClass('player__like-checked');
	});
	
	$(".player__waveform-2").css("width",$(".player__waveform-1").css("width"));
	$(".player__audio").on('click', function(e) {
		
		var $this = $(this); // or use $(e.target) in some cases;
		var offset = $this.offset();
		var width = $this.width();
		var height = $this.height();
		var posX = offset.left;
		var posY = offset.top;
		var x = e.pageX-posX;
			x = parseInt(x/width*100,10);
			x = x<0?0:x;
			x = x>100?100:x;
		var y = e.pageY-posY;
			y = parseInt(y/height*100,10);
			y = y<0?0:y;
			y = y>100?100:y;
		//console.log(x+'% '+y+'%');
		if(!$(this).parents(".panel__player").find(".player__button").hasClass("player__button-playing")){
			$(this).parents(".panel__player").find(".player__button").trigger("click");
		}
		if(!isNaN($(this).parents(".panel__player").find(".html5-player")[0].duration))
			$(this).parents(".panel__player").find(".html5-player")[0].currentTime=$(this).parents(".panel__player").find(".html5-player")[0].duration*x/100;
			
		else{
			var hms = $(this).parents(".player__container").find(".time__duration").text();   // your input string
			if($(this).parents(".pod-page-player").find(".html5-player").length>0){
				hms = $(this).parents(".pod-page-player").find(".time__duration").text();   // your input string
			}
			hms=hms.trim();
			var a = hms.split(':'); // split it at the colons
			// minutes are worth 60 seconds. Hours are worth 60 minutes.
			var seconds = (+a[0]) * 60 + (+a[1]); 
			if(a.length==3)
				seconds = (+a[0]) * 60 * 60 +(+a[1]) * 60 + (+a[2]);
			//console.log(seconds*x/100);
			//$(this).parents(".panel__player").find(".player__button").trigger("click");
			$(this).parents(".panel__player").find(".html5-player")[0].currentTime=seconds*x/100;
		}
	});
	
	$(document).on('click','.player__button',function(){
		/*if($(this).hasClass("link_button")){
			location.replace($(this).attr("href"));
			return false;
		}*/
		var d = new Date();
		d.setDate(d.getDate() + 30);
		if(getCookie("podcasts_listen_count").length==0)
			setCookie("podcasts_listen_count",0,{expires: d,path: "/"});
			$(".html5-player").trigger("pause");
			console.log(getCookie("logined"));
			//тут ограничение на кол-во прослушиваний до формы регистрации
		if(getCookie("logined").length>0||parseInt(getCookie("podcasts_listen_count"))</*3*/222222222222222222222222222222||parseInt(getCookie("podcasts_forwarding"))>0){
			var podcast_id=$(this).attr("podcast-id");
			$.ajax({
				type:"POST",
				url:"/ajax/listened.php",
				data:{
					ID:podcast_id
				},
				success: function(response){
					gtag('event', 'play', {'event_category': 'podcast','event_label': 'playPodcast'});
					yaCounter46865727.reachGoal('listen_podcast');
					//console.log(response);
					
				}
			});
			$(this).parents(".podcast__panel").find(".podcast__label").removeClass("podcast__label-latest");
			$(this).parents(".podcast__panel").find(".podcast__label").addClass("podcast__label-listened");
			$(this).parents(".podcast__panel").find(".podcast__label span").text("прослушано");
			if($(this).parents(".header__podcast").length>0){
				if($(this).hasClass("player__button-playing")){
					$(this).parents(".header__podcast").find(".html5-player").trigger("pause");
				}
				else{
					if(parseInt(getCookie("podcasts_listen_count"))>0&&getCookie("podcasts_forwarding").length==0)
						setCookie("podcasts_listen_count",parseInt(getCookie("podcasts_listen_count"))+1,{expires: d,path: "/"});
					else if(getCookie("podcasts_forwarding").length==0)
						setCookie("podcasts_listen_count",1,{expires: d,path: "/"});
					$(this).addClass("fa-spin");
					$(this).parents(".header__podcast").find(".html5-player").trigger("play");
					$('.player__button-playing').removeClass("player__button-playing");
				}
				$($(this).parents(".header__podcast").find(".html5-player")).bind('timeupdate', updateTime);
				this.classList.toggle('player__button-playing');
				return false;
			}
			$(this).parents(".panel__player").find(".player__waveform-2").css("width",$(this).parents(".panel__player").find(".player__waveform-1").css("width"));
			if($(this).hasClass("player__button-playing")){
				$(this).parents(".panel__player").find(".html5-player").trigger("pause");
				
			}
			else{
				$(this).addClass("fa-spin");
				console.log(parseInt(getCookie("podcasts_forwarding"))+"forward");
				if(parseInt(getCookie("podcasts_listen_count"))>0&&getCookie("podcasts_forwarding").length==0)
						setCookie("podcasts_listen_count",parseInt(getCookie("podcasts_listen_count"))+1,{expires: d,path: "/"});
				else if(getCookie("podcasts_forwarding").length==0)
					setCookie("podcasts_listen_count",1,{expires: d,path: "/"});
				$(this).parents(".panel__player").find(".html5-player").trigger("play");
				$('.player__button-playing').parents(".panel__player").find(".html5-player").trigger("pause");
				$('.player__button-playing').removeClass("player__button-playing");
				$($(this).parents(".panel__player").find(".html5-player")).bind('timeupdate', updateTime);
			}
			this.classList.toggle('player__button-playing');
		}
		else{
			$('.authorization').fadeIn('normal').css('display', 'flex');
			$('body').addClass('bodyoverflow');
			this.classList.toggle('player__button-playing');
		}
    });
	var updateTime = function () { 
		var thisPlayer = $(this);
		thisPlayer.prop("muted",false);
		$(".player__button").removeClass("fa-spin");
		if(this.currentTime>=this.duration){
			$(".player__button").removeClass("player__button-playing");
		}
		var widthOfProgressBar = Math.floor((100 / this.duration) * this.currentTime);
		$(this).parents(".panel__player").find(".player__waveform-2-wrapper").css("width",""+parseInt(widthOfProgressBar)+"%");
		var date = new Date(null);
		date.setSeconds(parseInt(this.currentTime)); // specify value for SECONDS here
		if(parseInt(this.currentTime)==300){
			yaCounter46865727.reachGoal('listen_podcast_5m');
		}
		if(parseInt(this.currentTime)==900){
			yaCounter46865727.reachGoal('listen_podcast_15m');
		}
		if(this.currentTime==this.duration/2){
			yaCounter46865727.reachGoal('listen_podcast_half');
		}
		if(this.currentTime==this.duration){
			yaCounter46865727.reachGoal('listen_podcast_full');
		}
		var result = date.toISOString().substr(11, 8);
		result = result.replace("00:", "");
		$(this).parents(".panel__player").find(".player__button").removeClass("fa-spin");
		
		$(this).parents(".player__container").find(".time__current").html(result);
		if($(this).parents(".pod-page-player").find(".html5-player")){
			
			$(this).parents(".pod-page-player").find(".time__current").html(result);
		}
		//console.log(widthOfProgressBar);
	}
	//форма написать автору
	$( document ).on( "click", ".write-author", function() {
       var oldtxt=$(this).text();
       $(".back-form input").removeClass("error");
		$(".back-form textarea").removeClass("error");
		var check="";
		if($(".back-form input[type='checkbox']:checked").length>0){
            check="1";
        }
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax/form.author.php',
           data:{
                name:$(".back-form .name").val(),
                email:$(".back-form .email").val(),
                message:$(".back-form .message").val(),
			    check:check,
           },
           success: function(ans){
              console.log(ans);
			   
               if(ans.indexOf("okidoki")!=-1){
                   $(".back-form").addClass("success");
                   $(".write-author").text("Отправлено");
				   $(".write-author").attr("disabled","disabled");
				   //$("#message_order_fit").html("<br>Менеджер свяжется с Вами в течение часа");
                   setTimeout(function() {
                            $(".back-form").removeClass("success");
                            $(".write-author").text(oldtxt);
					   		$(".write-author").removeAttr("disabled","disabled");
					   		//$("#message_order_fit").text("");
                    }, 10000);
               }
               else{
				   var ans = ans.split(";");
					for (var x = 0; x < ans.length; x++) {
						var class_error=ans[x].split("-");

						if(class_error[0].length>0)
							$(".back-form ."+class_error[0]).addClass("error");
					}
                    $(".write-author").text(oldtxt);
               }
           }
       });
		return false;
	});
	//форма обратной связи в контактах
	$( document ).on( "click", ".write-feedback", function() {
       var oldtxt=$(this).text();
        $(".back-form input").removeClass("error");
		$(".back-form textarea").removeClass("error");
		var check="";
		if($(".back-form input[type='checkbox']:checked").length>0){
            check="1";
        }
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax/form.feedback.php',
           data:{
                name:$(".back-form .name").val(),
                email:$(".back-form .email").val(),
                message:$(".back-form .message").val(),
			    theme:$(".back-form .theme option:selected").val(),
			    check:check,
           },
           success: function(ans){
              console.log(ans);
                
               if(ans.indexOf("okidoki")!=-1){
                   $(".back-form").addClass("success");
                   $(".write-feedback").text("Отправлено");
				   $(".write-feedback").attr("disabled","disabled");
				   //$("#message_order_fit").html("<br>Менеджер свяжется с Вами в течение часа");
                   setTimeout(function() {
                            $(".back-form").removeClass("success");
                            $(".write-feedback").text(oldtxt);
					   		$(".write-feedback").removeAttr("disabled","disabled");
					   		//$("#message_order_fit").text("");
                    }, 10000);
               }
               else{
				   var ans = ans.split(";");
					for (var x = 0; x < ans.length; x++) {
						var class_error=ans[x].split("-");
						if(class_error[0].length>0)
							$(".back-form ."+class_error[0]).addClass("error");
					}
                    $(".write-feedback").text(oldtxt);
               }
           }
       });
		return false;
	});
	//форма стать автором
	$( document ).on( "click", ".write-anketa", function() {
       var oldtxt=$(this).text();
       $(".back-form input").removeClass("error");
		$(".back-form textarea").removeClass("error");
		var check="";
		if($(".back-form input[type='checkbox']:checked").length>0){
            check="1";
        }
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax/form.feedback.php',
           data:{
                name:$(".back-form .name").val(),
                email:$(".back-form .email").val(),
                message:$(".back-form .message").val(),
			    phone:$(".back-form .phone").val(),
			   	link:$(".back-form .link").val(),
			    city:$(".back-form .city").val(),
			    check:check,
           },
           success: function(ans){
              console.log(ans);
              var ans = ans.split(";");
				
               if(ans.indexOf("okidoki")!=-1){
                   $(".back-form").addClass("success");
                   $(".write-anketa").text("Отправлено");
				   $(".write-anketa").attr("disabled","disabled");
				   //$("#message_order_fit").html("<br>Менеджер свяжется с Вами в течение часа");
                   setTimeout(function() {
                            $(".back-form").removeClass("success");
                            $(".write-anketa").text(oldtxt);
					   		$(".write-anketa").removeAttr("disabled","disabled");
					   		//$("#message_order_fit").text("");
                    }, 10000);
               }
               else{
				   for (var x = 0; x < ans.length; x++) {
						var class_error=ans[x].split("-");
						console.log(class_error[0]);
						if(class_error[0].length>0)
							$(".back-form ."+class_error[0]).addClass("error");
					}
                    $(".write-anketa").text(oldtxt);
               }
           }
       });
		return false;
	});
	//форма заказа
	$( document ).on( "click", ".open-order-form", function() {
		$(".buy-form .product-id").val($(this).attr("product-id"));
		$(".buy-form .modal-title").text($(this).attr("product-name"));
	});
	$( document ).on( "click", ".write-order", function() {
       var oldtxt=$(this).text();
       $(".buy-form input").removeClass("error");
		var check="";
		if($(".buy-form input[type='checkbox']:checked").length>0){
            check="1";
        }
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax/form.order.php',
           data:{
                name:$(".buy-form .name").val(),
			    phone:$(".buy-form .phone").val(),
			   	productid:$(".buy-form .product-id").val(),
			    check:check,
           },
           success: function(ans){
              console.log(ans);
              var ans = ans.split(";");
				
               if(ans.indexOf("okidoki")!=-1){
                   $(".buy-form .success-text").show();
                   $(".write-order").text("Отправлено");
				   $(".write-order").attr("disabled","disabled");
				   //$("#message_order_fit").html("<br>Менеджер свяжется с Вами в течение часа");
                   setTimeout(function() {
                            $(".buy-form .success-text").hide();
                            $(".write-order").text(oldtxt);
					   		$(".write-order").removeAttr("disabled","disabled");
					   		//$("#message_order_fit").text("");
                    }, 10000);
               }
               else{
				   for (var x = 0; x < ans.length; x++) {
						var class_error=ans[x].split("-");
						console.log(class_error[0]);
						if(class_error[0].length>0)
							$(".buy-form ."+class_error[0]).addClass("error");
					}
                    $(".write-order").text(oldtxt);
               }
           }
       });
		return false;
	});
	//вход в лк
	$( document ).on( "click", ".authorization button", function() {
		var oldtxt=$(this).text();
		$(".authorization  input").removeClass("error");
		$(this).text("Загрузка...");
		$.ajax({
			url: '/ajax/form.auth.php',
			data:{
				 login:$(".authorization input[name=login]").val(),
				 password:$(".authorization input[name=password]").val()
			},
			success: function(ans){
			   console.log(ans);
				if(ans.indexOf("error-login")!=-1){
					$(".authorization input[name='login']").addClass("error");
					$(".authorization button").text(oldtxt);
				}
				if(ans.indexOf("error-password")!=-1){
					$(".authorization input[name='password']").addClass("error");
					$(".authorization button").text(oldtxt);
			 }
				if(ans.indexOf("okidoki")!=-1){
				   location.reload();
				}
			}
		});
		  return false;
	 });
	 $( document ).on( "click", ".losepass", function(e) {
	 	e.preventDefault();
		$('.misspassword').fadeIn('normal').css('display', 'flex');
		$(this).parent().parent().fadeOut('slow');
	 });
	 //регистрация
	 $( document ).on( "click", ".registr button", function() {
		var oldtxt=$(this).text();
		 $(".registr  input").removeClass("error");
		$(this).text("Загрузка...");
		$.ajax({
			url: '/ajax/form.reg.php',
			data:{
				 USER_NAME:$(".registr input[name=USER_NAME]").val(),
				 USER_EMAIL:$(".registr input[name=USER_EMAIL]").val(),
				 USER_PASSWORD:$(".registr input[name=USER_PASSWORD]").val()
			},
			success: function(ans){
			   console.log(ans);
				if(ans.indexOf("name-error")!=-1){
					$(".registr input[name='USER_NAME']").addClass("error");
				}
				if(ans.indexOf("email-error")!=-1){
					$(".registr input[name='USER_EMAIL']").addClass("error");
				}
				if(ans.indexOf("password-error")!=-1){
					$(".registr input[name='USER_PASSWORD']").addClass("error");
				}
				if(ans.indexOf("okidoki")!=-1){
					var d = new Date();
                   	d.setDate(d.getDate() + 30);
					setCookie("logined","1",{expires: d,path: "/"});
					location.reload();
				}
				else{
					 $(".registr button").text(oldtxt);
				 }
			}
			
		});
		return false;
	 }); 
	 //диалоговое окно на выход
	 $( document ).on( "click", ".link-exit", function() {
		 $(".popup-agreement .popup-warning").text("Вы уверены что хотите выйти?");
		 $(".popup.popup-agreement").fadeIn();
		 $(".popup-agreement .popup-yes").addClass("go-exit");
		 $(".popup-agreement .popup-no").addClass("go-exit");
	 });
	 $( document ).on( "click", ".popup-no.go-exit", function() {
		 $(".popup.popup-agreement").fadeOut();
	 });
	 $( document ).on( "click", ".popup-yes.go-exit", function() {
		 $( ".popup-no.go-exit" ).remove();
		 $(".popup-yes.go-exit").text("Загрузка...");
		 $(".popup-yes.go-exit").css("width","100%");
		 $(".popup-yes.go-exit").attr("disabled","disabled");
		 $.ajax({
			 type:"POST",
			 url:'/ajax/form.exit.php',
			 data:{
				 EXIT: 'Y',
			 },
			 success: function(response){
				deleteCookie("logined");
				 location.replace("/"); 
			 }
		 });
	 });
	 //забыли пароль
 
	  $( document ).on( "click", ".misspassword button", function() {
		  $(".misspassword input").removeClass("error");
		  $(".misspassword .g-recaptcha").removeClass("error");
		  var oldtxt=$(".misspassword button").text();
		  $(".misspassword button").text("Загрузка...");
		 var form=$(".misspassword form");
		 $.ajax({
			 type:"POST",
			 url:"/ajax/form.forgot.php",
			 data:form.serialize(),
			 recaptcha:grecaptcha.getResponse(),
			 success: function(response){
				 console.log(response);
				 grecaptcha.reset();
				 $(".misspassword button").text(oldtxt);
				 if(response.indexOf("login-error")!=-1){
					 $(".misspassword input").addClass("error");
				 }
				 if(response.indexOf("recaptcha-error")!=-1){
					 $(".misspassword .g-recaptcha").addClass("error");
				 }
				 if(response.indexOf("login-error")==-1&&response.indexOf("recaptcha-error")==-1){
					 $(".misspassword input").addClass("success");
					 $(".misspassword button").text("Перейти на "+response);
					 $(".misspassword input").val("");
					 $(".misspassword button").attr("onclick","location.replace('https://"+response+"')");
				 }
				 
			 }
		 });
		 return false;
	 });
	 //сохранение в лк
	 $( document ).on( "click", ".profilecontent .saveall", function() {
		changeuserfield('NAME', 'personal-name');
		changeuserfield('EMAIL', 'personal-email');
		checknewpass();
	});
	//подписка в лк
	$( document ).on( "click", ".subscribes .truecheckbox", function() {
		if(!$("#checkinput").prop('checked')){
			$.ajax({
				url: '/ajax/form.profile.subscribe.php',
				data: {
					NAME:"SUB",
					VALUE: "Y",
				},
				success: function(ans){
					console.log(ans);
				}
			});
		}
		else{
			$.ajax({
				url: '/ajax/form.profile.subscribe.php',
				data: {
					NAME:"SUB",
					VALUE: "",
				},
				success: function(ans){
					console.log(ans);
				}
			});
		}
	});
	//выход из лк
	$( document ).on( "click", ".exit button", function() {
		$(this).text("Загрузка...");
		$.ajax({
			type:"POST",
			url:'/ajax/form.exit.php',
			data:{
				EXIT: 'Y',
            },
			success: function(response){
				deleteCookie("logined");
				location.replace("/"); 
			}
		});
	});
});

//изменение значения обычного поля
function changeuserfield(name,id){
	$(".profilecontent #"+id).removeClass("error");
	$.ajax({
		url: '/ajax/form.profile.php',
		data: {
			NAME:name,
			VALUE: $(".profilecontent #"+id).val(),
		},
		success: function(ans){
			//console.log(ans);
			if(ans=="error"){
				$(".profilecontent #"+id).addClass("error");
			}
			else{
				$(".profilecontent #"+id).addClass("success");
				setTimeout(function() {
					$(".profilecontent #"+id).removeClass("success");
				}, 2000);
			}
		}
	});
}
//проверка пароля
function checkpass(){
	$.ajax({
		url: '/ajax/form.profile.php',
		data: {
			NAME:"password",
			VALUE: $(".active_password").val(),
		},
		success: function(ans){
			$(".active_password").removeClass("error");
			$(".active_password").removeClass("success");
			if(ans=="1"){
				$(".active_password").addClass("success");
				$(".active_password").attr("disabled","disabled");
				$('.new_pass1').trigger( "focus" );
			}
			else{
				$(".active_password").addClass("error");
			}
		}
	});
}
//проверка нового пароля
function checknewpass(){
	$.ajax({
		url: '/ajax/form.profile.php',
		data: {
			NAME:"password",
			VALUE1: $(".new_pass1").val(),
			VALUE2: $(".new_pass2").val(),
			VALUE3:$(".active_password").val()
		},
		success: function(ans){
			$(".new_pass1").removeClass("success");
			$(".new_pass2").removeClass("success");
			$(".new_pass1").removeClass("error");
			$(".new_pass2").removeClass("error");
			if(ans=="1"){
				$(".new_pass1").addClass("success");
				$(".new_pass2").addClass("success");
			}
			else{
				$(".new_pass1").addClass("error");
				$(".new_pass2").addClass("error");
			}
			//console.log(ans);
		}
	});
}
// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
	  "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : "";
  }
  //задавать свои куки
  function setCookie(name, value, options) {
	options = options || {};
  
	var expires = options.expires;
  
	if (typeof expires == "number" && expires) {
	  var d = new Date();
	  d.setTime(d.getTime() + expires * 1000);
	  expires = options.expires = d;
	}
	if (expires && expires.toUTCString) {
	  options.expires = expires.toUTCString();
	}
  
	value = encodeURIComponent(value);
  
	var updatedCookie = name + "=" + value;
  
	for (var propName in options) {
	  updatedCookie += "; " + propName;
	  var propValue = options[propName];
	  if (propValue !== true) {
		updatedCookie += "=" + propValue;
	  }
	}
  
	document.cookie = updatedCookie;
  }
  //удаление кук
  function deleteCookie(name) {
	setCookie(name, "", {
	  expires: -1,
	  path: "/",
	})
  }

  function show_text(e) {
  	$(e).after($(e).data('text'));
  	$(e).remove();
  	return false;
  }
