jQuery(document).ready(function($){

	var hamburgerButton = $('.navigation__mobile');
	var mobileMenu = $('.navigation__list');

	//$.fn.snow({ minSize: 15, maxSize: 50, newOn: 1000, flakeColor: '#add8e6' });

	hamburgerButton.on('click', function() {
	  $(this).toggleClass('open');
	  mobileMenu.toggleClass('navigation__list-opened');
	});

    $( document ).on( "click", ".cookie .closecookie", function() {
        var d = new Date();
        d.setDate(d.getDate() + 7);
        setCookie("COOKIE_ALERT","1",{expires: d,path: "/"});
		});
		
		$('.cookie.subscribe .subscribe__panel.mainpage__shadowpanel').delay(2000).animate({
			bottom: 0
		}, 500);

		$('.cookie .closecookie').click(function(){
			$('.cookie.subscribe .subscribe__panel.mainpage__shadowpanel').animate({
				bottom: '-100px'
			}, 500);
		});

	$('.closecontent').click(function(){
		$(this).parent().parent().fadeOut('normal');
		$('body, html').removeClass('bodyoverflow');
	});

	$('.popuppass .closecontent').click(function(){
		$('.authorization').fadeIn('normal').css('display', 'flex');
	});

	$('.entrypopup').click(function(event){
		event.preventDefault();
		$('.authorization').fadeIn('normal').css('display', 'flex');
		$('body').addClass('bodyoverflow');
	});

	$('.regpopup').click(function(e){
		e.preventDefault();
		$('body').addClass('bodyoverflow');
		$('.registr').fadeIn('normal').css('display', 'flex');
	});

	$('.entrynow').click(function(e){
		e.preventDefault();
		$(this).parent().parent().fadeOut('normal');
		$('.authorization').fadeIn('normal').css('display', 'flex');
	});

	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '.name',
		fixedContentPos: false
	});

	$(document).mouseup(function (e){
		var div = $(".popup"); 
		if (!div.is(e.target) 
		    && div.has(e.target).length === 0 && div.is(':visible')) { 
			div.parent().fadeOut('normal'); 
		//$('.authorization').fadeIn('normal').css('display', 'flex');
		$('body, html').removeClass('bodyoverflow');
		}
	});

	$(document).mouseup(function (e){
		var pass = $(".popuppass"); 
		if (!pass.is(e.target) 
		    && pass.has(e.target).length === 0 && pass.is(':visible')) { 
			pass.parent().fadeOut('normal'); 
		$('.authorization').fadeIn('normal').css('display', 'flex');
		$('body, html').removeClass('bodyoverflow');
		}
	});
/*

	$(document).mouseup(function (e){
		var passw = $(".popup.passw"); 
		if (!passw.is(e.target) 
		    && passw.has(e.target).length === 0 && passw.is(':visible')) { 
			passw.parent().fadeOut('normal'); 
		$('.authorization').fadeIn('normal').css('display', 'flex');
		//$('body, html').removeClass('bodyoverflow');
		}
	});

	/*$(document).mouseup(function (e){
		var autho = $(".authorization .popup"); 
		if (!autho.is(e.target) 
		    && autho.has(e.target).length === 0 && autho.is(':visible')) { 
			autho.parent().fadeOut('normal'); 
		//$('body, html').removeClass('bodyoverflow');
		}
	});

	$(document).mouseup(function (e){
		var registr = $(".registr .popup"); 
		if (!registr.is(e.target) 
		    && registr.has(e.target).length === 0 && registr.is(':visible')) { 
			registr.parent().fadeOut('normal'); 
		//$('body, html').removeClass('bodyoverflow');
		}
	});*/

	$('.sublink').click(function(e){
		e.preventDefault();
		$(this).addClass('activeradio');
		$(this).prev().removeClass('activeradio');
		$('.profilecontent').hide();
		$('.subscribes').show();
		$('.editprofile').hide();
	});

	$('.profilelink').click(function(e){
		e.preventDefault();
		$(this).addClass('activeradio');
		$(this).next().removeClass('activeradio');
		$('.profilecontent').show();
		$('.subscribes').hide();
		$('.editprofile').show();
		if($('.row.profilecontent').hasClass('showcontent')) {
			$('.row.profilecontent').show();
		} else {
			$('.row.profilecontent').hide();
		}
		
	});

	$('.editprofile').click(function(e){
		e.preventDefault();
		$('.inputcontent input').prop('disabled', false);
		$('.row.profilecontent').addClass('showcontent').fadeIn('normal');
	});

	$('.saveall button').click(function(){
		$('.inputcontent input').prop('disabled', true);
		$(this).parent().parent().removeClass('showcontent').fadeOut('slow');
		//alert($('.passwordinput').val());
	});

	var isMacLike = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i)?true:false;
	var isIOS = navigator.platform.match(/(iPhone|iPod|iPad)/i)?true:false;

	// if (isMacLike || isIOS) {
	// 	$('.visible-mac').css('display', 'block');
	// }

	// var headerPlayButton = $('.header__playButton');
	// headerPlayButton.on('click', function() {

	// })

	
	var catSlider = $('.cat-slider').lightSlider({
		item:4,
		loop:true,
		pager: false,
		controls: false,
		responsive : [
			{
				breakpoint:992,
				settings: {
						item:2
					}
			},
			{
				breakpoint:765,
				settings: {
						item:1
					}
			},
		],
		onSliderLoad: function() {
		$('.cat-slider').removeClass('cS-hidden');
	}
	});
	$('.cat-slider-nav .slide-left').click(function(){
		catSlider.goToPrevSlide();
	});
	$('.cat-slider-nav .slide-right').click(function(){
		catSlider.goToNextSlide();
	});

	$('.open-form').click(function(){
		var form = $(this).attr('formname');
		$('.' + form).addClass('active');
		$('body').addClass('no-scroll');
	});
	$('.bg-close-modal, .modal-close').click(function(){
		$(this).parents('.modal').removeClass('active');
		$('body').removeClass('no-scroll');
	});
	$('.full-width-search .navigation__icon-search').click(function(){
		$('.navigation__search').parent().submit();
	});

	$('.theme').change(function() {
		if($(this).val().length > 0){
			$(this).addClass('choosed');
		}else{
			$(this).removeClass('choosed');
		}
	});
	$('.close-itunes-sub').click(function(){
		$(this).parent().slideUp();
		setCookie("itunes-sub","yes",{expires: d,path: "/"});
	});


	// $('.card-body-click').click(function(e){
	// 	$('.uniq-card-full').show();
	// });
	// $('.uniq-toggle').click(function(e) {
	// 	e.preventDefault();
	// 	$('.uniq-card-full').toggleClass('is-active');
	// 	$('.uniq-card-full').hide();
	// });
	// $('.card-body-click').click(function(e){
	// 	$('.uniq-card-full').toggleClass('is-active');
	// });
	// $('.uniq-toggle').click(function(e) {
	// 	e.preventDefault();
	// 	$('.uniq-card-full').toggleClass('is-active');
	// 		});
	// $(' .card-body-click, .uniq-card-full').click(function(e){
	// 	$('.uniq-card-full').toggleClass('is-active');
	// });
	// $(' .unic-cards-format').click(function(e){
	// 		$('.uniq-card-full').toggleClass('is-active');
	// 		return false;
	// 	});
	

	function setCookie(name, value, options) {
		options = options || {};
	  
		var expires = options.expires;
	  
		if (typeof expires == "number" && expires) {
		  var d = new Date();
		  d.setTime(d.getTime() + expires * 1000);
		  expires = options.expires = d;
		}
		if (expires && expires.toUTCString) {
		  options.expires = expires.toUTCString();
		}
	  
		value = encodeURIComponent(value);
	  
		var updatedCookie = name + "=" + value;
	  
		for (var propName in options) {
		  updatedCookie += "; " + propName;
		  var propValue = options[propName];
		  if (propValue !== true) {
			updatedCookie += "=" + propValue;
		  }
		}
	  
		document.cookie = updatedCookie;
	  }
	  var d = new Date();
	d.setDate(d.getDate() + 7);
	 

});