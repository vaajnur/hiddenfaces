<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Меняем элементы на секции");

// $USER->Authorize(1);

// die();


function pr($ob){
	echo "<pre>";
	print_r($ob);
	echo "</pre>";
}

$categors = [
	'Arts' => ['Books','Design','Fashion & Beauty','Food','Performing Arts','Visual Arts'],
	'Business' => ['Careers','Entrepreneurship','Investing','Management','Marketing','Non-Profit'],
	'Comedy' => ['Comedy Interviews','Improv','Stand-Up'],
	'Education' => ['Courses','How To','Language Learning','Self-Improvement'],
	'Fiction' => ['Comedy Fiction','Drama','Science Fiction'],
	'Government' => [],
	'History' => [],
	'Health & Fitness' => ['Alternative Health','Fitness','Medicine','Mental Health','Nutrition','Sexuality'],
	'Kids & Family' => ['Education for Kids','Parenting','Pets & Animals','Stories for Kids'],
	'Leisure' => ['Animation & Manga','Automotive','Aviation','Crafts','Games','Hobbies','Home & Garden','Video Games'],
	'Music' => ['Music Commentary','Music History','Music Interviews'],
	'News' => ['Business News','Daily News','Entertainment News','News Commentary','Politics','Sports News','Tech News'],
	'Religion & Spirituality' => ['Buddhism','Christianity','Hinduism','Islam','Judaism','Religion','Spirituality'],
	'Science' => ['Astronomy','Chemistry','Earth Sciences','Life Sciences','Mathematics','Natural Sciences','Nature','Physics','Social Sciences'],
	'Society & Culture' => ['Documentary','Personal Journals','Philosophy','Places & Travel','Relationships'],
	'Sports' => ['Baseball','Basketball','Cricket','Fantasy Sports','Football','Golf','Hockey','Rugby','Running','Soccer','Swimming','Tennis','Volleyball','Wilderness','Wrestling'],
	'Technology' => [],
	'True Crime' => [],
	'TV & Film' => ['After Shows','Film History','Film Interviews','Film Reviews','TV Reviews'],
];

function search_parent_categor($cat){
	global $categors;
	// var_dump(count($categors));
	foreach ($categors as $key => $value) {
		if($cat == $key){
			if(empty($value))
				return [1, $cat];
			return false;
		}
		foreach ($value as $k => $v) {
			if($v == $cat)
				return [2, $key];
		}
	}
	return false;
}

 		$res1 = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_ID" => '24',
        ));
 		$categors_vals = [];
        while($ob = $res1->GetNext()){
        	$categors_vals[$ob['ID']] = $ob['VALUE'];
        }

$arFiltersec = array('IBLOCK_ID' => 3);
$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFiltersec,array(),array("ID","NAME","PICTURE","DESCRIPTION","UF_*",'SECTION_PAGE_URL'));
	$goods = '';
while ($arSection = $rsSections->Fetch())
{
	global $goods;
	foreach ($arSection['UF_CATEGORS'] as $key => $cat_id) {
		$parent = search_parent_categor($categors_vals[$cat_id]);
		if( $parent != false){
			if($parent[0] == 1){
				$goods.= '<itunes:category text="'.$parent[1].'" />';
			}elseif($parent[0] == 2){
				$goods.= '<itunes:category text="'.$parent[1].'">
					<itunes:category text="'.$categors_vals[$cat_id].'" />
				</itunes:category>';

			}
		}
	}
}

echo "string";
echo($goods);
echo "string";

// pr($categors);



// $IBLOCK_ID=5;
// $sl = array();
// $el = array();
// $ml = array();
// $IBLOCK_ID_POSTS=3;
// $pl = array();
// $PROPERTY_CODE = 'CATS';

//   $arFilter = array('IBLOCK_ID'=>$IBLOCK_ID);
//   $rsSect = CIBlockSection::GetList(array('ID'=>'ASC'),$arFilter);
//   while ($arSect = $rsSect->GetNext()) {
//     // var_dump($arSect['ID'],$arSect['IBLOCK_SECTION_ID'],$arSect['ACTIVE'],$arSect['NAME'],'<br>');
//     $sl[$arSect['IBLOCK_SECTION_ID'].$arSect['NAME']] = $arSect['ID'];
//   }
  
//   $arSelect = Array("ID", "NAME", "ACTIVE", "IBLOCK_SECTION_ID");
//   $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID);
//   $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>100), Array());
//   while($arSect = $res->GetNext()) {
//     // if (empty($arSect['IBLOCK_SECTION_ID'])) var_dump('ID: '.$arSect['ID'].' NAME: '.$arSect['NAME'],'<br>');
//     // var_dump($arSect['ID'],$arSect['IBLOCK_SECTION_ID'],$arSect['ACTIVE'],$arSect['NAME'],'<br>');
//     $el[$arSect['IBLOCK_SECTION_ID'].$arSect['NAME']] = $arSect['ID'];
//   }
  
//   $arSelect = Array("ID", "NAME", "PROPERTY_CATEGORIES");
//   $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID_POSTS,"PROPERTY_CATS"=>false);
//   $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
//   while($arSect = $res->GetNext()) {
//     // if (empty($arSect['IBLOCK_SECTION_ID'])) var_dump('ID: '.$arSect['ID'].' NAME: '.$arSect['NAME'],'<br>');
//     // var_dump('<pre>',$arSect,'</pre><br>');
//     $pl[$arSect['ID']][] = $arSect['PROPERTY_CATEGORIES_VALUE'];
//   }
  
//   foreach ($el as $k => $v) {
//     $ml[$v]=$sl[$k];
//     // var_dump($k.' _ '.$v.' - '.$sl[$k],'<br>');
//         /*
//         $arSelect_item = Array("ID","NAME");
//         $arFilter_item = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'PROPERTY_CATEGORIES'  => array($v));
//         $res_item = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC","SORT"=>"ASC"), $arFilter_item, false, Array("nPageSize"=>1000), $arSelect_item);
//         if($ob_item = $res_item->GetNext()){ 
//           var_dump(' *** '.$ob_item['NAME'],'<br>');
//         }
//         */
//   }
//   foreach ($pl as $k => $v) {
//     // if ($k!=719) continue;
//     $PROPERTY_VALUE = array();
//     foreach ($v as $oldId) $PROPERTY_VALUE[] = $ml[$oldId];
//     var_dump('<pre>',$k,$PROPERTY_VALUE,'</pre><br>');
//     // CIBlockElement::SetPropertyValues($k, $IBLOCK_ID_POSTS, $PROPERTY_VALUE, $PROPERTY_CODE);
//   }
  // var_dump('<pre>',$pl,'</pre><br>');
  // CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
  

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>