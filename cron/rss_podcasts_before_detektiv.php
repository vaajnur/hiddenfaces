#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');

//раз в день
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
//активные и подтвержденные адреса, подписанные на рубрики
$subscr = CSubscription::GetList(
    array("ID"=>"ASC"),
    array("RUBRIC"=>array(1),  "ACTIVE"=>"Y" )
);

function cut($string, $length){
	$string = mb_substr($string, 0, $length,'UTF-8'); // обрезаем и работаем со всеми кодировками и указываем исходную кодировку
	$position = mb_strrpos($string, ' ', 'UTF-8'); // определение позиции последнего пробела. Именно по нему и разделяем слова
	$string = mb_substr($string, 0, $position, 'UTF-8'); // Обрезаем переменную по позиции
	return $string."...";
}

$count=10000;
$goods=array();
$count_goods=0;
$goods="";
$goods='';
$arFiltersec = array('IBLOCK_ID' => 3);
$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFiltersec,array(),array("ID","NAME","PICTURE","DESCRIPTION","UF_*",'SECTION_PAGE_URL'));
while ($arSection = $rsSections->Fetch())
{
	$goods='';
	$arFilter2 = Array("IBLOCK_ID"=>7,"ID"=>$arSection['UF_AUTHOR'], "ACTIVE"=>"Y");			   
	$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize"=>3), $arSelect2);
	while($ob2 = $res2->GetNextElement()){
		$arFields2 = $ob2->GetFields();
		$author=$arFields2['NAME'];
	}
	$arSelect = Array();
	$arFilter = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$arSection['ID'],"ACTIVE"=>"Y","PROPERTY_MAIN_PODCAST"=>false,'<DATE_ACTIVE_FROM'  => date('d.m.Y H:i:s',strtotime("+3 hours")));
	$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
	$file = CFile::ResizeImageGet($arSection['UF_ITUNES_COVER'], array('width'=>3000, 'height'=>3000), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	$cover=$file['src'];
	
	if (strlen(strip_tags($arSection['DESCRIPTION'])) > 240) $subtitle = html_entity_decode(cut(strip_tags($arSection['DESCRIPTION']), 240));
	else $subtitle = html_entity_decode(strip_tags($arSection['DESCRIPTION']));
	
		$goods.='<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0"><channel>
			<title>'.$arSection['NAME'].'</title>
			<link>https://hiddenfaces.ru/peredachi/'.$arSection['CODE'].'/</link>
			<description>'.$subtitle.'</description>
			<language>ru</language>
			<copyright>'.date("Y").' Скрытые лица</copyright>
			<image>
				<url>https://hiddenfaces.ru'.$cover.'</url>
				<title>'.$arSection['NAME'].'</title>
				<link>https://hiddenfaces.ru/peredachi/'.$arSection['CODE'].'/</link>
			</image>
			<itunes:author>Скрытые лица</itunes:author>
			<itunes:owner>
				<itunes:name>Мария Павлович</itunes:name>
				<itunes:email>info@hiddenfaces.ru</itunes:email>
			</itunes:owner>
			<itunes:image href="https://hiddenfaces.ru'.$cover.'"/>
			<itunes:subtitle>'.$subtitle.'</itunes:subtitle>
			<itunes:summary>'.$subtitle.'</itunes:summary>
			<itunes:type>episodic</itunes:type>
			<itunes:explicit>clean</itunes:explicit>';
	
	if($arSection['CODE']=="skrytye-litsa-besedy"){
		$goods.='<itunes:category text="Comedy" />';
	}
	if($arSection['CODE']=="skrytye-litsa-o-kino"){
		$goods.='<itunes:category text="TV  &amp; Film" />
				<itunes:category text="Comedy" />
			';
	}
	if($arSection['CODE']=="wrapupshow-s-buboy"){
		$goods.='<itunes:category text="Music" />
			';
	}
	if($arSection['CODE']=="skrytye-litsa-knizhnyy-klub"){
		$goods.='<itunes:category text="Arts">
					<itunes:category text="Literature" />
				</itunes:category>
				<itunes:category text="Comedy" />
			<itunes:category text="Society &amp; Culture">
				<itunes:category text="Personal Journals" />
			</itunes:category>
			<itunes:category text="Society &amp; Culture">
				<itunes:category text="Philosophy" />
			</itunes:category>
			<itunes:category text="Science &amp; Medicine">
				<itunes:category text="Social Sciences" />
			</itunes:category>
			<itunes:category text="Arts">
				<itunes:category text="Fashion &amp; Beauty" />
			</itunes:category>
			';
	}
	if($arSection['CODE']=="prozhektor-vospriyatiya"){
		$goods.='<itunes:category text="Society &amp; Culture">
				<itunes:category text="Personal Journals" />
			</itunes:category>
			<itunes:category text="Science &amp; Medicine">
				<itunes:category text="Social Sciences" />
			</itunes:category>';
	}
	if($arSection['CODE']=="rasshifrovka"){
		$goods.='<itunes:category text="Comedy" />
		<itunes:category text="Music" />';
	}
	if($arSection['CODE']=="eyler-v-rossii"){
		$goods.='<itunes:category text="Society &amp; Culture">
				<itunes:category text="Places &amp; Travel" />
			</itunes:category>
			<itunes:category text="Comedy" />
		<itunes:category text="Society &amp; Culture">
				<itunes:category text="Personal Journals" />
			</itunes:category>
			';
	}
	if($arSection['CODE']=="osnovnoe-vremya-s-buboy"){
		$goods.='<itunes:category text="Sports &amp; Recreation" />
		<itunes:category text="Music" />';
	}
	if($arSection['CODE']=="detektiv"){
		$goods.='<itunes:category text="News &amp; Politics" />';
	}
	while($ob = $res->GetNextElement())
	{
		$count_goods++;
		$arFields = $ob->GetFields();
		$arProps=$ob->GetProperties();
		$arSelect2 = Array("ID","NAME");
		$arFilter2 = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$arFields["IBLOCK_SECTION_ID"], "ACTIVE"=>"Y"); 
		$res2 = CIBlockElement::GetList(Array("ACTIVE_FROM" => "ASC"), $arFilter2, false, Array("nPageSize"=>100), $arSelect2);
		$index=0;
		$section_index=0;
		while($ob2 = $res2->GetNextElement()){ 
			$index++;
			$arFields2 = $ob2->GetFields();
			if($arFields2['ID']==$arFields["ID"])
				$section_index=$index;
		}
		$author="";
		$rsUser = CUser::GetByID($arResult["CREATED_BY"]); 
		$arUser = $rsUser->Fetch(); 
		$arSelect2 = Array("ID", "NAME", "DETAIL_PAGE_URL");
		if(count($arProps['AUTHORS']['VALUE'])==0)
			$arFilter2 = Array("IBLOCK_ID"=>7,"ID"=>$arUser['UF_AUTHOR_INFO'], "ACTIVE"=>"Y");
		else
			$arFilter2 = Array("IBLOCK_ID"=>7,"ID"=>$arProps['AUTHORS']['VALUE'], "ACTIVE"=>"Y");			   
		$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize"=>3), $arSelect2);
		while($ob2 = $res2->GetNextElement()){
			$arFields2 = $ob2->GetFields();
			$author=$arFields2['NAME'];
		}
		if($arProps['ITUNES_COVER']['VALUE']>0){
			$cover_item=CFile::GetPath($arProps['ITUNES_COVER']['VALUE']);
		}
		else{
			$cover_item=$cover;
		}
		if (!empty($arProps['ITUNES_NAME']['VALUE'])) {
			$name_podcast = $arProps['ITUNES_NAME']['VALUE'];
		}
		else {
			$name_podcast = $arFields['NAME'];
		}
		$date=strlen($arFields['ACTIVE_FROM'])>0?date("D, d M Y H:i:s",strtotime($arFields['ACTIVE_FROM'])):date("D, d M Y H:i:s",strtotime($arFields['DATE_CREATE']));
		//$date=explode(" ",$date)[0];
		$a=explode(":",$arProps['TIME_LENGTH']['VALUE']);
		$time_seconds=$a[0]*60+$a[1];
		if(count($a)==3)
			$time_seconds= $a[0] * 60 * 60 +$a[1] * 60 + $a[2];
			$arFields['PREVIEW_TEXT']=html_entity_decode($arFields['PREVIEW_TEXT']);
			$arFields['DETAIL_TEXT']=html_entity_decode($arFields['DETAIL_TEXT']);
		if (strlen(strip_tags($arFields['PREVIEW_TEXT'])) > 240) $subtitle_goods = cut(strip_tags($arFields['PREVIEW_TEXT']), 240);
		else $subtitle_goods = strip_tags($arFields['PREVIEW_TEXT']);		
		if(strlen($arProps['EIGHTEEN']['VALUE'])>0){
			$explicit="explicit";
		}
		else{
			$explicit="clean";
		}
		$goods.='<item>
				<title>#'.$section_index.' '.$name_podcast.'</title>
				<link>https://hiddenfaces.ru'.$arFields['DETAIL_PAGE_URL'].'</link>
				<description>'.strip_tags($arFields['DETAIL_TEXT']).'</description>
				<guid>https://hiddenfaces.ru/podcast/download'.CFile::GetPath($arProps['PODCAST']['VALUE']).'</guid>
				<pubDate>'.$date.' +0300</pubDate>
				<enclosure length="0" type="audio/mpeg" url="https://hiddenfaces.ru/podcast/download'.CFile::GetPath($arProps['PODCAST']['VALUE']).'"/>
				<itunes:author>Скрытые лица</itunes:author>
				<itunes:subtitle>'.$subtitle_goods.'</itunes:subtitle>
				<itunes:summary>'.strip_tags($arFields['DETAIL_TEXT']).'</itunes:summary>
				<itunes:duration>'.$time_seconds.'</itunes:duration>
				<itunes:explicit>'.$explicit.'</itunes:explicit>
				<itunes:image href="https://hiddenfaces.ru'.$cover_item.'"/>
				<itunes:episodeType>full</itunes:episodeType>
			</item>';
	}
	$goods.='</channel></rss>';
	// Открыть текстовый файл
	$f = fopen($_SERVER['DOCUMENT_ROOT']."/rss/hiddenfaces-".$arSection['CODE'].".rss", "w");

	// Записать строку текста
	fwrite($f, $goods); 

	// Закрыть текстовый файл
	fclose($f); 	
}


?>