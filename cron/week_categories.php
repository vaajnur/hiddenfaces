<?php
    $root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
    define("NO_KEEP_STATISTIC", true);
    define("NOT_CHECK_PERMISSIONS", true);
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    
    
    CModule::IncludeModule("iblock");   
    CModule::IncludeModule("catalog"); 
    CModule::IncludeModule("sale");
    
    $sections = array();
    $categories = array();
    // если $ID не задан или это не число, тогда 
    // $ID будет =0, выбираем корневые разделы
    $ID = IntVal(0);
    // выберем папки из информационного блока $BID и раздела $ID
    $items = GetIBlockSectionList(5, $ID, Array("sort"=>"asc"), 10);
    while($arItem = $items->GetNext())
    {
        $arSelect = Array("ID", "NAME", "PROPERTY_ICON","DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID"=>5,"IBLOCK_SECTION_ID"=>$arItem['ID'], "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arSelect_item = Array("ID","NAME");
            $arFilter_item = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y",'PROPERTY_CATEGORIES'  => array($arFields['ID']));
            $res_item = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC","SORT"=>"ASC"), $arFilter_item, false, Array("nPageSize"=>1), $arSelect_item);
            if($ob_item = $res_item->GetNextElement()){ 
                $sections[$arItem['ID']] = $arItem['ID'];
            }
        }
    }
    $rand_keys = array_rand($sections, 2);
    $fd = fopen("week.json", 'w') or die("не удалось создать файл");
    $str = json_encode($rand_keys);
    fwrite($fd, $str);
    fclose($fd);
?>