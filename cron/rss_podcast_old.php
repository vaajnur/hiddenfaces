#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');

//раз в день
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
//активные и подтвержденные адреса, подписанные на рубрики
$subscr = CSubscription::GetList(
    array("ID"=>"ASC"),
    array("RUBRIC"=>array(1),  "ACTIVE"=>"Y" )
);
$count=10000;
$goods=array();
$count_goods=0;
$goods="";
$goods='';
$arFiltersec = array('IBLOCK_ID' => 3);
$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFiltersec,array(),array("ID","NAME","PICTURE","DESCRIPTION","UF_*",'SECTION_PAGE_URL'));
while ($arSection = $rsSections->Fetch())
{
	$goods='';
	$arFilter2 = Array("IBLOCK_ID"=>7,"ID"=>$arSection['UF_AUTHOR'], "ACTIVE"=>"Y");			   
	$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize"=>3), $arSelect2);
	while($ob2 = $res2->GetNextElement()){
		$arFields2 = $ob2->GetFields();
		$author=$arFields2['NAME'];
	}
	$arSelect = Array();
	$arFilter = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$arSection['ID'],"ACTIVE"=>"Y","PROPERTY_MAIN_PODCAST"=>false);
	$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
	$file = CFile::ResizeImageGet($arSection['UF_ITUNES_COVER'], array('width'=>3000, 'height'=>3000), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	$cover=$file['src'];
		$goods.='<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0" xmlns:atom="http://www.w3.org/2005/Atom"><channel>
			<title>'.$arSection['NAME'].'</title>
			<link>http://hiddenfaces.ru/peredachi/'.$arSection['CODE'].'/</link>
			<language>ru-ru</language>
			<copyright>&#xA9; '.date("Y").' Скрытые лица</copyright>
			<itunes:subtitle>'.strip_tags($arSection['DESCRIPTION']).'</itunes:subtitle>
			<itunes:author>Скрытые лица</itunes:author>
			<itunes:summary>'.strip_tags($arSection['DESCRIPTION']).'</itunes:summary>
			<description>'.strip_tags($arSection['DESCRIPTION']).'</description>
			<itunes:type>episodic</itunes:type>
			<itunes:owner>
				<itunes:name>Мария Павлович</itunes:name>
				<itunes:email>info@hiddenfaces.ru</itunes:email>
			</itunes:owner>
			<itunes:image href="http://hiddenfaces.ru'.$cover.'"/>
			<lastBuildDate>'.date("D, d M Y H:i:s").' +0300</lastBuildDate>
			<itunes:explicit>no</itunes:explicit>';
	
	if($arSection['CODE']=="skrytye-litsa-besedy"){
		$goods.='<itunes:category text="Comedy" />
			<itunes:category text="Society &amp; Culture">
				<itunes:category text="Personal Journals" />
			</itunes:category>
			<itunes:category text="Society &amp; Culture">
				<itunes:category text="Philosophy" />
			</itunes:category>
			<itunes:category text="Arts">
				<itunes:category text="Literature" />
			</itunes:category>
			<itunes:category text="Science &amp; Medicine">
				<itunes:category text="Social Sciences" />
			</itunes:category>
			<itunes:category text="Arts">
				<itunes:category text="Fashion &amp; Beauty" />
			</itunes:category>';
	}
	if($arSection['CODE']=="skrytye-litsa-o-kino"){
		$goods.='<itunes:category text="TV  &amp; Film" />
				<itunes:category text="Comedy" />
			';
	}
	if($arSection['CODE']=="wrapupshow-s-buboy"){
		$goods.='<itunes:category text="Music" />
			';
	}
	if($arSection['CODE']=="skrytye-litsa-knizhnyy-klub"){
		$goods.='<itunes:category text="Arts">
					<itunes:category text="Literature" />
				</itunes:category>
				<itunes:category text="Comedy" />
			<itunes:category text="Society &amp; Culture">
				<itunes:category text="Personal Journals" />
			</itunes:category>
			<itunes:category text="Society &amp; Culture">
				<itunes:category text="Philosophy" />
			</itunes:category>
			<itunes:category text="Science &amp; Medicine">
				<itunes:category text="Social Sciences" />
			</itunes:category>
			<itunes:category text="Arts">
				<itunes:category text="Fashion &amp; Beauty" />
			</itunes:category>
			';
	}
	if($arSection['CODE']=="prozhektor-vospriyatiya"){
		$goods.='<itunes:category text="Society &amp; Culture">
				<itunes:category text="Personal Journals" />
			</itunes:category>
			<itunes:category text="Science &amp; Medicine">
				<itunes:category text="Social Sciences" />
			</itunes:category>';
	}
	if($arSection['CODE']=="rasshifrovka"){
		$goods.='<itunes:category text="Comedy" />
		<itunes:category text="Music" />';
	}
	if($arSection['CODE']=="eyler-v-rossii"){
		$goods.='<itunes:category text="Society &amp; Culture">
				<itunes:category text="Places &amp; Travel" />
			</itunes:category>
			<itunes:category text="Comedy" />
		<itunes:category text="Society &amp; Culture">
				<itunes:category text="Personal Journals" />
			</itunes:category>
			';
	}
	if($arSection['CODE']=="osnovnoe-vremya-s-buboy"){
		$goods.='<itunes:category text="Sports &amp; Recreation" />
		<itunes:category text="Music" />';
	}
	while($ob = $res->GetNextElement())
	{
		$count_goods++;
		$arFields = $ob->GetFields();
		$arProps=$ob->GetProperties();
		$arSelect2 = Array("ID","NAME");
		$arFilter2 = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$arFields["IBLOCK_SECTION_ID"], "ACTIVE"=>"Y"); 
		$res2 = CIBlockElement::GetList(Array("ACTIVE_FROM" => "ASC"), $arFilter2, false, Array("nPageSize"=>100), $arSelect2);
		$index=0;
		$section_index=0;
		while($ob2 = $res2->GetNextElement()){ 
			$index++;
			$arFields2 = $ob2->GetFields();
			if($arFields2['ID']==$arFields["ID"])
				$section_index=$index;
		}
		$author="";
		$rsUser = CUser::GetByID($arResult["CREATED_BY"]); 
		$arUser = $rsUser->Fetch(); 
		$arSelect2 = Array("ID", "NAME", "DETAIL_PAGE_URL");
		if(count($arProps['AUTHORS']['VALUE'])==0)
			$arFilter2 = Array("IBLOCK_ID"=>7,"ID"=>$arUser['UF_AUTHOR_INFO'], "ACTIVE"=>"Y");
		else
			$arFilter2 = Array("IBLOCK_ID"=>7,"ID"=>$arProps['AUTHORS']['VALUE'], "ACTIVE"=>"Y");			   
		$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize"=>3), $arSelect2);
		while($ob2 = $res2->GetNextElement()){
			$arFields2 = $ob2->GetFields();
			$author=$arFields2['NAME'];
		}
		if($arProps['ITUNES_COVER']['VALUE']>0){
			$cover_item=CFile::GetPath($arProps['ITUNES_COVER']['VALUE']);
		}
		else{
			$cover_item=$cover;
		}
		$date=strlen($arFields['ACTIVE_FROM'])>0?date("D, d M Y H:i:s",strtotime($arFields['ACTIVE_FROM'])):date("D, d M Y H:i:s",strtotime($arFields['DATE_CREATE']));
		//$date=explode(" ",$date)[0];
		$a=explode(":",$arProps['TIME_LENGTH']['VALUE']);
		$time_seconds=$a[0]*60+$a[1];
		if(count($a)==3)
			$time_seconds= $a[0] * 60 * 60 +$a[1] * 60 + $a[2];
		$goods.='<item>
				<itunes:episodeType>full</itunes:episodeType>
				<itunes:title>#'.$section_index.' '.$arFields['NAME'].'</itunes:title>
				<title>#'.$section_index.' '.$arFields['NAME'].'</title>
				<itunes:episode>'.$section_index.'</itunes:episode>
				<itunes:author>Скрытые лица</itunes:author>
				<itunes:subtitle>'.strip_tags($arFields['PREVIEW_TEXT']).'</itunes:subtitle>
				<itunes:summary>'.strip_tags($arFields['DETAIL_TEXT']).'
				
				
Слушайте на http://hiddenfaces.ru
				
Facebook: https://fb.com/hiddenfaces.ru
				
Instagram: http://instagram.com/podcaststudio
				
VK: https://vk.com/hiddenfacesru
				
Twitter: https://twitter.com/hiddenfacesru</itunes:summary>
				<description>'.strip_tags($arFields['DETAIL_TEXT']).'&lt;br&gt;&lt;br&gt;&lt;br&gt;Слушайте на http://hiddenfaces.ru&lt;br&gt;&lt;br&gt;Facebook: https://fb.com/hiddenfaces.ru&lt;br&gt;&lt;br&gt;Instagram: http://instagram.com/podcaststudio&lt;br&gt;&lt;br&gt;VK: https://vk.com/hiddenfacesru&lt;br&gt;&lt;br&gt;Twitter: https://twitter.com/hiddenfacesru&lt;br&gt;&lt;br&gt;YouTube: https://www.youtube.com/channel/UCae6jrb8xEL7VLd89DG5iPg</description>
				<itunes:image href="http://hiddenfaces.ru'.$cover_item.'"/>
				<content:encoded><![CDATA['.$arFields['PREVIEW_TEXT'].'<br><br><br>Слушайте на http://hiddenfaces.ru<br><br>Facebook: https://fb.com/hiddenfaces.ru<br><br>Instagram: http://instagram.com/podcaststudio<br><br>VK: https://vk.com/hiddenfacesru<br><br>Twitter: https://twitter.com/hiddenfacesru<br><br>YouTube: https://www.youtube.com/channel/UCae6jrb8xEL7VLd89DG5iPg]]></content:encoded>
				<enclosure length="'.CFile::GetFileArray($arProps['PODCAST']['VALUE'])['FILE_SIZE'].'" type="audio/mpeg" url="http://hiddenfaces.ru'.CFile::GetPath($arProps['PODCAST']['VALUE']).'"/>
				<guid>http://hiddenfaces.ru'.CFile::GetPath($arProps['PODCAST']['VALUE']).'</guid>
				<link>http://hiddenfaces.ru'.$arFields['DETAIL_PAGE_URL'].'</link>
				<pubDate>'.$date.' GMT</pubDate>
				<itunes:duration>'.$arProps['TIME_LENGTH']['VALUE'].'</itunes:duration>
				<itunes:explicit>no</itunes:explicit>
			</item>';
	}
	$goods.='</channel></rss>';
	// Открыть текстовый файл
	$f = fopen($_SERVER['DOCUMENT_ROOT']."/rss/hiddenfaces-".$arSection['CODE'].".rss", "w");

	// Записать строку текста
	fwrite($f, $goods); 

	// Закрыть текстовый файл
	fclose($f); 	
}


?>