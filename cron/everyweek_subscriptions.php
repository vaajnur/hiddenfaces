#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');

//раз в неделю
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
//активные и подтвержденные адреса, подписанные на рубрики
$subscr = CSubscription::GetList(
    array("ID"=>"ASC"),
    array("RUBRIC"=>array(2),  "ACTIVE"=>"Y" )
);
$count=10;
$goods=array();
$count_goods=0;
$goods="";
$arSelect = Array();
$arFilter = Array("IBLOCK_ID"=>3,"ACTIVE"=>"Y", /*"PROPERTY_MAIN_PODCAST"=>false,*/ '<DATE_ACTIVE_FROM'  => date('d.m.Y H:i:s'),'>DATE_ACTIVE_FROM'  => date('d.m.Y H:i:s',strtotime("-7 day")));
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
if($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $announce=$arFields['PREVIEW_TEXT'];
}
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
$goods='<span class="preheader" style="color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden">'.$announce.'</span>
<table class="body" style="Margin:0;background:#f3f3f3;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%">
    <tr style="padding:0;text-align:left;vertical-align:top">
        <td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
            <center data-parsed="" style="min-width:580px;width:100%">
                <table align="center" class="container float-center" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:580px">
                    <tbody>
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                            <table class="row header" style="background-color:#707fa6;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                    <tbody>
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <th class="small-12 large-2 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:16px;padding-right:8px;padding-top:0;text-align:left;width:80.67px">
                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                            <img src="http://hiddenfaces.ru/local/templates/hiddenfaces/img/footer/logo_footer.png" alt="" style="-ms-interpolation-mode:bicubic;clear:both;display:block;margin-top:10px;max-width:100%;outline:0;text-decoration:none;width:auto">
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="small-12 large-2 columns" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:8px;padding-right:8px;padding-top:0;text-align:left;width:80.67px">
                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                            <a href="http://hiddenfaces.ru/peredachi/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:50px 0;text-align:center;text-decoration:none">Передачи</a>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="small-12 large-2 columns" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:8px;padding-right:8px;padding-top:0;text-align:left;width:80.67px">
                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                            <a href="http://hiddenfaces.ru/gosti/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:50px 0;text-align:center;text-decoration:none">Гости</a>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="small-12 large-2 columns" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:8px;padding-right:8px;padding-top:0;text-align:left;width:80.67px">
                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                            <a href="http://hiddenfaces.ru/o-proekte/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:50px 0;text-align:center;text-decoration:none">О Проекте</a>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="small-12 large-2 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:8px;padding-right:16px;padding-top:0;text-align:left;width:80.67px">
                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                            <a href="http://hiddenfaces.ru/kontakty/" style="Margin:0;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:50px 0;text-align:center;text-decoration:none">Контакты</a>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
<table class="row content" style="border-collapse:collapse;border-spacing:0;display:table;padding:20px 0!important;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
		<tr><td><h2 style="margin-left: 20px">Выпуски за последнюю неделю</p></td></tr>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <!-- <columns large="4">
                  
                  <img src="assets/img/item-2.jpg" alt="">
                </columns> -->
            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px">
                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">';
while($ob = $res->GetNextElement())
{
    $count_goods++;
    $arFields = $ob->GetFields();
    $arProps=$ob->GetProperties();
	$arSelect2 = Array("ID","NAME");
	$arFilter2 = Array("IBLOCK_ID"=>3,"IBLOCK_SECTION_ID"=>$arFields["IBLOCK_SECTION_ID"], "ACTIVE"=>"Y"); 
	$res2 = CIBlockElement::GetList(Array("ACTIVE_FROM" => "ASC"), $arFilter2, false, Array("nPageSize"=>1000), $arSelect2);
	$index=0;
	$section_index=0;
	while($ob2 = $res2->GetNextElement()){ 
		$index++;
		$arFields2 = $ob2->GetFields();
		if($arFields2['ID']==$arFields["ID"])
			$section_index=$index;
    }
    if($arFields["IBLOCK_SECTION_ID"]==29){
        $title_podcast="Детектив - ".$arFields['NAME'];
    }
    else{
        $title_podcast='<span>#'.$section_index.'</span> '.$arFields['NAME'];
    }
	$date=strlen($arFields['ACTIVE_FROM'])>0?$arFields['ACTIVE_FROM']:$arFields['DATE_CREATE'];
	$date=explode(" ",$date)[0];
	$file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $goods.='
    <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
       <tr style="padding:0;text-align:left;vertical-align:top">
           <th class="small-12 large-4 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%">
               <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                   <tr style="padding:0;text-align:left;vertical-align:top">
                       <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                           <img src="http://hiddenfaces.ru'.$file['src'].'" alt="" style="-ms-interpolation-mode:bicubic;clear:both;display:block;margin-top: 10px;max-width:150px;outline:0;text-decoration:none;width:auto">
                       </th>
                   </tr>
               </table>
           </th>
           <th class="small-12 large-8 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:66.66667%">
               <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                   <tr style="padding:0;text-align:left;vertical-align:top">
                       <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                           <div class="podcast-item" style="padding:20px 0">
                               <h4 class="podcast-name" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:700;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal">
                                   <a href="http://hiddenfaces.ru'.$arFields['DETAIL_PAGE_URL'].'" style="Margin:0;color:#000!important;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">
                                       '.$title_podcast.'</a>
                               </h4>
                               <p class="podcast-text" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left">'.$arFields['PREVIEW_TEXT'].'</p>
                               <div class="podcast-info">
                                   <span class="podcast-date" style="margin-right:10px">'.$date.'</span>';
	if(count($arProps['CATEGORIES']['VALUE'])>0){
		$arSelect2 = Array("ID", "NAME", "DETAIL_PAGE_URL");
		$arFilter2 = Array("IBLOCK_ID"=>5,"ID"=>$arProps['CATEGORIES']['VALUE'], "ACTIVE"=>"Y");
		$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize"=>3), $arSelect2);
		while($ob2 = $res2->GetNextElement()){
			$arFields2 = $ob2->GetFields();  
			$arProps2 = $ob2->GetProperties();
			$goods.='<a href="http://hiddenfaces.ru'.$arFields2['DETAIL_PAGE_URL'].'" style="Margin:0;border-bottom:1px dashed;color:#2199e8;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;margin-right:10px;padding:0;text-align:left;text-decoration:none">'.$arFields2['NAME'].'</a>';
		}
	}
	$goods.='</div></div></th></tr></table></th></tr></tbody></table>';
    //CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array("WAS_SENT" => "1"));
}
$goods.='</th>
                    </tr>
                </table>
            </th>
        </tr>
    </tbody>
</table>';
if($count_goods>0){
    while(($subscr_arr = $subscr->Fetch())){  
       echo $subscr_arr["EMAIL"]."-";
        //отправляем письмо
         $arEventFields = array( 
            "EMAIL" => $subscr_arr["EMAIL"],  
			 "THEME" => "Еженедельная подписка на Скрытые лица",
            "GOODS" => $goods
        ); 
        if (CEvent::Send("PODCASTS_SUBSCRIPTION", "s1", $arEventFields,"Y",50)): 
           echo "ok<br>"; 
        endif;
    }
}
    

?>