<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как слушать подкасты на iPhone");
?>

<div class="container inner__container">
	<div class="row guests">
		<h1 class="col-12 no-bot-wrap">Как слушать подкасты на iPhone</h1>
		<div class="bread col-12">
			<a href="/">Главная</a>
			<span> > </span>
			<a>Как слушать подкасты на iPhone</a>
		</div>
		<div class="col-12">
			<p class="iphone-p">Все выпуски подкастов студии «Скрытые лица» можно слушать через стандартное приложение Apple «<a href="https://itunes.apple.com/ru/app/подкасты/id525463029?mt=8" class="iphone-a" target="_blank">Подкасты</a>»</p>
			<br />
			<ul class="iphone-ul">
				<li>1. Откройте приложение</li>
				<li>2. Откройте раздел поиска в правом нижнем углу</li>
				<li>3. Введите «Скрытые лица»</li>
				<li>4. Откройте подкаст «Скрытые лица» и нажмите «подписаться»</li>
				<li>5. Выберите интересующий ваш подкаст и нажмите «play»</li>
			</ul>
		</div>
	</div>
</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>