<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

	<div class="container inner__container">
		<div class="row guests">
			<h1 class="col-12 no-bot-wrap">Контакты</h1>
			<div class="bread col-sm-12">
				<a href="/">Главная</a>
				<span> > </span>
				<a>Контакты</a>
			</div>
			<div class="col-12 col-lg-6">
				<h2 class="kontakty-title">Свяжитесь с нами!</h2>
				<br />
				<p class="kontakty-p">По любому вопросу вы можете написать нам на почту:</p>
				<br />
				<p>
					<a href="mailto:info@hiddenfaces.ru" class="kontakty-a" target="_blank">info@hiddenfaces.ru</a>
				</p>
				<br />
				<p class="kontakty-p">Подписывайтесь на наши социальные сети:</p>
				<br>
				<div class="social__icons">
					<!-- <a href="#" class="item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<a href="#" class="item"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				<a href="#" class="item"><i class="fa fa-vk" aria-hidden="true"></i></a>
				<a href="#" class="item"><i class="fa fa-youtube" aria-hidden="true"></i></a> -->
					<a href="https://vk.com/hiddenfacesru" target="_blank" class="item">
						<i class="fa fa-vk" aria-hidden="true"></i>
					</a>
					<a href="https://www.facebook.com/hiddenfaces.ru/" target="_blank" class="item">
						<i class="fa fa-facebook" aria-hidden="true"></i>
					</a>
					<a href="https://twitter.com/hiddenfacesru" target="_blank" class="item">
						<i class="fa fa-twitter" aria-hidden="true"></i>
					</a>
					<a href="https://www.instagram.com/podcaststudio/" target="_blank" class="item">
						<i class="fa fa-instagram" aria-hidden="true"></i>
					</a>
					<a href="https://www.youtube.com/channel/UCGauKoZxKx4-z276hIVxY2A" target="_blank" class="item">
						<i class="fa fa-youtube-play" aria-hidden="true"></i>
					</a>
				</div>
				<br />
				<br />
				<br>
				<br>
				<form action="" class="back-form contacts">
					<h2 class="kontakty-title col-sm-12">Воспользуйтесь формой обратной связи:</h2>
					<div class="sm-space"></div>
					<div class="row align-items-stretch">
						<div class="col-sm-12">
							<input class="name form__input" type="text" placeholder="Введите ваше имя">
							<input class="email form__input" type="text" placeholder="Введите E-Mail">
							<div class="select-style">

								<select class="theme">
									<option value="">Укажите тему письма</option>
									<option value="Технические проблемы">Технические проблемы</option>
									<option value="По коммерческим вопросам">По коммерческим вопросам</option>
									<option value="Связь с авторами">Связь с авторами</option>
									<option value="Другое">Другое</option>
								</select>
							</div>
							<textarea class="message form__input" rows="3" placeholder="Ваше сообщение..." style="min-height: 0%;"></textarea>
							<div class="row">
								<div class="col-sm-6 form__group">
									<label for="personal-form" class="label__checkbox">Согласие на обработку персональных данных
										<input type="checkbox" class="check" id="personal-form" checked="">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="col-sm-6">
									<button class="header__button write-feedback btn">Отправить</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-12 col-lg-6 kontacts-banners">
				<div class="mainpage__shadowpanel mainpage__banner author__panel author__panel__contacts">
					<h2 class="title">СЛЕДИТЕ ЗА НАМИ<br>ВКОНТАКТЕ</h2>
					<p class="text">Слушайте первыми!
						<br>Комментируйте и делитесь с друзьями!</p>
					<a href="https://vk.com/hiddenfacesru" target="_blank" class="btn">Присоединиться</a>
				</div>
				<?$APPLICATION->IncludeComponent(
				  "bitrix:main.include",
				  "",
				  Array(
					"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
					"AREA_FILE_SUFFIX" => "",
					"EDIT_TEMPLATE" => "",
					"WITHOUT_FOOTER"=>"Y",
					"PATH" => "/include/subscribe-block-kontakty.php" //Указываем путь к файлу
				  )
			);?>
			</div>
		</div>
	</div>
	</div>

	<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>