<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Гости студии подкастов Скрытые лица");
$APPLICATION->SetTitle("Гости");
?>

<div class="container inner__container">
	<div class="row guests">
		<?
		$APPLICATION->IncludeComponent(
			  "bitrix:main.include",
			  "",
			  Array(
				"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
				"AREA_FILE_SUFFIX" => "",
				"EDIT_TEMPLATE" => "",
				"WITHOUT_FOOTER"=>"Y",
				  "WITHOUT_ROW"=>"Y",
				"section__margin"=>"section__margin",
				"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
			  )
		);
		?>
		<?
		$arFilter = Array("IBLOCK_ID"=>6, "ACTIVE"=>"Y");  
		$arSelect = Array("ID","NAME");
		$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
		if($ob = $res->GetNextElement()){ 
			$arFields = $ob->GetFields();  
			$arProps = $ob->GetProperties();
			$el = new CIBlockElement;
			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
				"SORT" => 99, 
			);
			// $res = $el->Update($arFields['ID'], $arLoadProductArray);
		}
		$APPLICATION->IncludeComponent("hiddenfaces:news.list","guests",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"FILTER_NAME" => "FILTER_PROGRAM",
				"IBLOCK_TYPE" => "hidden_faces",
				"IBLOCK_ID" => "6",
				"NEWS_COUNT" => "12",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "ACTIVE_FROM",
				"SORT_ORDER2" => "DESC",
				"FIELD_CODE" => Array("ID","DATE_CREATE","CREATED_BY","ACTIVE_FROM"),
				"PROPERTY_CODE" => Array("DESCRIPTION","STATUS","SHORT_DESCRIPTION"), 
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_LAST_MODIFIED" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"PAGER_BASE_LINK_ENABLE" => "Y",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
				"MESSAGE_404" => "",
				"PAGER_BASE_LINK" =>$_SERVER['REQUEST_URI'],
				"PAGER_PARAMS_NAME" => "arrPager",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
			)
		);
		?>
</div>
</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>